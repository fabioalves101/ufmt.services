﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ufmt.services.entities;
using System.ServiceModel.Activation;
using ufmt.services.extensions;
using System.Data.Common;
using System.Data.SqlClient;
using ufmt.sig.business;

namespace ufmt.services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Curso" in code, svc and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]    
    public class Curso : ICurso
    {
        public PlanoPedagogico GetPlanoPedagogico(int cursoUID, int periodo)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = 65536 * 5;

            idbufmtEntities db = new idbufmtEntities();
            vwArquivo arquivo = db.vwArquivo.Where(a => a.cursoSiga == cursoUID && a.periodo == periodo).SingleOrDefault();

            PlanoPedagogico ppc = new PlanoPedagogico()
            {
                ppcUID = int.Parse(arquivo.codigoArquivo.ToString()),
                cursoUID = arquivo.cursoSiga,
                periodo = arquivo.periodo,
                conteudo = arquivo.conteudoArquivo,
                nomeArquivo = arquivo.nomeArquivo,
                extensao = arquivo.extensaoArquivo                
            };

            return ppc;
        }

        public PlanoPedagogico GetLastPlanoPedagogico(int cursoUID)
        {
            
            idbufmtEntities db = new idbufmtEntities();

            //(from a in db.vwArquivo where a.cursoSiga == cursoUID orderby a.periodo descending select a).FirstOrDefault();
            vwArquivo arquivo = db.vwArquivo.Where(a => a.cursoSiga == cursoUID).OrderByDescending(a => a.periodo).FirstOrDefault();

            PlanoPedagogico ppc = new PlanoPedagogico()
            {
                ppcUID = int.Parse(arquivo.codigoArquivo.ToString()),
                cursoUID = arquivo.cursoSiga,
                periodo = arquivo.periodo,
                conteudo = arquivo.conteudoArquivo.Compress(),
                nomeArquivo = arquivo.nomeArquivo,
                extensao = arquivo.extensaoArquivo   
            };

            return ppc;
        }


        public List<CursoSite> GetCursosGraduacao(int campus)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = 65536 * 2;
                using (idbufmtEntities db = new idbufmtEntities())
                {
                    List<CursoSite> listCurso = new List<CursoSite>();

                    List<ViewCursosSite> cursos = db.ViewCursosSite.Where(c => c.sigacampuscurso == campus).ToList();

                    foreach (ViewCursosSite curso in cursos)
                    {
                        CursoSite c = new CursoSite() {
                            descr01_habilitacao = curso.descr01_habilitacao,
                            descr02_habilitacao = curso.descr02_habilitacao,
                            descr03_habilitacao = curso.descr03_habilitacao,
                            sigaanomaxgradcurso = curso.sigaanomaxgradcurso,
                            sigaanomingradcurso = curso.sigaanomingradcurso,
                            sigaautorizacaocurso = curso.sigaautorizacaocurso,
                            sigacampuscurso = Convert.ToInt32(curso.sigacampuscurso),
                            sigacodcursosiga = curso.sigacodcursosiga,
                            sigadescrgrau = curso.sigadescrgrau,
                            sigadescrregime = curso.sigadescrregime,
                            sigadescrturno = curso.sigadescrturno,
                            sigafonecurso = curso.sigafonecurso,
                            sigahistoricocurso = curso.sigahistoricocurso,
                            sigamec2reconhecimento = curso.sigamec2reconhecimento,
                            sigamecreconhecimento = curso.sigamecreconhecimento,
                            siganomecp1curso = curso.siganomecp1curso,
                            siganomecursovest = curso.siganomecursovest,
                            siganomepinstituto = curso.siganomepinstituto,
                            siganomesenha = curso.siganomesenha,
                            sigaobs2reconhecimento = curso.sigaobs2reconhecimento,
                            sigaobsreconhecimento = curso.sigaobsreconhecimento,
                            sigaperfilcurso = curso.sigaperfilcurso,
                            sigatipovest = curso.sigatipovest,
                            totalvagas = curso.totalvagas                            
                        };

                        listCurso.Add(c);
                    }

                    return listCurso;
                }
                
        }

        public List<CursoSite> GetCursosGraduacaoPorNome(int campus, string nome)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = 65536 * 2;
            using (idbufmtEntities db = new idbufmtEntities())
            {
                List<CursoSite> listCurso = new List<CursoSite>();

                List<ViewCursosSite> cursos = db.ViewCursosSite.Where(c => c.sigacampuscurso == campus && c.siganomecursovest.Contains(nome)).ToList();

                foreach (ViewCursosSite curso in cursos)
                {
                    CursoSite c = new CursoSite()
                    {
                        descr01_habilitacao = curso.descr01_habilitacao,
                        descr02_habilitacao = curso.descr02_habilitacao,
                        descr03_habilitacao = curso.descr03_habilitacao,
                        sigaanomaxgradcurso = curso.sigaanomaxgradcurso,
                        sigaanomingradcurso = curso.sigaanomingradcurso,
                        sigaautorizacaocurso = curso.sigaautorizacaocurso,
                        sigacampuscurso = Convert.ToInt32(curso.sigacampuscurso),
                        sigacodcursosiga = curso.sigacodcursosiga,
                        sigadescrgrau = curso.sigadescrgrau,
                        sigadescrregime = curso.sigadescrregime,
                        sigadescrturno = curso.sigadescrturno,
                        sigafonecurso = curso.sigafonecurso,
                        sigahistoricocurso = curso.sigahistoricocurso,
                        sigamec2reconhecimento = curso.sigamec2reconhecimento,
                        sigamecreconhecimento = curso.sigamecreconhecimento,
                        siganomecp1curso = curso.siganomecp1curso,
                        siganomecursovest = curso.siganomecursovest,
                        siganomepinstituto = curso.siganomepinstituto,
                        siganomesenha = curso.siganomesenha,
                        sigaobs2reconhecimento = curso.sigaobs2reconhecimento,
                        sigaobsreconhecimento = curso.sigaobsreconhecimento,
                        sigaperfilcurso = curso.sigaperfilcurso,
                        sigatipovest = curso.sigatipovest,
                        totalvagas = curso.totalvagas
                    };

                    listCurso.Add(c);
                }

                return listCurso;
            }

        }

        public List<ConcorrenciaCurso> GetConcorrencia(int? ano, string curso)
        {
            idbufmtEntities db = new idbufmtEntities();

            List<ViewConcorrencia> listView = new List<ViewConcorrencia>();
            if (ano != 0)
            {
                listView = ((!string.IsNullOrEmpty(curso)) ? db.ViewConcorrencia.Where(c => c.ano == ano.Value && c.nome.Contains(curso)).ToList() 
                    : db.ViewConcorrencia.Where(c => c.ano == ano.Value).ToList());             
            }
            else
            {
                listView = ((!string.IsNullOrEmpty(curso)) ? db.ViewConcorrencia.Where(c => c.nome.Contains(curso)).ToList()
                    : db.ViewConcorrencia.ToList());          
            }

            List<ConcorrenciaCurso> listConcorrencia = new List<ConcorrenciaCurso>();
            foreach (ViewConcorrencia vconc in listView)
            {
                ConcorrenciaCurso concorrencia = new ConcorrenciaCurso()
                {
                    ano = vconc.ano,
                    campus = vconc.campus,
                    concorrencia = vconc.concorrência,
                    inscritos = vconc.inscritos,
                    nomeCurso = vconc.nome,
                    vagas = vconc.vagas
                };

                listConcorrencia.Add(concorrencia);
            }

            return listConcorrencia; 
        }

        public List<PlanoEnsinoDisciplina> GetPlanosEnsino(int codigoCurso, string ano, string semestre)
        {
            try
            {
                idbufmtEntities db = new idbufmtEntities();
                Periodo periodo = db.Periodo.Where(p => p.ano.Equals(ano) && p.semestre.Equals(semestre)).FirstOrDefault();

                List<PlanoEnsinoDisciplina> planosDisciplina = new List<PlanoEnsinoDisciplina>();
                if (periodo != null)
                {
                    List<PlanoEnsino> planos = new List<PlanoEnsino>();
                    planos =
                        db.PlanoEnsino
                            .Where(pl => pl.periodoUID == periodo.periodoUID && pl.codigoCurso.Equals(codigoCurso))
                            .Where(pl => pl.homologado == true).ToList();

                    foreach (PlanoEnsino plano in planos)
                    {
                        PlanoEnsinoDisciplina ped = new PlanoEnsinoDisciplina()
                        {
                            codigoCurso = plano.codigoCurso,
                            matrizDisciplinarUID = plano.matrizDisciplinarUID,
                            nomeDisciplina = plano.MatrizDisciplinar.nome,
                            periodo = periodo.ano+periodo.semestre,
                            periodoUID = periodo.periodoUID,
                            planoEnsinoUID = plano.planoEnsinoUID,                            
                            urlDownload = "http://sistemas.ufmt.br/ufmt.planoensino/Download.aspx?disciplinaUID=" + plano.matrizDisciplinarUID + "&codigoCurso="+codigoCurso+"&periodo="+periodo.periodoUID

                        };

                        planosDisciplina.Add(ped);
                    }
                }
                else
                {
                    throw new FaultException<Exception>(new Exception("Período Inválido"), new FaultReason("Período Inválido"));
                }

                return planosDisciplina;
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        public CursoEntity GetCursoPorCodigoSiga(int codigoCurso)
        {
            try
            {               

                idbufmtEntities db = new idbufmtEntities();
                ufmt.services.entities.Curso curso = (from c in db.Curso where c.codigoExterno == codigoCurso select c).FirstOrDefault();

                CursoEntity cursoEntity = new CursoEntity()
                {
                    campusUID = curso.campusUID,
                    codigoExterno = curso.codigoExterno,
                    codigoIntegracao = curso.codigoIntegracao,
                    cursoAssociadoUID = curso.cursoAssociadoUID,
                    cursoUID = curso.cursoUID,
                    dataFundacao = curso.dataFundacao,
                    instituicaoConvenioUID = curso.instituicaoConvenioUID,
                    nivelCursoUID = curso.nivelCursoUID,
                    nome = curso.nome,
                    tipoCursoUID = curso.tipoCursoUID,
                    tipoIntegracao = curso.tipoIntegracao,
                    unidadeOfertanteUID = curso.unidadeOfertanteUID

                };

                return cursoEntity;
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }


        public CursoEntity GetCursoPorCodigoSiged(int codigoCurso)
        {
            try
            {

                idbufmtEntities db = new idbufmtEntities();
                ufmt.services.entities.Curso curso = 
                    (from c in db.Curso 
                        where c.codigoExterno == codigoCurso && c.tipoIntegracao == "D"
                     select c).FirstOrDefault();

                CursoEntity cursoEntity = new CursoEntity()
                {
                    campusUID = curso.campusUID,
                    codigoExterno = curso.codigoExterno,
                    codigoIntegracao = curso.codigoIntegracao,
                    cursoAssociadoUID = curso.cursoAssociadoUID,
                    cursoUID = curso.cursoUID,
                    dataFundacao = curso.dataFundacao,
                    instituicaoConvenioUID = curso.instituicaoConvenioUID,
                    nivelCursoUID = curso.nivelCursoUID,
                    nome = curso.nome,
                    tipoCursoUID = curso.tipoCursoUID,
                    tipoIntegracao = curso.tipoIntegracao,
                    unidadeOfertanteUID = curso.unidadeOfertanteUID
                };

                return cursoEntity;
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        public CursoEntity GetCurso(long cursoUID)
        {
            try
            {
                idbufmtEntities db = new idbufmtEntities();
                ufmt.services.entities.Curso curso = (from c in db.Curso where 
                                                     c.cursoUID == cursoUID select c).FirstOrDefault();

                CursoEntity cursoEntity = new CursoEntity()
                {
                    campusUID = curso.campusUID,
                    codigoExterno = curso.codigoExterno,
                    codigoIntegracao = curso.codigoIntegracao,
                    cursoAssociadoUID = curso.cursoAssociadoUID,
                    cursoUID = curso.cursoUID,
                    dataFundacao = curso.dataFundacao,
                    instituicaoConvenioUID = curso.instituicaoConvenioUID,
                    nivelCursoUID = curso.nivelCursoUID,
                    nome = curso.nome,
                    tipoCursoUID = curso.tipoCursoUID,
                    tipoIntegracao = curso.tipoIntegracao,
                    unidadeOfertanteUID = curso.unidadeOfertanteUID
                };

                return cursoEntity;
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }


        public List<CursoEntity> GetCursos(int campusUID)
        {
            try
            {
                idbufmtEntities db = new idbufmtEntities();
                List<ufmt.services.entities.Curso> cursos = (from c in db.Curso
                                                      where
                                                          c.campusUID == campusUID
                                                      select c).ToList();

                List<CursoEntity> cursoEntityList = new List<CursoEntity>();

                foreach (ufmt.services.entities.Curso curso in cursos)
                {
                    CursoEntity cursoEntity = new CursoEntity()
                    {
                        campusUID = curso.campusUID,
                        codigoExterno = curso.codigoExterno,
                        codigoIntegracao = curso.codigoIntegracao,
                        cursoAssociadoUID = curso.cursoAssociadoUID,
                        cursoUID = curso.cursoUID,
                        dataFundacao = curso.dataFundacao,
                        instituicaoConvenioUID = curso.instituicaoConvenioUID,
                        nivelCursoUID = curso.nivelCursoUID,
                        nome = curso.nome,
                        tipoCursoUID = curso.tipoCursoUID,
                        tipoIntegracao = curso.tipoIntegracao,
                        unidadeOfertanteUID = curso.unidadeOfertanteUID
                    };

                    cursoEntityList.Add(cursoEntity);
                }

                return cursoEntityList;
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        public List<MatrizCurricularEntity> GetMatrizesCurriculares(long cursoUID)
        {
            CursoEntity curso = this.GetCurso(cursoUID);

            List<MatrizCurricularEntity> matrizesE = new List<MatrizCurricularEntity>();

            using (ICursoBO cursoBO = BusinessFactory.GetInstance<ICursoBO>())
            {
                List<ufmt.sig.entity.MatrizCurricular> matrizes = 
                    cursoBO.GetMatrizesCurricularesAtivas(curso.campusUID,
                    curso.nivelCursoUID, cursoUID, null, null).ToList();

                
                foreach (ufmt.sig.entity.MatrizCurricular matriz in matrizes)
                {
                    MatrizCurricularEntity matrizE = new MatrizCurricularEntity()
                    {
                        Descricao = matriz.Descricao,
                        FormatoCursoUID = matriz.FormatoCurso.FormatoCursoUID,
                        MatrizCurricularUID = matriz.MatrizCurricularUID,
                        ModalidadeCursoUID = matriz.ModalidadeCurso.ModalidadeCursoUID,
                        NumeroDeSemanas = matriz.NumeroDeSemanas,
                        PeriodicidadeCursoUID = matriz.PeriodicidadeCurso.PeriodicidadeCursoUID,
                        PeriodoFinalFuncionamento = matriz.PeriodoFinalFuncionamento,
                        PeriodoInicialFuncionamento = matriz.PeriodoInicialFuncionamento,
                        RegimeCursoUID = matriz.RegimeCurso.RegimeCursoUID,
                        SituacaoCursoUID = matriz.SituacaoCurso.SituacaoCursoUID,
                        TurnoCursoUID = matriz.TurnoCurso.TurnoCursoUID,
                        Curso = curso                       
                    };

                    matrizesE.Add(matrizE);
                }
            }

            return matrizesE;
        }

        public List<CursoEntity> GetCursosPorCoordenador(int codigoCoordenador)
        {
            idbufmtEntities db = new idbufmtEntities();
            List<vwCoordenadores> coordenadores = db.vwCoordenadores.Where(c => c.coordenadorUID == codigoCoordenador).ToList();


            List<CursoEntity> cursos = new List<CursoEntity>();
            foreach (vwCoordenadores coordenador in coordenadores)
            {
                ufmt.services.entities.Curso curso = db.Curso.Where(cur => cur.codigoExterno == coordenador.codigoCurso).FirstOrDefault();

                CursoEntity cursoEntity = new CursoEntity()
                {
                    campusUID = curso.campusUID,
                    codigoExterno = curso.codigoExterno,
                    codigoIntegracao = curso.codigoIntegracao,
                    cursoAssociadoUID = curso.cursoAssociadoUID,
                    cursoUID = curso.cursoUID,
                    dataFundacao = curso.dataFundacao,
                    instituicaoConvenioUID = curso.instituicaoConvenioUID,
                    nivelCursoUID = curso.nivelCursoUID,
                    nome = curso.nome,
                    tipoCursoUID = curso.tipoCursoUID,
                    tipoIntegracao = curso.tipoIntegracao,
                    unidadeOfertanteUID = curso.unidadeOfertanteUID                    
                };

                cursos.Add(cursoEntity);                
            }

            return cursos;
        }

        public List<MatrizCurricularEntity> GetMatrizesCurricularesPorCodigoExterno(int codigoCurso,
            bool apenasAtivas)
        {
            try
            {
                CursoEntity curso = this.GetCursoPorCodigoSiga(codigoCurso);

                List<MatrizCurricularEntity> matrizesE = new List<MatrizCurricularEntity>();

                using (ICursoBO cursoBO = BusinessFactory.GetInstance<ICursoBO>())
                {
                    List<ufmt.sig.entity.MatrizCurricular> matrizes = new List<sig.entity.MatrizCurricular>();
                    if (apenasAtivas)
                    {
                        matrizes = cursoBO.GetMatrizesCurricularesAtivas(curso.campusUID,
                                    curso.nivelCursoUID, curso.cursoUID, null, null).ToList();
                    }
                    else
                    {
                        matrizes = cursoBO.GetMatrizesCurriculares(curso.campusUID,
                                    curso.nivelCursoUID, curso.cursoUID, null, null).ToList();
                    }

                    foreach (ufmt.sig.entity.MatrizCurricular matriz in matrizes)
                    {
                        MatrizCurricularEntity matrizE = new MatrizCurricularEntity()
                        {
                            Descricao = matriz.Descricao,
                            FormatoCursoUID = matriz.FormatoCurso.FormatoCursoUID,
                            MatrizCurricularUID = matriz.MatrizCurricularUID,
                            ModalidadeCursoUID = matriz.ModalidadeCurso.ModalidadeCursoUID,
                            NumeroDeSemanas = matriz.NumeroDeSemanas,
                            PeriodicidadeCursoUID = matriz.PeriodicidadeCurso.PeriodicidadeCursoUID,
                            PeriodoFinalFuncionamento = matriz.PeriodoFinalFuncionamento,
                            PeriodoInicialFuncionamento = matriz.PeriodoInicialFuncionamento,
                            RegimeCursoUID = matriz.RegimeCurso.RegimeCursoUID,
                            SituacaoCursoUID = matriz.SituacaoCurso.SituacaoCursoUID,
                            TurnoCursoUID = matriz.TurnoCurso.TurnoCursoUID,
                            Curso = curso
                        };

                        matrizesE.Add(matrizE);
                    }
                }

                return matrizesE;

            }            
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }  
        }


        public List<PeriodoEntity> GetPeriodos(bool apenasAtivos)
        {
            try
            {
                using (idbufmtEntities db = new idbufmtEntities())
                {
                    List<Periodo> periodoList = new List<Periodo>();
                    if (apenasAtivos)
                        periodoList = db.Periodo.Where(p => p.ativo == true).ToList();
                    else
                        periodoList = db.Periodo.ToList();

                    List<PeriodoEntity> periodoEntityList = new List<PeriodoEntity>();
                    foreach (var periodo in periodoList)
                    {
                        PeriodoEntity periodoEntity = new PeriodoEntity()
                        {
                            Ano = periodo.ano,
                            Ativo = periodo.ativo,
                            PeriodoUID = periodo.periodoUID,
                            Semestre = periodo.semestre
                        };

                        periodoEntityList.Add(periodoEntity);
                    }

                    return periodoEntityList;
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }  
        }

        private CursoEntity CastCursoToCursoEntity(ufmt.sig.entity.Curso curso, bool infoSiga)
        {
            CursoEntity cursoEntity = new CursoEntity()
            {
                cursoUID = curso.CursoUID,
                campusUID = curso.Campus.CampusUID,
                codigoExterno = curso.CodigoExterno,
                codigoIntegracao = curso.CodigoIntegracao,
                tipoIntegracao = curso.TipoIntegracao.ToString(),
                nome = curso.Nome
            };

            cursoEntity.cursoAssociadoUID = (curso.CursoAssociado != null) ? curso.CursoAssociado.CursoUID : 0;
            cursoEntity.dataFundacao = (curso.DataFundacao.HasValue) ? curso.DataFundacao.Value : new DateTime();
            cursoEntity.instituicaoConvenioUID = (curso.InstituicaoConvenio != null) ? curso.InstituicaoConvenio.InstituicaoConvenioUID : 0;
            cursoEntity.nivelCursoUID = (curso.NivelCurso != null) ? curso.NivelCurso.NivelCursoUID : 0;
            cursoEntity.tipoCursoUID = (curso.TipoCurso != null) ? curso.TipoCurso.TipoCursoUID : 0;
            cursoEntity.unidadeOfertanteUID = (curso.UnidadeOfertante != null) ? curso.UnidadeOfertante.UnidadeUID : 0;

            if (infoSiga)
            {
                using (idbufmtEntities db = new idbufmtEntities())
                {
                    ViewCursoSiga cursoSiga = db.ViewCursoSiga.Where(vc => 
                                                vc.SigaCodCurso == curso.CodigoExterno &&
                                                vc.SigaPerFimCurso == 99999).FirstOrDefault();

                    cursoEntity.Regime = cursoSiga.SigaRegimeCurso.ToString();
                }
            }

            return cursoEntity;
        }
    }

    
}
