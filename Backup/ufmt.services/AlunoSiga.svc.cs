﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ufmt.services.entities;
using System.ServiceModel.Activation;

namespace ufmt.services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AlunoSiga" in code, svc and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]    
    public class AlunoSiga : IAlunoSiga
    {
        public entities.Aluno GetInfoAluno(string rga)
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {
                    decimal matricula = decimal.Parse(rga);

                    GetDadosAluno al =
                        (from c in contexto.GetDadosAluno
                         where c.rga == matricula
                         select c).FirstOrDefault();

                    if (al != null)
                    {
                        ufmt.services.entities.Aluno aluno = new ufmt.services.entities.Aluno()
                        {
                            CampusUID = al.campusUID,
                            Cpf = al.cpf,
                            Curso = al.curso,
                            DataNascimento = DateTime.Parse(al.dataNascimento),
                            Email = al.email,
                            NomeAluno = al.nomeAluno,
                            NomeCampus = al.campus,
                            Rga = al.rga.ToString(),
                            Sexo = al.sexo,
                            CodigoCurso = al.codidoCurso,
                            Rg = al.rg,
                            Agencia = al.agencia,
                            CodigoBanco = al.CodigoFebrabram,
                            NumeroConta = al.numeroConta,
                            NomeBanco = al.NomeBanco
                        };

                        return aluno;
                    }
                    else
                    {
                        string msg = "Nenhum aluno encontrado com os dados informados";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }

        public List<entities.Aluno> GetAlunos()
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {

                    IQueryable<Aluno> alunos =
                        (from c in contexto.GetDadosAlunoGeral select new Aluno() {
                            CampusUID = c.campusUID,
                            Cpf = c.cpf,
                            Curso = c.curso,
                            //DataNascimento = c.dataNascimento,
                            Email = c.email,
                            NomeAluno = c.nomeAluno,
                            Matricula = c.rga
                        });

                    if (alunos != null)
                    {                        
                        return alunos.ToList();
                    }
                    else
                    {
                        string msg = "Nenhum aluno encontrado";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }

        public List<entities.Aluno> GetAlunosGraduacao()
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {

                    IQueryable<Aluno> alunos =
                        (from c in contexto.GetDadosAluno select new Aluno() {
                            CampusUID = c.campusUID,
                            Cpf = c.cpf,
                            Curso = c.curso,
                            DataNascimento = DateTime.Parse(c.dataNascimento),
                            Email = c.email,
                            NomeAluno = c.nomeAluno,
                            Matricula = c.rga
                        });

                    if (alunos != null)
                    {                        
                        return alunos.ToList();
                    }
                    else
                    {
                        string msg = "Nenhum aluno encontrado";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }



        public List<entities.Aluno> GetAlunosPorCampus(int campusUID)
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {

                    IQueryable<Aluno> alunos =
                        (from c in contexto.GetDadosAlunoGeral where c.campusUID == campusUID
                         select new Aluno()
                         {
                             CampusUID = c.campusUID,
                             Cpf = c.cpf,
                             Curso = c.curso,
                             //DataNascimento = c.dataNascimento,
                             Email = c.email,
                             NomeAluno = c.nomeAluno,
                             Matricula = c.rga
                         });

                    if (alunos != null)
                    {
                        return alunos.ToList();
                    }
                    else
                    {
                        string msg = "Nenhum aluno encontrado";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }

        public List<entities.Aluno> GetAlunosGraduacaoPorCampus(int campusUID)
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {

                    IQueryable<Aluno> alunos =
                        (from c in contexto.GetDadosAluno
                         where c.campusUID == campusUID
                         select new Aluno()
                         {
                             CampusUID = c.campusUID,
                             Cpf = c.cpf,
                             Curso = c.curso,
                             DataNascimento = DateTime.Parse(c.dataNascimento),
                             Email = c.email,
                             NomeAluno = c.nomeAluno,
                             Matricula = c.rga
                         });

                    if (alunos != null)
                    {
                        return alunos.ToList();
                    }
                    else
                    {
                        string msg = "Nenhum aluno encontrado";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }

        public List<entities.Aluno> GetAlunosPaginado(int registroInicial, int quantidadeRegistros)
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {

                    IQueryable<Aluno> alunos =
                        (from c in contexto.GetDadosAlunoGeral.OrderBy(c => c.rga).Skip(registroInicial).Take(quantidadeRegistros)
                         select new Aluno()
                         {
                             CampusUID = c.campusUID,
                             Cpf = c.cpf,
                             Curso = c.curso,
                             //DataNascimento = c.dataNascimento,
                             Email = c.email,
                             NomeAluno = c.nomeAluno,
                             Matricula = c.rga
                         });

                    if (alunos != null)
                    {
                        return alunos.ToList();
                    }
                    else
                    {
                        string msg = "Nenhum aluno encontrado";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }

        public List<entities.Aluno> GetAlunosPorCampusPaginado(int campusUID, int registroInicial, int quantidadeRegistros)
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {

                    IQueryable<Aluno> alunos =
                        (from c in contexto.GetDadosAlunoGeral
                             .Where(c => c.campusUID == campusUID)
                             .OrderBy(c => c.rga)
                             .Skip(registroInicial)
                             .Take(quantidadeRegistros)
                         
                         select new Aluno()
                         {
                             CampusUID = c.campusUID,
                             Cpf = c.cpf,
                             Curso = c.curso,
                             //DataNascimento = c.dataNascimento,
                             Email = c.email,
                             NomeAluno = c.nomeAluno,
                             Matricula = c.rga
                         });

                    if (alunos != null)
                    {
                        return alunos.ToList();
                    }
                    else
                    {
                        string msg = "Nenhum aluno encontrado";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }

        public double GetCoeficiente(string rga)
        {
            using (idbufmtEntities contexto = new idbufmtEntities())
            {
                decimal drga = decimal.Parse(rga);
                SP_CALCULO_COEFICIENTE_RENDIMENTO_ALUNO_Result coeficiente = 
                    contexto.SP_CALCULO_COEFICIENTE_RENDIMENTO_ALUNO(drga).FirstOrDefault();
                if (coeficiente.COEFICIENTE_RENDIMENTO.HasValue)
                    return coeficiente.COEFICIENTE_RENDIMENTO.Value;
                else
                    return 0d;
            }
            
        }
        
    }
}
