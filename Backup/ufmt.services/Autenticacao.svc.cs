﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ufmt.sig.business;
using ufmt.sig.entity;
using System.ServiceModel.Activation;
using ufmt.services.entities;

namespace ufmt.services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]    
    public class Autenticacao : IAutenticacao
    {
        public Usuario Login(string nomeAcesso, string senha)
        {
            try
            {
                using (IAutenticacaoBO autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
                {
                    try
                    {
                        Usuario usuario = autenticacaoBO.GetUsuario(nomeAcesso, senha);
                        return usuario;
                    }
                    catch (Exception ex)
                    {
                        throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
                    }
                }

            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }            
        }


        public Usuario LoginDocente(string nomeAcesso, string senha)
        {
            try
            {
                using (IAutenticacaoBO autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
                {
                    try
                    {
                        Usuario usuario = autenticacaoBO.GetUsuario(nomeAcesso, senha);

                        using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                        {                            
                            IList<Servidor> list = pessoaBO.GetVinculosDocenciaPorPessoa(usuario.Pessoa.PessoaUID);
                            if (list.Count > 0)
                            {
                                return usuario;
                            }
                            else
                            {
                                throw new FaultException<Exception>(new Exception("O usuário não é um docente"), new FaultReason("O usuário não é um docente"));
                            }
                        }                        
                        
                    }
                    catch (FaultException<Exception> ex)
                    {
                        throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
                    }
                }

            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        public Servidor GetVinculoPessoaSimples(long pessoaUID)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    Servidor servidor = pessoaBO.GetVinculosPorPessoa(pessoaUID, true).First();

                    Servidor s = new Servidor();
                    s.Registro = servidor.Registro;
                    s.TipoRegistro = servidor.TipoRegistro;
                    
                    return s;
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        public IList<Servidor> GetVinculosPorPessoa(long pessoaUID)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    IList<Servidor> servidorList = pessoaBO.GetVinculosPorPessoa(pessoaUID, true);

                    IList<Servidor> newServidorList = new List<Servidor>();

                    foreach (Servidor servidor in servidorList)
                    {
                        servidor.UnidadeLotacao = null;
                        newServidorList.Add(servidor);
                        
                    }
                    return newServidorList;
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        public IList<ufmt.sig.entity.Pessoa> GetPessoas(string Nome)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    IList<ufmt.sig.entity.Pessoa> p = pessoaBO.GetPessoas(Nome);
                    return p;
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }


        public ufmt.sig.entity.Pessoa GetPessoa(long pessoaUID)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    ufmt.sig.entity.Pessoa p = pessoaBO.GetPessoa(pessoaUID);
                    return p;
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }


        public IList<Servidor> GetVinculoDocente(long pessoaUID)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    IList<Servidor> servidorList = pessoaBO.GetVinculosDocenciaPorPessoa(pessoaUID);
                    
                    IList<Servidor> newServidorList = new List<Servidor>();

                    foreach (Servidor servidor in servidorList)
                    {
                        servidor.UnidadeLotacao = null;
                        newServidorList.Add(servidor);
                    }
                    return newServidorList;
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }


        public entities.Aluno LoginAluno(string rga, string senha)
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {
                    decimal matricula = decimal.Parse(rga);
                    
                    GetDadosAluno al =
                        (from c in contexto.GetDadosAluno
                         where c.rga == matricula &&
                               c.senha.Trim().Equals(senha)
                         select c).FirstOrDefault();

                    if (al != null)
                    {
                        Aluno aluno = new Aluno()
                        {
                            CampusUID = al.campusUID,
                            Cpf = al.cpf,
                            Curso = al.curso,
                            DataNascimento = DateTime.Parse(al.dataNascimento),
                            Email = al.email,
                            NomeAluno = al.nomeAluno,
                            NomeCampus = al.campus,
                            Rga = al.rga.ToString(),
                            Sexo = al.sexo,
                            CodigoCurso = al.codidoCurso,
                            Agencia = al.agencia,
                            NumeroConta = al.numeroConta,
                            NomeBanco = al.NomeBanco,
                            CodigoBanco = al.CodigoFebrabram,
                            Rg = al.rg,
                            Endereco = al.enderecoAluno,
                            Cidade = al.cidadeAluno,
                            UF = al.ufAluno,
                            CEP = al.cepAluno,
                            ano = al.ano,
                            semestre = al.semestre
                        };

                        return aluno;
                    }
                    else
                    {
                        string msg = "Nenhum aluno encontrado com os dados informados";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }   
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message), 
                    new FaultReason(ex.Message));
            }
            
        }

        public entities.Aluno LoginDiscente(string rga, string senha)
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {
                    decimal matricula = decimal.Parse(rga);

                    GetDadosAluno al =
                        (from c in contexto.GetDadosAluno
                         where c.rga == matricula &&
                               c.senha.Trim().Equals(senha)
                         select c).FirstOrDefault();

                    if (al != null)
                    {
                        Aluno aluno = new Aluno()
                        {
                            CampusUID = al.campusUID,
                            Cpf = al.cpf,
                            Curso = al.curso,
                            DataNascimento = DateTime.Parse(al.dataNascimento),
                            Email = al.email,
                            NomeAluno = al.nomeAluno,
                            NomeCampus = al.campus,
                            Rga = al.rga.ToString(),
                            Sexo = al.sexo,
                            CodigoCurso = al.codidoCurso,
                            Agencia = al.agencia,
                            NumeroConta = al.numeroConta,
                            NomeBanco = al.NomeBanco,
                            CodigoBanco = al.CodigoFebrabram
                        };

                        return aluno;
                    }
                    else
                    {
                        string msg = "Nenhum aluno encontrado com os dados informados";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }

        }

        public PermissaoUsuario GetPermissao(long aplicacaoUID, long permissaoUID, long usuarioUID)
        {
            using (IAutenticacaoBO autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
            {
                PermissaoUsuario permissaoUsuario = 
                    autenticacaoBO.GetPermissaoUsuario(aplicacaoUID, permissaoUID, usuarioUID);
                return permissaoUsuario;
            }
        }


        public string VerificaDados(string nomeAcesso, string senha)
        {
            return "nomeAcesso='"+nomeAcesso+"' e senha='"+senha+"'";
        }


        public Usuario LoginDelphi(string nomeAcesso, string senha)
        {
            try
            {
                return this.Login(nomeAcesso, senha);
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }  
        }



        public Coordenador LoginCoordenadorCurso(string username, string password)
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {
                    var result =
                        (from c in contexto.vwCoordenadores 
                            where c.usuario == username && c.senha == password
                         select c).FirstOrDefault();

                    if (result != null)
                    {

                        Coordenador coordenador = new Coordenador()
                        {
                            CampusUID = result.campusUID.Value,
                            CodigoCurso = result.codigoCurso,
                            CoordenadorUID = result.coordenadorUID,
                            cpf = result.cpf,
                            Email = result.email,
                            Nome = result.nomeCoordenador,                            
                            Usuario = result.usuario
                        };


                        if (result.siape.HasValue)
                            coordenador.Siape = result.siape.Value;

                        return coordenador;
                    }
                    else
                    {
                        string msg = "Nenhum usuário encontrado com os dados informados";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }


        public Coordenador GetCoordenadorPorChave(string chave)
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {
                    Guid chaveGuid = new Guid(chave);
                    var result =
                        (from c in contexto.vwCoordenadores
                         where c.chave.Value == chaveGuid
                         select c).FirstOrDefault();

                    if (result != null)
                    {

                        Coordenador coordenador = this.ConvertVwCoordenadoresToCoordenador(result);


                        if (result.siape.HasValue)
                            coordenador.Siape = result.siape.Value;

                        return coordenador;
                    }
                    else
                    {
                        string msg = "Nenhum usuário encontrado com os dados informados";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        public Coordenador GetCoordenadorPorChaveCodigoCurso(string chave, string codigoCurso)
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {
                    int iCodigoCurso = int.Parse(codigoCurso);

                    Guid chaveGuid = new Guid(chave);
                    var result =
                        (from c in contexto.vwCoordenadores
                         where c.chave.Value == chaveGuid && c.codigoCurso == iCodigoCurso
                         select c).FirstOrDefault();

                    if (result != null)
                    {
                        Coordenador coordenador = this.ConvertVwCoordenadoresToCoordenador(result);

                        if (result.siape.HasValue)
                            coordenador.Siape = result.siape.Value;

                        return coordenador;
                    }
                    else
                    {
                        string msg = "Nenhum usuário encontrado com os dados informados";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }


        public Aluno LoginAluno(string chave)
        {
            try
            {
                using (idbufmtEntities db = new idbufmtEntities())
                {
                    Guid guidChave = new Guid(chave);

                    GetDadosAluno al = db.GetDadosAluno.Where(a => a.chave == guidChave).FirstOrDefault();
                    
                    Aluno aluno = new Aluno()
                    {
                        CampusUID = al.campusUID,
                        Cpf = al.cpf,
                        Curso = al.curso,
                        DataNascimento = DateTime.Parse(al.dataNascimento),
                        Email = al.email,
                        NomeAluno = al.nomeAluno,
                        NomeCampus = al.campus,
                        Rga = al.rga.ToString(),
                        Sexo = al.sexo,
                        CodigoCurso = al.codidoCurso,
                        Agencia = al.agencia,
                        NumeroConta = al.numeroConta,
                        NomeBanco = al.NomeBanco,
                        CodigoBanco = al.CodigoFebrabram,
                        Rg = al.rg,
                        Endereco = al.enderecoAluno,
                        Cidade = al.cidadeAluno,
                        UF = al.ufAluno,
                        CEP = al.cepAluno,
                        ano = al.ano,
                        semestre = al.semestre
                    };

                    return aluno;
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        
       

        public Coordenador ConvertVwCoordenadoresToCoordenador(vwCoordenadores result)
        {
            Coordenador coordenador = new Coordenador()
            {
                CampusUID = result.campusUID.Value,
                CodigoCurso = result.codigoCurso,
                CoordenadorUID = result.coordenadorUID,
                cpf = result.cpf,
                Email = result.email,
                Nome = result.nomeCoordenador,
                Usuario = result.usuario
            };

            return coordenador;
        }

        public void ReiniciarSenha(string cpf)
        {
            try
            {
                using (IAutenticacaoBO autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
                {
                    ufmt.sig.entity.Pessoa pessoa = null;
                    using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                    {
                        pessoa = pessoaBO.GetPessoa(cpf);
                    }

                    if (pessoa != null)
                    {
                        ufmt.sig.entity.Usuario usuario = autenticacaoBO.GetUsuarioPorPessoa(pessoa.PessoaUID);
                        if (usuario != null)
                        {
                            autenticacaoBO.ReiniciarSenha(usuario);
                        }
                        else
                        {
                            throw new Exception("Não foi encontrado registro de usuário com o CPF informado");
                        }
                    } else {
                        throw new Exception("Não foi encontrado registro de pessoa com o CPF informado");
                    }
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        public void CriarUsuario(string cpf)
        {
            try
            {
                using (IAutenticacaoBO autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
                {
                    ufmt.sig.entity.Pessoa pessoa = null;
                    using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                    {
                        pessoa = pessoaBO.GetPessoa(cpf);
                    }

                    autenticacaoBO.CrieUsuario(pessoa, pessoa.Cpf, null);
                }

            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        public void AlterarSenha(string cpf, string senhaAtual, string novaSenha)
        {
            try
            {
                using (IAutenticacaoBO autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
                {
                    if (autenticacaoBO.VerificaNomeAcesso(cpf))
                    {
                        ufmt.sig.entity.Pessoa pessoa = null;
                        using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                        {
                            pessoa = pessoaBO.GetPessoa(cpf);
                        }
                        ufmt.sig.entity.Usuario usuario = autenticacaoBO.GetUsuarioPorPessoa(pessoa.PessoaUID);
                        autenticacaoBO.TroqueSenha(usuario, senhaAtual, novaSenha);
                    }
                }

            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
             
    }
}
