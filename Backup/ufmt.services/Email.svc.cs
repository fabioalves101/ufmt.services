﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.ServiceModel.Activation;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace ufmt.services
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]    
    public class Email : IEmail
    {
        public void EnviarSmtpPadrao(string enderecoEmail, string mensagem)
        {
            this.enviarEmail(enderecoEmail, mensagem);
        }

        public void EnviarSmtpPadrao(string enderecoEmail, string mensagem, string subject)
        {
            this.enviarEmail(enderecoEmail, mensagem);
        }

        private void enviarEmail(string enderecoEmail, string msg)
        {
            string destino = enderecoEmail;
            
            MailMessage message = new System.Net.Mail.MailMessage();
            message.To.Add(destino);
            message.Subject = "Sistema de Solicitações de Alunos da UFMT";
            message.IsBodyHtml = true;
            message.From = new MailAddress("gpsoft@ufmt.br", "SIGA/UFMT");
            message.Body = msg + "<br><br> ---- <br />NÃO RESPONDA A ESSE E-MAIL.";

            try
            {
                SmtpClient smtp = new SmtpClient("200.17.60.1", 25);
                smtp.EnableSsl = true;
                NetworkCredential cred = new NetworkCredential("gpsoft@ufmt.br", "wk165vx");
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = cred;

                //SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
                //smtp.EnableSsl = true;
                //NetworkCredential cred = new NetworkCredential("ufmt.sti.cesgea@gmail.com", "UFMTanalise");
                //smtp.UseDefaultCredentials = false;
                //smtp.Credentials = cred;

                ServicePointManager.ServerCertificateValidationCallback =
                    delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                    { return true; };

                smtp.Send(message);
            }
            catch (Exception) { }
        }
    }
}
