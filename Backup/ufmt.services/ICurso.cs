﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ufmt.services.entities;

namespace ufmt.services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ICurso" in both code and config file together.
    [ServiceContract]
    public interface ICurso
    {
        [OperationContract]
        PlanoPedagogico GetPlanoPedagogico(int cursoUID, int periodo);

        [OperationContract]
        PlanoPedagogico GetLastPlanoPedagogico(int cursoUID);

        [OperationContract]
        List<CursoSite> GetCursosGraduacao(int campus);
        
        [OperationContract]
        List<CursoSite> GetCursosGraduacaoPorNome(int campus, string nome);

        [OperationContract]
        List<ConcorrenciaCurso> GetConcorrencia(int? ano, string curso);

        [OperationContract]
        List<PlanoEnsinoDisciplina> GetPlanosEnsino(int codigoCurso, string ano, string semestre);

        [OperationContract]
        CursoEntity GetCursoPorCodigoSiga(int codigoCurso);

        [OperationContract]
        List<CursoEntity> GetCursosPorCoordenador(int codigoCoordenador);
        
        [OperationContract]
        List<MatrizCurricularEntity> GetMatrizesCurricularesPorCodigoExterno(int codigoCurso, bool apenasAtivas);

        [OperationContract]
        CursoEntity GetCurso(long cursoUID);

        [OperationContract]
        List<CursoEntity> GetCursos(int campusUID);

        [OperationContract]
        CursoEntity GetCursoPorCodigoSiged(int codigoCurso);

        [OperationContract]
        List<PeriodoEntity> GetPeriodos(bool apenasAtivos);
    }
}
