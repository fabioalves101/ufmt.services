﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ufmt.services.entities
{
    public class DisciplinaEntity
    {
        public String Nome;
        public long UnidadeOfertanteUID;
        public int? PeriodoInicialFuncionamento;
        public int? PeriodoFinalFuncionamento;
        public String TipoDisciplina;
        public String TipoOfertaDisciplinar;
        public long MatrizCurricularUID;
        public String Ementa;
        public float CargaHorariaTotal;
        public float CargaHorariaTeorica;
        public float CargaHorariaPraticaComponente;
        public float CargaHorariaPratica;
        public long? CodigoExterno;
        public long MatrizDisciplinaUID;
        public List<PessoaEntity> professorResponsavel;
    }
}
