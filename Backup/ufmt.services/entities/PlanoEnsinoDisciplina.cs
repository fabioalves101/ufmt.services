﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ufmt.services.entities
{
    public class PlanoEnsinoDisciplina
    {
        public int planoEnsinoUID { get; set; }
        public long matrizDisciplinarUID { get; set; }
        public int periodoUID { get; set; }
        public int codigoCurso { get; set; }
        public string periodo { get; set; }

        public string nomeDisciplina { get; set; }
        public string urlDownload { get; set; }
    }
}