﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ufmt.services.entities
{
    public class DisciplinaAlunoEntity
    {
        public DisciplinaEntity Discplina { get; set; }
        public string Turma { get; set; }
        public int Periodo { get; set; }
        public int CodigoCursoExterno { get; set; }
    }
}