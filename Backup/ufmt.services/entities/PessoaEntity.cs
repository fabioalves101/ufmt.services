﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ufmt.services.entities
{
    public class PessoaEntity : ufmt.sig.entity.Pessoa
    {
        public UnidadeEntity Unidade { get; set; }

        public string Siape { get; set; }
        public long ServidorUID { get; set; }

    }
}