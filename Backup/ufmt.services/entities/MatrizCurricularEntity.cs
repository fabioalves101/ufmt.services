﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ufmt.services.entities
{
    public class MatrizCurricularEntity
    {
        public virtual CursoEntity Curso { get; set; }
        public virtual string Descricao { get; set; }
        protected internal virtual int FormatoCursoUID { get; set; }
        public virtual long MatrizCurricularUID { get; set; }
        protected internal virtual int ModalidadeCursoUID { get; set; }
        public virtual int? NumeroDeSemanas { get; set; }
        protected internal virtual int PeriodicidadeCursoUID { get; set; }
        public virtual int? PeriodoFinalFuncionamento { get; set; }
        public virtual int? PeriodoInicialFuncionamento { get; set; }
        protected internal virtual int? RegimeCursoUID { get; set; }
        protected internal virtual int SituacaoCursoUID { get; set; }
        protected internal virtual int TurnoCursoUID { get; set; }
    }
}