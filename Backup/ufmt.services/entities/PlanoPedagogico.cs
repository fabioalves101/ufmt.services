﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace ufmt.services.entities
{
    [DataContract]
    public class PlanoPedagogico
    {
        [DataMember]
        public int ppcUID;

        [DataMember]
        public int cursoUID;

        [DataMember]
        public int periodo;

        [DataMember]
        public byte[] conteudo;

        [DataMember]
        public string nomeArquivo;

        [DataMember]
        public string extensao;
    }
}