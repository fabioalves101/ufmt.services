﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace ufmt.services.entities
{
    [DataContract]
    public class Aluno
    {
        [DataMember]
        public string Rga;

        [DataMember]
        public decimal Matricula;

        [DataMember]
        public string NomeAluno;

        [DataMember]
        public string Curso;

        [DataMember]
        public int CursoUID;

        [DataMember]
        public DateTime DataNascimento;

        [DataMember]
        public string Sexo;

        [DataMember]
        public string NomeCampus;

        [DataMember]
        public int CodigoCurso;

        [DataMember]
        public string Email;

        [DataMember]
        public string Cpf;

        [DataMember]
        public string Rg;

        [DataMember]
        public string Endereco;

        [DataMember]
        public string Cidade;

        [DataMember]
        public string UF;
        
        [DataMember]
        public string CEP;

        [DataMember]
        public int? CampusUID;

        [DataMember]
        public string Agencia;

        [DataMember]
        public string NumeroConta;

        [DataMember]
        public string CodigoBanco;

        [DataMember]
        public string NomeBanco;

        [DataMember]
        public string ano;

        [DataMember]
        public string semestre;
        

        private string senha;

        public string Senha
        {
            get { return senha; }
            set { senha = value; }
        }
    }
}