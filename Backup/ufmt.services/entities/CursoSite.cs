﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace ufmt.services.entities
{
    [DataContract]
    public class CursoSite
    {
        [DataMember]
        public string sigacodcursosiga;
        [DataMember]
        public string sigatipovest;
        [DataMember]
        public string siganomecursovest;
        [DataMember]
        public int? totalvagas;
        [DataMember]
        public string siganomecp1curso;
        [DataMember]
        public string siganomepinstituto;
        [DataMember]
        public string siganomesenha;
        [DataMember]
        public string sigafonecurso;
        [DataMember]
        public string sigadescrgrau;
        [DataMember]
        public string descr01_habilitacao;
        [DataMember]
        public string descr02_habilitacao;
        [DataMember]
        public string descr03_habilitacao;
        [DataMember]
        public string sigadescrregime;
        [DataMember]
        public string sigadescrturno;
        [DataMember]
        public double? sigaanomingradcurso;
        [DataMember]
        public double? sigaanomaxgradcurso;
        [DataMember]
        public string sigamecreconhecimento;
        [DataMember]
        public string sigamec2reconhecimento;
        [DataMember]
        public string sigaobsreconhecimento;
        [DataMember]
        public string sigaobs2reconhecimento;
        [DataMember]
        public string sigahistoricocurso;
        [DataMember]
        public string sigaperfilcurso;
        [DataMember]
        public string sigaautorizacaocurso;
        [DataMember]
        public int sigacampuscurso;
    }
}