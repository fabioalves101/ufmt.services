﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ufmt.services.entities
{
    public class PeriodoEntity
    {
        public int PeriodoUID { get; set; }
        public string Ano { get; set; }
        public string Semestre { get; set; }
        public bool Ativo { get; set; }
    }
}