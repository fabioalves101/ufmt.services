﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ufmt.sig.entity;

namespace ufmt.services.entities
{
    public class UnidadeEntity
    {
        public long CampusUID { get; set; }
        public string Campus { get; set; }
        public string Hierarquia { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }
        
        public int TipoUnidade { get; set; }
        public long UnidadeSuperiorUID { get; set; }
        public long UnidadeUID { get; set; }
        public char Ativa { get; set; }
    }
}