﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ufmt.services.entities
{
    public class ServidorPessoaSimples
    {
        private string nome;

        public string Nome
        {
            get { return nome; }
            set { nome = value; }
        }
        private string cpf;

        public string Cpf
        {
            get { return cpf; }
            set { cpf = value; }
        }
        private string siape;

        public string Siape
        {
            get { return siape; }
            set { siape = value; }
        }
        private string cargo;

        public string Cargo
        {
            get { return cargo; }
            set { cargo = value; }
        }
        private string lotacao;

        public string Lotacao
        {
            get { return lotacao; }
            set { lotacao = value; }
        }
    }
}