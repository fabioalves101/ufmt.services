﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ufmt.services.entities
{
    public class CursoEntity
    {
        public long cursoUID { get; set; }
        public Nullable<long> codigoExterno { get; set; }
        public string nome { get; set; }
        public Nullable<System.DateTime> dataFundacao { get; set; }
        public string codigoIntegracao { get; set; }
        public string tipoIntegracao { get; set; }
        public int nivelCursoUID { get; set; }
        public int tipoCursoUID { get; set; }
        public Nullable<long> cursoAssociadoUID { get; set; }
        public Nullable<int> instituicaoConvenioUID { get; set; }
        public int campusUID { get; set; }
        public Nullable<long> unidadeOfertanteUID { get; set; }
        public String Regime { get; set; }
    }
}