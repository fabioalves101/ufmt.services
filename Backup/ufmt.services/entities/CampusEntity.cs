﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ufmt.services.entities
{
    public class CampusEntity
    {
        public int CampusUID { get; set; }
        public string Nome { get; set; }
    }
}