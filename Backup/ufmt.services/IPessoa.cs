﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ufmt.sig.entity;
using ufmt.services.entities;

namespace ufmt.services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPessoa" in both code and config file together.
    [ServiceContract]
    public interface IPessoa
    {
        [OperationContract]
        ufmt.sig.entity.Pessoa GetPessoa(long pessoaUID);

        [OperationContract]
        List<ufmt.sig.entity.Pessoa> GetPessoas(string nome);

        [OperationContract]
        ufmt.sig.entity.Pessoa GetPessoaPorUsuario(long usuarioUID);

        [OperationContract]
        IList<CompositeTypeServidor> GetVinculoDocente(long pessoaUID);

        [OperationContract]
        ServidorPessoaSimples GetServidorPessoaSimples(string siape);

        [OperationContract]
        ufmt.sig.entity.Pessoa GetPessoaPorCPF(string cpf);

        [OperationContract]
        List<ufmt.sig.entity.Pessoa> GetProfessores(string nome);

        [OperationContract]
        PessoaEntity GetPessoaPorServidor(long servidorUID);


        [OperationContract]
        List<PessoaEntity> GetPessoasPorUnidade(long unidadeUID);

        [OperationContract]
        PessoaEntity GetPessoaEntity(long pessoaUID);

        [OperationContract]
        List<PessoaEntity> GetPessoasEntity(string nome, int registroInicial, int quantidadeRegistros);

        [OperationContract]
        vwCoordenadores GetCoordenadorSigaPorCodigoCurso(int codigoCurso);

        [OperationContract]
        vwCoordenadores GetCoordenadorSigaPorCursoUID(long cursoUID);
    }

    [DataContract(IsReference=true)]
        [KnownType(typeof(Unidade))]
        [KnownType(typeof(Cargo))]
        [KnownType(typeof(JornadaTrabalho))]
        [KnownType(typeof(ufmt.sig.entity.Pessoa))]
        [KnownType(typeof(SituacaoServidor))]    
    public class CompositeTypeServidor
    {
        [DataMember]
        public Int64 ServidorUID;

        [DataMember]
        public Pessoa Pessoa;

        [DataMember]
        public String Registro;

        [DataMember]
        public Char RegistroDV;

        [DataMember]
        public Char TipoRegistro;

        [DataMember]
        public String RegimeJuridico;

        [DataMember]
        public JornadaTrabalho JornadaTrabalho;

        [DataMember]
        public Cargo Cargo;

        [DataMember]
        public Char? CargoClasse;

        [DataMember]
        public String CargoReferenciaNivelPadrao;

        [DataMember]
        public Unidade UnidadeLotacao;

        [DataMember]
        public String AposentadoriaNumeroProcesso;

        [DataMember]
        public int? AposentadoriaAnoPrevisto;

        [DataMember]
        public Char? AposentadoriaOpcaoIntegral;
        
        [DataMember]
        public SituacaoServidor SituacaoServidor;
    }

}
