﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ufmt.sig.business;
using ufmt.sig.entity;
using System.ServiceModel.Activation;
using ufmt.services.entities;

namespace ufmt.services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Pessoa" in code, svc and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]    
    public class Pessoa : IPessoa
    {
        


        public sig.entity.Pessoa GetPessoa(long pessoaUID)
        {            
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    return pessoaBO.GetPessoa(pessoaUID);
                }
            }
            catch
            {
                throw;
            }
        }


        public sig.entity.Pessoa GetPessoaPorUsuario(long usuarioUID)
        {
            try
            {
                Usuario usuario = null;
                ufmt.sig.entity.Pessoa pessoa = null;
                using (IAutenticacaoBO autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
                {
                    usuario = autenticacaoBO.GetUsuario(usuarioUID);                    
                }

                if (usuario != null)
                {
                    using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                    {
                        pessoa = pessoaBO.GetPessoa(usuario.Pessoa.PessoaUID);
                    }
                }

                if (pessoa != null)
                    return pessoa;
                else
                    throw new Exception("Pessoa não encontrada com o ID de usuário fornecido");
            }
            catch
            {
                throw;
            }
        }


        public IList<CompositeTypeServidor> GetVinculoDocente(long pessoaUID)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    List<CompositeTypeServidor> obj = new List<CompositeTypeServidor>();
                    obj = (List<CompositeTypeServidor>)pessoaBO.GetVinculosDocenciaPorPessoa(pessoaUID);
                    
                    return obj;
                }
            }
            catch
            {
                throw;
            }
        }


        public entities.ServidorPessoaSimples GetServidorPessoaSimples(string siape)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    Servidor servidor = pessoaBO.GetServidor(siape, 'S');

                    entities.ServidorPessoaSimples obj = new entities.ServidorPessoaSimples()
                    {
                        Nome = servidor.Pessoa.Nome,
                        Siape = servidor.Registro,
                        Lotacao = (servidor.UnidadeLotacao != null ? servidor.UnidadeLotacao.Nome : "Sem Lotação Cadastrada"),
                        Cpf = servidor.Pessoa.Cpf,
                        Cargo = servidor.Cargo.Nome
                    };

                    return obj;
                }
            }
            catch
            {
                throw;
            }
        }

        public ufmt.sig.entity.Pessoa GetPessoaPorCPF(string cpf)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    return pessoaBO.GetPessoa(cpf);
                }
            }
            catch
            {
                throw;
            }
        }


        public List<sig.entity.Pessoa> GetPessoas(string nome)
        {
            using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
            {
                return pessoaBO.GetPessoas(nome).ToList();
            }
        }

        public List<sig.entity.Pessoa> GetProfessores(string nome)
        {
            using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
            {
                IList<ufmt.sig.entity.Servidor> servidores = pessoaBO.GetServidores(nome, null, true, null, null);

                return (from s in servidores select s.Pessoa).ToList();
            }
        }


        public List<PessoaEntity> GetPessoasPorUnidade(long unidadeUID)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    List<Servidor> servidores = 
                        pessoaBO.GetServidoresPorUnidadeLotacional(unidadeUID, null, null, false, null, null)
                        .ToList();

                    List<ufmt.sig.entity.Pessoa> pessoas = servidores.Select(p => p.Pessoa).ToList();

                    List<PessoaEntity> pessoasEntity = new List<PessoaEntity>();

                    foreach (ufmt.sig.entity.Pessoa pessoa in pessoas)
                    {
                        PessoaEntity pessoaEntity = new PessoaEntity()
                        {
                            Cpf = pessoa.Cpf,
                            Ctps = pessoa.Ctps,
                            CtpsSerie = pessoa.CtpsSerie,
                            CtpsUf = pessoa.CtpsUf,
                            EnderecoBairro = pessoa.EnderecoBairro,
                            EnderecoCep = pessoa.EnderecoCep,
                            EnderecoComplemento = pessoa.EnderecoComplemento,
                            EnderecoLogradouro = pessoa.EnderecoLogradouro,
                            EnderecoMunicipio = pessoa.EnderecoMunicipio,
                            EnderecoNumero = pessoa.EnderecoNumero,
                            EnderecoUf = pessoa.EnderecoUf,
                            Mail = pessoa.Mail,
                            NascimentoData = pessoa.NascimentoData,
                            Nome = pessoa.Nome,
                            NomeMae = pessoa.NomeMae,
                            NomePai = pessoa.NomePai,
                            PessoaUID = pessoa.PessoaUID,
                            PisPasep = pessoa.PisPasep,
                            Rg = pessoa.Rg,
                            RgDataExpedicao = pessoa.RgDataExpedicao,
                            RgOrgaoExpedidor = pessoa.RgOrgaoExpedidor,
                            RgUf = pessoa.RgUf,
                            Sexo = pessoa.Sexo,
                            TelefoneComercial = pessoa.TelefoneComercial,
                            TelefonePrimario = pessoa.TelefonePrimario,
                            TelefoneSecundario = pessoa.TelefoneSecundario,
                            TipoTitulacao = pessoa.TipoTitulacao,
                            TituloEleitor = pessoa.TituloEleitor,
                            TituloEleitorDataEmissao = pessoa.TituloEleitorDataEmissao,
                            TituloEleitorSecao = pessoa.TituloEleitorSecao,
                            TituloEleitorUf = pessoa.TituloEleitorUf,
                            TituloEleitorZona = pessoa.TituloEleitorZona                            
                        };

                        pessoasEntity.Add(pessoaEntity);
                    }

                    return pessoasEntity;

                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        public PessoaEntity GetPessoaEntity(long pessoaUID)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    ufmt.sig.entity.Pessoa pessoa = pessoaBO.GetPessoa(pessoaUID);

                    PessoaEntity pessoaEntity = this.CastPessoaToPessoaEntity(pessoa);
                    return pessoaEntity;
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        public PessoaEntity CastPessoaToPessoaEntity(ufmt.sig.entity.Pessoa pessoa)
        {
            PessoaEntity pessoaEntity = new PessoaEntity()
            {
                Cpf = pessoa.Cpf,
                Ctps = pessoa.Ctps,
                CtpsSerie = pessoa.CtpsSerie,
                CtpsUf = pessoa.CtpsUf,
                EnderecoBairro = pessoa.EnderecoBairro,
                EnderecoCep = pessoa.EnderecoCep,
                EnderecoComplemento = pessoa.EnderecoComplemento,
                EnderecoLogradouro = pessoa.EnderecoLogradouro,
                EnderecoMunicipio = pessoa.EnderecoMunicipio,
                EnderecoNumero = pessoa.EnderecoNumero,
                EnderecoUf = pessoa.EnderecoUf,
                Mail = pessoa.Mail,
                NascimentoData = pessoa.NascimentoData,
                Nome = pessoa.Nome,
                NomeMae = pessoa.NomeMae,
                NomePai = pessoa.NomePai,
                PessoaUID = pessoa.PessoaUID,
                PisPasep = pessoa.PisPasep,
                Rg = pessoa.Rg,
                RgDataExpedicao = pessoa.RgDataExpedicao,
                RgOrgaoExpedidor = pessoa.RgOrgaoExpedidor,
                RgUf = pessoa.RgUf,
                Sexo = pessoa.Sexo,
                TelefoneComercial = pessoa.TelefoneComercial,
                TelefonePrimario = pessoa.TelefonePrimario,
                TelefoneSecundario = pessoa.TelefoneSecundario,
                TipoTitulacao = pessoa.TipoTitulacao,
                TituloEleitor = pessoa.TituloEleitor,
                TituloEleitorDataEmissao = pessoa.TituloEleitorDataEmissao,
                TituloEleitorSecao = pessoa.TituloEleitorSecao,
                TituloEleitorUf = pessoa.TituloEleitorUf,
                TituloEleitorZona = pessoa.TituloEleitorZona                
            };

            using(IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
            {
                ufmt.sig.entity.Servidor servidor = pessoaBO.GetServidorAtivo(pessoa.PessoaUID);
                if (servidor != null)
                {
                    pessoaEntity.ServidorUID = servidor.ServidorUID;
                    pessoaEntity.Siape = servidor.Registro;
                    if (servidor.UnidadeLotacao != null)
                        pessoaEntity.Unidade = new Localizacao().CastUnidadeToUnidadeEntity(servidor.UnidadeLotacao);
                }
            }

            return pessoaEntity;
        }




        public PessoaEntity GetPessoaPorServidor(long servidorUID)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    Servidor servidor = pessoaBO.GetServidor(servidorUID);


                    PessoaEntity pessoa = this.ConvertServidorToPessoaEntity(servidor);

                    if(servidor.UnidadeLotacao != null)
                        pessoa.Unidade = new Localizacao().GetUnidade(servidor.UnidadeLotacao.UnidadeUID);



                    return pessoa;
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        public List<PessoaEntity> GetPessoasEntity(string nome, int registroInicial, int quantidadeRegistros)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    IEnumerable<ufmt.sig.entity.Pessoa> pessoasList = pessoaBO.GetPessoas(nome).
                        OrderBy(p => p.Nome)
                        .Skip(registroInicial)
                        .Take(quantidadeRegistros);

                    List<PessoaEntity> pessoaEntityList = new List<PessoaEntity>();
                    foreach (var pessoa in pessoasList)
                    {
                        pessoaEntityList.Add(CastPessoaToPessoaEntity(pessoa));
                    }

                    return pessoaEntityList;
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        protected PessoaEntity ConvertServidorToPessoaEntity(ufmt.sig.entity.Servidor servidor)
        {
            ufmt.sig.entity.Pessoa pessoa = servidor.Pessoa;
            PessoaEntity pessoaEntity = new PessoaEntity()
            {
                Cpf = pessoa.Cpf,
                Ctps = pessoa.Ctps,
                CtpsSerie = pessoa.CtpsSerie,
                CtpsUf = pessoa.CtpsUf,
                EnderecoBairro = pessoa.EnderecoBairro,
                EnderecoCep = pessoa.EnderecoCep,
                EnderecoComplemento = pessoa.EnderecoComplemento,
                EnderecoLogradouro = pessoa.EnderecoLogradouro,
                EnderecoMunicipio = pessoa.EnderecoMunicipio,
                EnderecoNumero = pessoa.EnderecoNumero,
                EnderecoUf = pessoa.EnderecoUf,
                Mail = pessoa.Mail,
                NascimentoData = pessoa.NascimentoData,
                Nome = pessoa.Nome,
                NomeMae = pessoa.NomeMae,
                NomePai = pessoa.NomePai,
                PessoaUID = pessoa.PessoaUID,
                PisPasep = pessoa.PisPasep,
                Rg = pessoa.Rg,
                RgDataExpedicao = pessoa.RgDataExpedicao,
                RgOrgaoExpedidor = pessoa.RgOrgaoExpedidor,
                RgUf = pessoa.RgUf,
                Sexo = pessoa.Sexo,
                TelefoneComercial = pessoa.TelefoneComercial,
                TelefonePrimario = pessoa.TelefonePrimario,
                TelefoneSecundario = pessoa.TelefoneSecundario,
                TipoTitulacao = pessoa.TipoTitulacao,
                TituloEleitor = pessoa.TituloEleitor,
                TituloEleitorDataEmissao = pessoa.TituloEleitorDataEmissao,
                TituloEleitorSecao = pessoa.TituloEleitorSecao,
                TituloEleitorUf = pessoa.TituloEleitorUf,
                TituloEleitorZona = pessoa.TituloEleitorZona,
                ServidorUID = servidor.ServidorUID,
                Siape = servidor.Registro
            };

            return pessoaEntity;
        }


        public vwCoordenadores GetCoordenadorSigaPorCodigoCurso(int codigoCurso)
        {
            try
            {
                using (idbufmtEntities db = new idbufmtEntities())
                {
                    vwCoordenadores coordenador = db.vwCoordenadores.Where(c => c.codigoCurso == codigoCurso).FirstOrDefault();

                    return coordenador;
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        public vwCoordenadores GetCoordenadorSigaPorCursoUID(long cursoUID)
        {
            try
            {
                ufmt.sig.entity.Curso curso;
                using (ICursoBO cursoBO = BusinessFactory.GetInstance<ICursoBO>())
                {
                    curso = cursoBO.GetCurso(cursoUID);
                }

                using (idbufmtEntities db = new idbufmtEntities())
                {
                    vwCoordenadores coordenador = db.vwCoordenadores.Where(c => c.codigoCurso == curso.CodigoExterno).FirstOrDefault();

                    return coordenador;
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
    }
}
