﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Activation;
using ufmt.sig.business;
using ufmt.services.entities;
using ufmt.sig.entity;

namespace ufmt.services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Localizacao" in code, svc and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]    
    public class Localizacao : ILocalizacao
    {
        public List<UnidadeEntity> GetUnidades(string nomeParcial, int? campusUID, bool? apenasAtivos, int registroInicial, 
            int quantidadeRegistros)
        {
            try
            {
                using (ILocalizacaoBO localizacaoBO = BusinessFactory.GetInstance<ILocalizacaoBO>())
                {
                    List<ufmt.sig.entity.Unidade> unidades =
                        localizacaoBO.GetUnidades(campusUID, null, nomeParcial, null, null, 
                        registroInicial, quantidadeRegistros).ToList();

                    if (apenasAtivos.HasValue && apenasAtivos.Value)
                        unidades = unidades.Where(u => u.Ativa == '1').ToList();

                    List<UnidadeEntity> unidadesEntity = new List<UnidadeEntity>();
                    foreach (Unidade unidade in unidades)
                    {
                        UnidadeEntity unidadeEntity = new UnidadeEntity()
                        {
                            CampusUID = unidade.Campus.CampusUID,
                            Hierarquia = unidade.Hierarquia,
                            Nome = unidade.Nome,
                            Sigla = unidade.Sigla,
                            TipoUnidade = (int)unidade.TipoUnidade,
                            UnidadeSuperiorUID = unidade.UnidadeSuperior.UnidadeUID,
                            UnidadeUID = unidade.UnidadeUID,
                            Ativa = unidade.Ativa
                        };

                        unidadesEntity.Add(unidadeEntity);
                    }

                    return unidadesEntity;
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }


        public UnidadeEntity GetUnidade(long unidadeUID)
        {
            try
            {
                using (ILocalizacaoBO localizacaoBO = BusinessFactory.GetInstance<ILocalizacaoBO>())
                {
                    ufmt.sig.entity.Unidade unidade = localizacaoBO.GetUnidade(unidadeUID);


                    UnidadeEntity unidadeEntity = new UnidadeEntity()
                    {
                        CampusUID = unidade.Campus.CampusUID,
                        Hierarquia = unidade.Hierarquia,
                        Nome = unidade.Nome,
                        Sigla = unidade.Sigla,
                        TipoUnidade = (int)unidade.TipoUnidade,
                        UnidadeSuperiorUID = unidade.UnidadeSuperior.UnidadeUID, 
                        UnidadeUID = unidade.UnidadeUID
                    };


                    return unidadeEntity;
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        public List<CampusEntity> GetCampi(string nomeParcial)
        {
            try
            {
                using (ILocalizacaoBO localizacaoBO = BusinessFactory.GetInstance<ILocalizacaoBO>())
                {
                    List<ufmt.sig.entity.Campus> campi = new List<ufmt.sig.entity.Campus>();

                    if (String.IsNullOrEmpty(nomeParcial))
                    {
                        campi = localizacaoBO.GetCampi().ToList();
                    }
                    else
                    {
                        campi = localizacaoBO.GetCampi().Where(c => c.Nome.Contains(nomeParcial.ToUpper())).ToList();
                    }
                    
                    List<CampusEntity> campiEntity = new List<CampusEntity>();
                    foreach (Campus campus in campi)
                    {
                        CampusEntity campusEntity = new CampusEntity()
                        {
                            CampusUID = campus.CampusUID,
                            Nome = campus.Nome
                        };

                        campiEntity.Add(campusEntity);
                    }

                    return campiEntity;
                }                
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        public UnidadeEntity GetUnidadePorResponsavel(long pessoaUID)
        {
            try
            {
                using (ILocalizacaoBO localizacaoBO = BusinessFactory.GetInstance<ILocalizacaoBO>())
                {
                    ufmt.sig.entity.Unidade unidade = localizacaoBO.GetUnidadesPorResponsavel(pessoaUID,
                        null, null, null, null).FirstOrDefault();

                    

                    UnidadeEntity unidadeEntity = new UnidadeEntity()
                    {
                        CampusUID = unidade.Campus.CampusUID,
                        Hierarquia = unidade.Hierarquia,
                        Nome = unidade.Nome,
                        Sigla = unidade.Sigla,
                        TipoUnidade = (int)unidade.TipoUnidade,
                        UnidadeSuperiorUID = unidade.UnidadeSuperior.UnidadeUID,
                        UnidadeUID = unidade.UnidadeUID
                    };

                    return unidadeEntity;
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        public UnidadeEntity CastUnidadeToUnidadeEntity(Unidade unidade)
        {
            UnidadeEntity unidadeEntity = new UnidadeEntity()
            {
                CampusUID = unidade.Campus.CampusUID,
                Hierarquia = unidade.Hierarquia,
                Nome = unidade.Nome,
                Sigla = unidade.Sigla,
                TipoUnidade = (int)unidade.TipoUnidade,
                UnidadeSuperiorUID = unidade.UnidadeSuperior.UnidadeUID,
                UnidadeUID = unidade.UnidadeUID
            };

            return unidadeEntity;
        }
    }
}
