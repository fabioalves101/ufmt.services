﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ufmt.sig.entity;
using ufmt.services.entities;

namespace ufmt.services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IAutenticacao
    {
        [OperationContract]
        Usuario LoginDelphi(String nomeAcesso, String senha);

        [OperationContract]
        Usuario Login(String nomeAcesso, String senha);

        [OperationContract]
        Usuario LoginDocente(String nomeAcesso, String senha);
        
        [OperationContract]
        String VerificaDados(String nomeAcesso, String senha);

        [OperationContract]
        Servidor GetVinculoPessoaSimples(long pessoaUID);

        [OperationContract]
            [ServiceKnownType(typeof(Servidor))]
        IList<Servidor> GetVinculoDocente(long pessoaUID);

        [OperationContract]
        ufmt.sig.entity.Pessoa GetPessoa(long pessoaUID);

        [OperationContract]
        IList<ufmt.sig.entity.Pessoa> GetPessoas(string Nome);

        [OperationContract]
        IList<Servidor> GetVinculosPorPessoa(long pessoaUID);

        [OperationContract(Name="LoginAluno")]
        Aluno LoginAluno(string rga, string senha);

        [OperationContract(Name = "LoginAlunoPorChave")]
        Aluno LoginAluno(string chave);

        [OperationContract]
        Aluno LoginDiscente(string rga, string senha);

        [OperationContract]
        Coordenador LoginCoordenadorCurso(string username, string password);
        
        [OperationContract]
        PermissaoUsuario GetPermissao(long aplicacaoUID, long permissaoUID, long usuarioUID);

        [OperationContract]
        Coordenador GetCoordenadorPorChave(string chave);

        [OperationContract]
        Coordenador GetCoordenadorPorChaveCodigoCurso(string chave, string codigoCurso);

        [OperationContract]
        void ReiniciarSenha(string cpf);

        [OperationContract]
        void CriarUsuario(string cpf);

        [OperationContract]
        void AlterarSenha(string cpf, string senhaAtual, string novaSenha);
    }
}
