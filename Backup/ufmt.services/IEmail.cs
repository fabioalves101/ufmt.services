﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ufmt.services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IEmail" in both code and config file together.
    [ServiceContract]
    public interface IEmail
    {
        [OperationContract]
        void EnviarSmtpPadrao(string enderecoEmail, string mensagem);
    }
}
