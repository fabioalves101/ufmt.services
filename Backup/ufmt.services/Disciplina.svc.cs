﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ufmt.services.entities;
using System.ServiceModel.Activation;
using ufmt.sig.business;

namespace ufmt.services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Disciplina" in code, svc and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]    
    public class Disciplina : IDisciplina
    {
        public Ementa GetEmenta(long disciplinaUID, int periodo)
        {
            using (idbufmtEntities db = new idbufmtEntities())
            {
                vwEmentas vwementa =
                db.vwEmentas.
                    Where(e => e.codigoDisciplina == disciplinaUID && e.periodo == periodo)
                    .Where(e => e.ativa == true)
                    .FirstOrDefault();

                if (vwementa != null)
                {
                    Ementa ementa = new Ementa()
                    {
                        ativa = vwementa.ativa.Value,
                        codigoDisciplina = vwementa.codigoDisciplina.Value,
                        cursoSiga = vwementa.cursoSiga.Value,
                        ementaDisciplina = vwementa.ementaDisciplina,
                        ementaUID = vwementa.ementaUID,
                        periodo = vwementa.periodo.Value
                    };

                    return ementa;
                }
                else
                {
                    return null;
                }
            }
        }

        public Ementa GetLastEmenta(long disciplinaUID)
        {
            using (idbufmtEntities db = new idbufmtEntities())
            {
                vwEmentas vwementa =
                db.vwEmentas.
                    Where(e => e.codigoDisciplina == disciplinaUID)
                    .Where(e => e.ativa == true)
                    .OrderByDescending(e => e.periodo)
                    .FirstOrDefault();
                
                if (vwementa != null)
                {
                    Ementa ementa = new Ementa()
                    {
                        ativa = vwementa.ativa.Value,
                        codigoDisciplina = vwementa.codigoDisciplina.Value,
                        cursoSiga = vwementa.cursoSiga.Value,
                        ementaDisciplina = vwementa.ementaDisciplina,
                        ementaUID = vwementa.ementaUID,
                        periodo = vwementa.periodo.Value
                    };
                    return ementa;
                }
                else
                {
                    return null;
                }                
            }
        }


        public List<DisciplinaEntity> GetDisciplinasGraduacaoPorMatrizCurricular(long matrizCurricularUID)
        {
            List<DisciplinaEntity> disciplinas = new List<DisciplinaEntity>();
            using (ICursoBO cursoBO = BusinessFactory.GetInstance<ICursoBO>())
            {

                ufmt.sig.entity.MatrizCurricular matrizCurricular = cursoBO.GetMatrizCurricular(matrizCurricularUID);
                
                int? campusUID = null;
                if(matrizCurricular.Curso.UnidadeOfertante != null) {
                    campusUID = matrizCurricular.Curso.UnidadeOfertante.Campus.CampusUID;
                }

                int? nivelCursoUID = null;
                if(matrizCurricular.Curso.NivelCurso != null) {
                    nivelCursoUID = matrizCurricular.Curso.NivelCurso.NivelCursoUID;
                }

                IList<ufmt.sig.entity.MatrizDisciplinar> matrizDisciplinar = 
                    cursoBO.GetMatrizesDisciplinares(campusUID,
                    nivelCursoUID,
                    matrizCurricular.Curso.CursoUID, matrizCurricularUID, null, null);
                
                foreach (ufmt.sig.entity.MatrizDisciplinar matriz in matrizDisciplinar)
                {
                    DisciplinaEntity disciplina = new DisciplinaEntity()
                    {
                        MatrizDisciplinaUID = matriz.MatrizDisciplinarUID,
                        Nome = matriz.Nome,
                        PeriodoInicialFuncionamento = matriz.PeriodoInicialFuncionamento,
                        PeriodoFinalFuncionamento = matriz.PeriodoFinalFuncionamento,
                        TipoDisciplina = matriz.TipoDisciplina.Descricao,
                        
                        MatrizCurricularUID = matriz.MatrizCurricular.MatrizCurricularUID,
                        Ementa = matriz.Ementa,
                        CargaHorariaTotal = matriz.CargaHorariaTotal,
                        CargaHorariaTeorica = matriz.CargaHorariaTeorica,
                        CargaHorariaPraticaComponente = matriz.CargaHorariaPraticaComponente,
                        CargaHorariaPratica = matriz.CargaHorariaPratica,
                        CodigoExterno = matriz.CodigoExterno
                    };

                    if (matriz.UnidadeOfertante != null)
                        disciplina.UnidadeOfertanteUID = matriz.UnidadeOfertante.UnidadeUID;

                    if (matriz.TipoOfertaDisciplinar != null)
                        disciplina.TipoOfertaDisciplinar = matriz.TipoOfertaDisciplinar.Descricao;
                    
                    disciplinas.Add(disciplina);
                }
            }
            
            return disciplinas;
        }

        public DisciplinaEntity GetDisciplinaPorCodigoExternoPeriodo(long codigoExterno, int periodo) 
        {
            try
            {
                using (idbufmtEntities db = new idbufmtEntities())
                {
                    MatrizDisciplinar matriz = db.MatrizDisciplinar
                                                .Where(d => d.codigoExterno == codigoExterno)
                                                //.Where(d => d.periodoInicialFuncionamento == periodo)
                                                .FirstOrDefault();

                    ufmt.sig.entity.MatrizDisciplinar matrizSig = new sig.entity.MatrizDisciplinar();
                    using (ICursoBO cursoBO = BusinessFactory.GetInstance<ICursoBO>())
                    {
                        matrizSig = cursoBO.GetMatrizDisciplinar(matriz.matrizDisciplinarUID);
                    }
                    DisciplinaEntity disciplina = new DisciplinaEntity()
                    {
                        MatrizDisciplinaUID = matrizSig.MatrizDisciplinarUID,
                        Nome = matrizSig.Nome,
                        PeriodoInicialFuncionamento = matrizSig.PeriodoInicialFuncionamento,
                        PeriodoFinalFuncionamento = matrizSig.PeriodoFinalFuncionamento,
                        TipoDisciplina = matrizSig.TipoDisciplina.Descricao,

                        MatrizCurricularUID = matrizSig.MatrizCurricular.MatrizCurricularUID,
                        Ementa = matrizSig.Ementa,
                        CargaHorariaTotal = matrizSig.CargaHorariaTotal,
                        CargaHorariaTeorica = matrizSig.CargaHorariaTeorica,
                        CargaHorariaPraticaComponente = matrizSig.CargaHorariaPraticaComponente,
                        CargaHorariaPratica = matrizSig.CargaHorariaPratica,
                        CodigoExterno = matrizSig.CodigoExterno
                    };

                    return disciplina;
                }
            } 
            catch (Exception ex) {
                throw new Exception(ex.Message);
            }
        }


        public List<DisciplinaAlunoEntity> GetDisciplinasCursadas(string rga)
        {
            try
            {
                using (idbufmtEntities db = new idbufmtEntities())
                {
                    decimal drga = decimal.Parse(rga);
                    List<ViewDisciplinaAlunoSiga> disciplinasAluno 
                        = db.ViewDisciplinaAlunoSiga.Where(h => h.rga == drga && h.observacao.Equals("ma")).ToList();

                    List<DisciplinaAlunoEntity> disciplinaList = new List<DisciplinaAlunoEntity>();

                    foreach (ViewDisciplinaAlunoSiga disciplinaAluno in disciplinasAluno)
                    {
                        DisciplinaEntity disciplina = 
                            this.GetDisciplinaPorCodigoExternoPeriodo((long)disciplinaAluno.codigoDisciplina, 
                                                                        disciplinaAluno.periodo);

                        DisciplinaAlunoEntity disciplinaAlunoEntity = new DisciplinaAlunoEntity()
                        {
                            Discplina = disciplina,
                            Periodo = disciplinaAluno.periodo,
                            CodigoCursoExterno = disciplinaAluno.codigoCurso,
                            Turma = disciplinaAluno.turma
                        };
                        
                        disciplinaList.Add(disciplinaAlunoEntity);
                    }

                    return disciplinaList;
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }

        }

        public List<DisciplinaAlunoEntity> GetDisciplinasAluno(string rga, string situacao)
        {
            try
            {
                using (idbufmtEntities db = new idbufmtEntities())
                {
                    decimal drga = decimal.Parse(rga);
                    List<ViewDisciplinaAlunoSiga> disciplinasAluno
                        = db.ViewDisciplinaAlunoSiga.Where(h => h.rga == drga && h.observacao.Equals(situacao)).ToList();

                    List<DisciplinaAlunoEntity> disciplinaList = new List<DisciplinaAlunoEntity>();

                    foreach (ViewDisciplinaAlunoSiga disciplinaAluno in disciplinasAluno)
                    {
                        DisciplinaEntity disciplina =
                            this.GetDisciplinaPorCodigoExternoPeriodo((long)disciplinaAluno.codigoDisciplina,
                                                                        disciplinaAluno.periodo);

                        DisciplinaAlunoEntity disciplinaAlunoEntity = new DisciplinaAlunoEntity()
                        {
                            Discplina = disciplina,
                            Periodo = disciplinaAluno.periodo,
                            CodigoCursoExterno = disciplinaAluno.codigoCurso,
                            Turma = disciplinaAluno.turma
                        };

                        disciplinaList.Add(disciplinaAlunoEntity);
                    }

                    return disciplinaList;
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }

        }


        public List<PessoaEntity> GetProfessoresPorDisciplina(string codigoDisciplinaExterno, string turma, int periodo, int codigoCursoExterno)
        {
            try
            {
                using (idbufmtEntities db = new idbufmtEntities())
                {
                    int codigoDisciplina = int.Parse(codigoDisciplinaExterno);
                    List<ViewDisciplinaProfessorSiga> professorDisciplinaList = 
                        db.ViewDisciplinaProfessorSiga.Where(p => p.disciplina == codigoDisciplina)
                                                      .Where(p => p.turma == turma)
                                                      .Where(p => p.periodo == periodo)
                                                      .Where(p => p.cursoSiga == codigoCursoExterno)
                                                      .ToList();

                    List<PessoaEntity> pessoaEntityList = new List<PessoaEntity>();
                    foreach (ViewDisciplinaProfessorSiga professorDisciplina in professorDisciplinaList)
                    {
                        Pessoa pessoaService = new Pessoa();
                        PessoaEntity pessoa = pessoaService.CastPessoaToPessoaEntity(
                                                            pessoaService.GetPessoaPorServidor(professorDisciplina.servidorUID)
                                                            );
                        pessoaEntityList.Add(pessoa);
                    }

                    return pessoaEntityList;
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }


        public DisciplinaEntity GetDisciplina(long matrizDisciplinarUID)
        {
            try
            {
                using (ICursoBO cursoBO = BusinessFactory.GetInstance<ICursoBO>())
                {
                    ufmt.sig.entity.MatrizDisciplinar matrizSig = cursoBO.GetMatrizDisciplinar(matrizDisciplinarUID);
                    DisciplinaEntity disciplina = this.ConvertMatrizDisciplinarToDisciplinaEntity(matrizSig);
                    return disciplina;
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }

        private DisciplinaEntity ConvertMatrizDisciplinarToDisciplinaEntity(ufmt.sig.entity.MatrizDisciplinar matrizSig)
        {
            DisciplinaEntity disciplina = new DisciplinaEntity()
            {
                MatrizDisciplinaUID = matrizSig.MatrizDisciplinarUID,
                Nome = matrizSig.Nome,
                PeriodoInicialFuncionamento = matrizSig.PeriodoInicialFuncionamento,
                PeriodoFinalFuncionamento = matrizSig.PeriodoFinalFuncionamento,
                TipoDisciplina = matrizSig.TipoDisciplina.Descricao,

                MatrizCurricularUID = matrizSig.MatrizCurricular.MatrizCurricularUID,
                Ementa = matrizSig.Ementa,
                CargaHorariaTotal = matrizSig.CargaHorariaTotal,
                CargaHorariaTeorica = matrizSig.CargaHorariaTeorica,
                CargaHorariaPraticaComponente = matrizSig.CargaHorariaPraticaComponente,
                CargaHorariaPratica = matrizSig.CargaHorariaPratica,
                CodigoExterno = matrizSig.CodigoExterno
            };

            return disciplina;
        }
    }
}
