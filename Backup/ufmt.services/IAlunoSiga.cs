﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ufmt.services.entities;
using System.ServiceModel.Web;

namespace ufmt.services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAlunoSiga" in both code and config file together.
    [ServiceContract]
    public interface IAlunoSiga
    {
        [OperationContract]
        Aluno GetInfoAluno(string rga);

        [OperationContract]
        List<Aluno> GetAlunos();

        [OperationContract]
        List<Aluno> GetAlunosPorCampus(int campusUID);

        [OperationContract]
        List<Aluno> GetAlunosGraduacao();

        [OperationContract]
        List<Aluno> GetAlunosGraduacaoPorCampus(int campusUID);

        [OperationContract]
        List<Aluno> GetAlunosPaginado(int registroInicial, int quantidadeRegistros);

        [OperationContract]
        List<Aluno> GetAlunosPorCampusPaginado(int campusUID, int registroInicial, int quantidadeRegistros);

        [OperationContract]
        double GetCoeficiente(string rga);
    }


}
