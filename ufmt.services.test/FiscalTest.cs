﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ufmt.services.test
{
    [TestClass]
    public class FiscalTest
    {
        [TestMethod]
        public void TestGetFiscalInformcao()
        {
            Fiscal fiscal = new Fiscal();
            var retorno = fiscal.ConsultaInscricao("76265803115");
            Assert.IsTrue(retorno.Count > 0);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException))]
        public void TestGetFiscalInformacaoInvalido()
        {
            Fiscal fiscal = new Fiscal();
            var retorno = fiscal.ConsultaInscricao("00000000000");
        }
    }
}
