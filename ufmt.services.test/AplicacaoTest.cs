﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.ServiceModel;

namespace ufmt.services.test
{
    [TestClass]
    public class AplicacaoTest
    {
        [TestMethod]
        public void TestGetAplicacaoPorUID()
        {
            Aplicacao app = new Aplicacao();
            var retorno = app.GetAplicacao(1);
            Assert.AreEqual(1, retorno.AplicacaoUID);
        }

        /// <summary>
        /// Teste para verificar se existem registros na tabela aplicacao
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(FaultException))]
        public void TestGetApplicacaoPorUIDInvalido()
        {
            Aplicacao app = new Aplicacao();
            var retorno = app.GetAplicacao(0);
        }

        [TestMethod]
        public void TestGetAplicacoes()
        {
            Aplicacao app = new Aplicacao();
            var retorno = app.GetTodasAplicacoes();
            Assert.IsTrue(retorno.Count > 0);
        }
        
        /// <summary>
        /// Teste para verificar se existem Aplicações para uma determinada Categoria
        /// </summary>
        [TestMethod]
        public void TestGetAplicacaoPorCategoria()
        {
            Aplicacao ap = new Aplicacao();
            var categoria = ap.GetAplicacaoPorCategoria(1,1);
            Assert.IsTrue(categoria.Count > 0);
        }
    }
}
