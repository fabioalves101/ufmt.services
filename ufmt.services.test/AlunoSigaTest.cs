﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.ServiceModel;

namespace ufmt.services.test
{
    [TestClass]
    public class AlunoSigaTest
    {
        #region Validações de Aluno
        [TestMethod]
        [TestCategory("Validações de Aluno")]
        public void TestRga()
        {
            string rga = "198219000001";
            AlunoSiga s = new AlunoSiga();
            var aluno = s.GetInfoAluno(rga);
            Assert.AreEqual(rga, aluno.Rga);
        }

        [TestMethod]
        [TestCategory("Validações de Aluno")]
        [ExpectedException(typeof(FormatException))]
        public void TestRgaInvalido()
        {
            string rga = "abc";
            AlunoSiga s = new AlunoSiga();
            var aluno = s.GetInfoAluno(rga);
        }

        [TestMethod]
        [TestCategory("Validações de Aluno")]
        [ExpectedException(typeof(FaultException))]
        public void TestRgaVazio()
        {
            string rga = "201699999999";
            AlunoSiga s = new AlunoSiga();
            var aluno = s.GetInfoAluno(rga);
        }
        #endregion

        #region Validações de Coeficiente

        [TestMethod]
        [TestCategory("Validações de Coeficiente")]
        public void TestCoeficienteAluno()
        {
            string rga = "198219000001";
            AlunoSiga s = new AlunoSiga();
            var coeficiente = s.GetCoeficiente(rga);
            Assert.IsTrue(coeficiente > 0);
        }

        [TestMethod]
        [TestCategory("Validações de Coeficiente")]
        public void TestCoeficienteAlunoRgaFalso()
        {
            string rga = "201699999999";
            AlunoSiga s = new AlunoSiga();
            var coeficiente = s.GetCoeficiente(rga);
            Assert.IsFalse(coeficiente > 0);
        }

        [TestMethod]
        [TestCategory("Validações de Coeficiente")]
        [ExpectedException(typeof(FormatException))]
        public void TestCoeficienteAlunoRgaVazio()
        {
            string rga = "";
            AlunoSiga s = new AlunoSiga();
            var coeficiente = s.GetCoeficiente(rga);
        }
        #endregion

        public void TestGetAlunosPorNome()
        {
            AlunoSiga aluno = new AlunoSiga();
            int quantidadeTotal = 0;
            var alunos = aluno.GetAlunosPorNome("LUIZ", 1, 5, 11234, out quantidadeTotal);
            Assert.IsTrue(alunos.Count > 0);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException))]
        public void TestGetAlunosPorNomeVazio()
        {
            AlunoSiga s = new AlunoSiga();
            int quantidadeTotal = 0;
            var coeficiente = s.GetAlunosPorNome("", 1, 5, 11234, out quantidadeTotal);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException))]
        public void TestGetAlunosPorRegistroInicial()
        {
            AlunoSiga s = new AlunoSiga();
            int quantidadeTotal = 0;
            var coeficiente = s.GetAlunosPorNome("Luiz", -1, 5, 11234, out quantidadeTotal);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException))]
        public void TestGetAlunosPorQuantidadeInicial()
        {
            AlunoSiga s = new AlunoSiga();
            int quantidadeTotal = 0;
            var coeficiente = s.GetAlunosPorNome("Luiz", 1, -5, 11234, out quantidadeTotal);
        }
    }
}
