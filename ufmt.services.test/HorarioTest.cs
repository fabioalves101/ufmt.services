﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ufmt.services.test
{
    [TestClass]
    public class HorarioTest
    {
        [TestMethod]
        public void TestGetHorarios() {
            Horario horario = new Horario();
            var retorno = horario.GetHorario("201211117007");
            Assert.IsTrue(retorno.Count > 0);
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException))]
        public void TestGetHorarioRgaInvalido() {
            Horario horario = new Horario();
            var retorno = horario.GetHorario("111111111111");
        }
    }
}
