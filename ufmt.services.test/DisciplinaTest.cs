﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ufmt.services.test
{
    /// <summary>
    /// Classe de teste para a Classe Disciplina
    /// </summary>
    [TestClass]
    public class DisciplinaTest
    {
        /// <summary>
        /// Metodo para realizar o teste do Metodo GetDisciplinasCursadas
        /// </summary>
        [TestMethod]
        public void GetDisciplinasCursadasTest()
        {
            Disciplina d = new Disciplina();
            // var retorno = d.GetDisciplinasCursadas("201321801039"); Producao
            var retorno = d.GetDisciplinasCursadas("201323140002"); // Teste
            Assert.IsTrue(retorno.Count > 0);
        }
    }
}
