﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace ufmt.services.test
{
    [TestClass]
    public class AutenticacaoTest
    {
        #region LoginPorSituacao
        [TestMethod]
        public void TestLoginPorSituacao()
        {
            Autenticacao aut = new Autenticacao();
            string usuario = "02000791158";
            string senha = "123456";
            var retorno = aut.LoginPorSituacao(usuario, senha, new SituacaoDoServidor[] { SituacaoDoServidor.Ativo, SituacaoDoServidor.Inativo });
            Assert.AreEqual(usuario, retorno.Pessoa.Cpf);
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void TestLoginPorSituacaoFalha()
        {

            Autenticacao aut = new Autenticacao();
            string usuario = "02000791158";
            string senha = "654321";
            var retorno = aut.LoginPorSituacao(usuario, senha, new SituacaoDoServidor[] { SituacaoDoServidor.Ativo, SituacaoDoServidor.Inativo });

        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void TestLoginPorSituacaoUsuarioInexistente()
        {
            Autenticacao aut = new Autenticacao();
            string usuario = "34078991149";
            string senha = "123";
            var retorno = aut.LoginPorSituacao(usuario, senha, new SituacaoDoServidor[] { SituacaoDoServidor.Ativo, SituacaoDoServidor.Inativo });
        }

        [TestMethod]
        [ExpectedException(typeof(FaultException))]
        public void TestLoginPorSituacaoCPFInvalido()
        {
            Autenticacao aut = new Autenticacao();
            string usuario = "11122233344";
            string senha = "123";
            var retorno = aut.LoginPorSituacao(usuario, senha, new SituacaoDoServidor[] { SituacaoDoServidor.Ativo, SituacaoDoServidor.Inativo });
        }

        [TestMethod]
        [ExpectedException(typeof(Exception))]
        public void TestLoginPorSituacaoUsuarioComSenhaExpirada()
        {
            Autenticacao aut = new Autenticacao();
            string usuario = "15443194879";
            string senha = "123456";
            var retorno = aut.LoginPorSituacao(usuario, senha, new SituacaoDoServidor[] { SituacaoDoServidor.Ativo, SituacaoDoServidor.Inativo });

        }

        [TestMethod]
        [ExpectedException(typeof(FaultException))]
        public void TestLoginPorSituacaoCPFTamanhoInvalido()
        {
            Autenticacao aut = new Autenticacao();
            string usuario = "111222333";
            string senha = "123456";
            var retorno = aut.LoginPorSituacao(usuario, senha, new SituacaoDoServidor[] { SituacaoDoServidor.Ativo, SituacaoDoServidor.Inativo });
        }

        #endregion
    }
}
