﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ufmt.services.entities;

namespace ufmt.services
{
    /// <summary>
    /// Serviço de solicitação de auxilio
    /// </summary>
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Auxilio" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Auxilio.svc or Auxilio.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class Auxilio : IAuxilio
    {
        /// <summary>
        /// Pesquisa de Solicitação e renovação por ano 
        /// </summary>
        /// <param name="ano"> ano da solitação de renovação </param>
        /// <param name="semestre">  semestre da solicitação de renovação a ser pesquisada </param>
        /// <returns> Retorna uma lista com dados das soliciatações de renovação  </returns>
        public List<SolicitacaoRenovacaoEntity> GetSolicitacaoRenovacaoPorSemestre(string ano, int semestre)
        {
            try
            {
                //throw new NotImplementedException();

                var result = new List<SolicitacaoRenovacaoEntity>();

                using (var db = new idbufmtEntities())
                {
                    var renovacoes = db.Renovacao.Where(x => x.ano == ano && x.semestre == semestre);

                    foreach (var r in renovacoes)
                    {
                        var solicitacoes = r.SolicitacaoRenovacao;

                        foreach (var sr in solicitacoes)
                        {
                            result.Add(new SolicitacaoRenovacaoEntity(db, sr));
                        }
                    }
                }

                return result;
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }
    }
}
