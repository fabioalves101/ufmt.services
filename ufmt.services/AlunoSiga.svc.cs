﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ufmt.services.entities;
using ufmt.services.EntityModels;
using System.ServiceModel.Activation;
using System.Linq.Dynamic;

namespace ufmt.services
{
    /// <summary>
    /// WebService que disponibiliza as Informações de Aluno Tradicional
    /// </summary>
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AlunoSiga" in code, svc and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class AlunoSiga : IAlunoSiga
    {
        /// <summary>
        /// Busca de alunos por rga
        /// </summary>
        /// <param name="rga"> Numero de matrícula do aluno a ser pesquisado </param>
        /// <returns> Retorna uma instancia de aluno</returns>
        public entities.Aluno GetInfoAluno(string rga)
        {

            using (idbufmtEntities contexto = new idbufmtEntities())
            {
                decimal matricula = decimal.Parse(rga);

                vwAlunoSiga al = 
                (from c in contexto.vwAlunoSiga
                 where c.Matricula == matricula
                 select c).FirstOrDefault();
                
                if (al != null)
                {
                    ufmt.services.entities.Aluno aluno = new ufmt.services.entities.Aluno(al);

                    return aluno;
                }
                else
                {
                    throw new FaultException("Nenhum aluno encontrado com os dados informados");
                }
            }
        }
        
        /// <summary>
        /// Pesquisa de alunos inativos por rga
        /// </summary>
        /// <param name="rga"> Numero de matricula do aluno a ser pesquisado</param>
        /// <returns> retorna uma instancia de aluno inativo</returns>
        public entities.Aluno GetInfoAlunoInativo(string rga)
        {
            using (var db = new DbSigaEntities())
            {
                if (!string.IsNullOrEmpty(rga))
                {
                    var rgaDecimal = decimal.Parse(rga);
                    var aluno = db.SIGAALUNO.Where(x => x.SigaRgaAluno == rgaDecimal).ToList().FirstOrDefault();

                    var curso = db.SIGACURSO.Where(x => x.SigaCodCurso == aluno.SigaCursoAluno).FirstOrDefault();

                    var campi = db.SIGACAMPUS.Where(x => x.SigaCodCampus == curso.SigaCampusCurso).FirstOrDefault();

                    if (aluno != null)
                    {
                        ufmt.services.entities.Aluno al = new ufmt.services.entities.Aluno(aluno, curso, campi);

                        return al;
                    }
                    else
                    {
                        throw new FaultException("Nenhum aluno encontrado com os dados informados");
                    }
                }
                else
                {
                    throw new FaultException("RGA não pode ser vazio");
                }
            }
        }
        
        /// <summary>
        /// Lista todos os alunos ativos 
        /// </summary>
        /// <returns> Retorna uma lista de alunos </returns>
        public List<entities.Aluno> GetAlunos()
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {

                    IQueryable<Aluno> alunos =
                        (from c in contexto.GetDadosAlunoGeral
                         select new Aluno()
                         {
                             CampusUID = c.campusUID,
                             Cpf = c.cpf,
                             Curso = c.curso,
                             //DataNascimento = c.dataNascimento,
                             Email = c.email,
                             NomeAluno = c.nomeAluno,
                             Matricula = c.rga
                         });

                    if (alunos != null)
                    {
                        return alunos.ToList();
                    }
                    else
                    {
                        string msg = "Nenhum aluno encontrado";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        ///  Lista de todos os alunos de Graduação
        /// </summary>
        /// <returns>Retorna uma lista de alunos de Graduação </returns>
        public List<entities.Aluno> GetAlunosGraduacao()
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {

                    IQueryable<Aluno> alunos =
                        (from c in contexto.GetDadosAluno
                         select new Aluno()
                         {
                             CampusUID = c.campusUID,
                             Cpf = c.cpf,
                             Curso = c.curso,
                             DataNascimento = DateTime.Parse(c.dataNascimento),
                             Email = c.email,
                             NomeAluno = c.nomeAluno,
                             Matricula = c.rga
                         });

                    if (alunos != null)
                    {
                        return alunos.ToList();
                    }
                    else
                    {
                        string msg = "Nenhum aluno encontrado";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        /// Pesquisa de todos os alunos por campus
        /// </summary>
        /// <param name="campusUID"> IUD do campus a ser pesquisado </param>
        /// <returns>retorna uma lista de alunos por campus</returns>
        public List<entities.Aluno> GetAlunosPorCampus(int campusUID)
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {
                    IQueryable<Aluno> alunos =
                        (from c in contexto.GetDadosAlunoGeral
                         where c.campusUID == campusUID
                         select new Aluno()
                         {
                             NomeCampus = c.campus,
                             CampusUID = c.campusUID,
                             Cpf = c.cpf,
                             Curso = c.curso,
                             //DataNascimento = c.dataNascimento,
                             Email = c.email,
                             NomeAluno = c.nomeAluno,
                             Matricula = c.rga,
                             CodigoCurso = c.codidoCurso
                         });

                    if (alunos != null)
                    {
                        return alunos.ToList();
                    }
                    else
                    {
                        string msg = "Nenhum aluno encontrado";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        /// Pesquisa de alunos de Graduação por campus
        /// </summary>
        /// <param name="campusUID"> UID do campus a ser pesquisado </param>
        /// <returns> Retorna uma lista de alunos de graduação por campus</returns>
        public List<entities.Aluno> GetAlunosGraduacaoPorCampus(int campusUID)
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {

                    IQueryable<Aluno> alunos =
                        (from c in contexto.GetDadosAluno
                         where c.campusUID == campusUID
                         select new Aluno()
                         {
                             CampusUID = c.campusUID,
                             Cpf = c.cpf,
                             Curso = c.curso,
                             DataNascimento = DateTime.Parse(c.dataNascimento),
                             Email = c.email,
                             NomeAluno = c.nomeAluno,
                             Matricula = c.rga
                         });

                    if (alunos != null)
                    {
                        return alunos.ToList();
                    }
                    else
                    {
                        string msg = "Nenhum aluno encontrado";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        ///  Pesquisa de alunos paginados
        /// </summary>
        /// <param name="registroInicial"> Registro inicial</param>
        /// <param name="quantidadeRegistros"> Quantidade de registro</param>
        /// <returns> Retorna uma lista de alunos paginados</returns>
        public List<entities.Aluno> GetAlunosPaginado(int registroInicial, int quantidadeRegistros)
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {

                    IQueryable<Aluno> alunos =
                        (from c in contexto.GetDadosAlunoGeral.OrderBy(c => c.rga).Skip(registroInicial).Take(quantidadeRegistros)
                         select new Aluno()
                         {
                             CampusUID = c.campusUID,
                             Cpf = c.cpf,
                             Curso = c.curso,
                             //DataNascimento = c.dataNascimento,
                             Email = c.email,
                             NomeAluno = c.nomeAluno,
                             Matricula = c.rga
                         });

                    if (alunos != null)
                    {
                        return alunos.ToList();
                    }
                    else
                    {
                        string msg = "Nenhum aluno encontrado";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        /// pesquisa de alunos por campus paginados
        /// </summary>
        /// <param name="campusUID">UID do campus a ser pesquisado </param>
        /// <param name="registroInicial"> resgitro inicial a ser pesquisado </param>
        /// <param name="quantidadeRegistros"> quantidade de registros a ser pesquisados </param>
        /// <returns> Retorna uma lista de alunos paginados por campus </returns>
        public List<entities.Aluno> GetAlunosPorCampusPaginado(int campusUID, int registroInicial, int quantidadeRegistros)
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {

                    IQueryable<Aluno> alunos =
                        (from c in contexto.GetDadosAlunoGeral
                             .Where(c => c.campusUID == campusUID)
                             .OrderBy(c => c.rga)
                             .Skip(registroInicial)
                             .Take(quantidadeRegistros)

                         select new Aluno()
                         {
                             CampusUID = c.campusUID,
                             Cpf = c.cpf,
                             Curso = c.curso,
                             //DataNascimento = c.dataNascimento,
                             Email = c.email,
                             NomeAluno = c.nomeAluno,
                             Matricula = c.rga
                         });

                    if (alunos != null)
                    {
                        return alunos.ToList();
                    }
                    else
                    {
                        string msg = "Nenhum aluno encontrado";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        ///  Calcula o coenficiente de redimento do aluno
        /// </summary>
        /// <param name="rga"> Numero de Matricula do aluno a ser pesquisado</param>
        /// <returns> Retorna coeficiente de rendimento do aluno pesquisado</returns>
        public double GetCoeficiente(string rga)
        {
            using (idbufmtEntities contexto = new idbufmtEntities())
            {
                decimal drga = decimal.Parse(rga);
                SP_CALCULO_COEFICIENTE_RENDIMENTO_ALUNO_Result coeficiente =
                    contexto.SP_CALCULO_COEFICIENTE_RENDIMENTO_ALUNO(drga).FirstOrDefault();
                if (coeficiente.COEFICIENTE_RENDIMENTO.HasValue)
                    return coeficiente.COEFICIENTE_RENDIMENTO.Value;
                else
                    return 0d;
            }
        }
        
        /// <summary>
        /// Pesquisa de aluno por cpf
        /// </summary>
        /// <param name="cpf"> cpf do aluno a ser pesquisado</param>
        /// <returns> Retorna uma instancia de alunos ativos por cpf </returns>
        public List<Aluno> GetAlunosPorCpf(string cpf)
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {
                    List<vwAlunoSiga> gdAlunos =
                        (from c in contexto.vwAlunoSiga
                         where c.CPF == cpf
                               && !c.Situacao.StartsWith("E")
                         select c).ToList();

                    if (gdAlunos.Count > 0)
                    {
                        List<ufmt.services.entities.Aluno> alunos = new List<Aluno>();
                        foreach (vwAlunoSiga al in gdAlunos)
                        {
                            alunos.Add(new ufmt.services.entities.Aluno(al));
                        }

                        return alunos;
                    }
                    else
                    {
                        string msg = "Nenhum aluno encontrado com os dados informados";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        ///  pesquisa alunos inativos por cpf
        /// </summary>
        /// <param name="cpf"> Número de cpf do aluno a ser pesquisado </param>
        /// <returns> Retorna lista de aluno inativos por cpf</returns>
        public List<Aluno> GetAlunosInativosPorCpf(string cpf)
        {
            try
            {
                List<Aluno> alunos = new List<Aluno>();
                using (var db = new DbSigaEntities())
                {
                    var alunosInativos = db.SIGAALUNO.Where(x => x.SigaCPFAluno == cpf);

                    if (alunosInativos != null)
                    {
                        foreach (var item in alunosInativos)
                        {
                            var curso = db.SIGACURSO.Where(x => x.SigaCodCurso == item.SigaCursoAluno).FirstOrDefault();
                            var campi = db.SIGACAMPUS.Where(x => x.SigaCodCampus == curso.SigaCampusCurso).FirstOrDefault();
                            Aluno a = new Aluno(item, curso, campi);
                            alunos.Add(a);
                        }
                        return alunos;
                    }
                    else
                    {
                        throw new FaultException("Não foi possível encontrar Aluno com o CPF informado.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new FaultException("Falha ao Pesquisar por CPF. " + ex.Message.ToString());
            }
        }
        
        /// <summary>
        ///  Consulta genérica de estudantes 
        /// </summary>
        /// <param name="nomeParametro">Nome do estudante a ser pesquisado</param>
        /// <param name="valorParametro">Valor a ser pesquisado </param>
        /// <param name="registroInicial">Registro inicial a ser pesquisado </param>
        /// <param name="quantidadeDeRegistros"> Quantidade de registros</param>
        /// <param name="campusUID">UID do campus a ser pesquisado</param>
        /// <returns>Retorna uma lista de estudantes </returns>
        public List<Aluno> ConsultaGenerica(ParametrosEstudante nomeParametro, string valorParametro, int registroInicial, int quantidadeDeRegistros, int? campusUID)
        {
            try
            {
                throw new FaultException("Método descontinuado, por favor utilize algumas das consultas específicas disponíveis ou entre em contato com os desenvolvedores.");

                if (string.IsNullOrEmpty(valorParametro)) { throw new FaultException("Valor do parâmetro não pode ser vazio."); }

                if (registroInicial < 0) { throw new FaultException("Registro inicial não pode ser negativo."); }

                if (quantidadeDeRegistros <= 0) { throw new FaultException("Quantidade de Registros deve ser maior que zero."); }

                string expressao = string.Empty;

                if (nomeParametro == ParametrosEstudante.nome)
                {
                    //return GetAlunosPorNome(valorParametro, registroInicial, quantidadeDeRegistros, campusUID);
                }
                else
                {
                    expressao = nomeParametro.ToString() + " = " + valorParametro;
                }

                if (campusUID.HasValue)
                {
                    expressao = expressao + " AND campusUID = " + campusUID.Value.ToString();
                }

                using (var db = new idbufmtEntities())
                {
                    //string nomeRealDoParametro = string.Empty;
                    //switch (nomeParametro)
                    //{
                    //    case ParametrosEstudante.rga:
                    //    case ParametrosEstudante.cpf:
                    //    case ParametrosEstudante.rg:
                    //        {
                    //            nomeRealDoParametro = nomeParametro.ToString();
                    //            expressao = nomeRealDoParametro + " = " + valorParametro;
                    //            break;
                    //        }
                    //    case ParametrosEstudante.nome:
                    //        {
                    //            return GetAlunosPorNome(valorParametro, registroInicial, quantidadeDeRegistros);
                    //            //nomeRealDoParametro = "nomeAluno";
                    //            //expressao = nomeRealDoParametro + " LIKE '%" + valorParametro + "%'";
                    //            //break;
                    //        }
                    //    default:
                    //        {
                    //            throw new FaultException("Parâmetro desconhecido.");
                    //        }
                    //}

                    var resultadoPesquisa = db.GetDadosAluno
                        .OrderBy(nomeParametro.ToString()).Where(expressao)
                        .Skip(registroInicial).Take(quantidadeDeRegistros).ToList();

                    if (resultadoPesquisa != null)
                    {
                        var resultado = new List<Aluno>();

                        foreach (var a in resultadoPesquisa)
                            resultado.Add(new Aluno(a));

                        return resultado;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        /// Peaquisa de alunos por nome
        /// </summary>
        /// <param name="nome"> Nome do aluno a ser pesquisado </param>
        /// <param name="registroInicial">Resgistro inicial a ser pesquisado</param>
        /// <param name="quantidadeDeRegistros">Quantidade de registros a ser pesquisado </param>
        /// <param name="campusUID"> UID do campus a ser pesquisado</param>
        /// <param name="quantidadeTotal"> Quantidade total a ser pesquisado</param>
        /// <returns> Retorna uma lista de alunos ativos </returns>
        public List<Aluno> GetAlunosPorNome(string nome, int registroInicial, int quantidadeDeRegistros, int? campusUID, out int quantidadeTotal)
        {

            if (string.IsNullOrEmpty(nome))
            {
                throw new FaultException("Nome não pode ser vazio.");
            }

            if (registroInicial < 0)
            {
                throw new FaultException("Registro inicial não pode ser negativo.");
            }

            if (quantidadeDeRegistros <= 0)
            {
                throw new FaultException("Quantidade de Registros deve ser maior que zero.");
            }

            using (var db = new idbufmtEntities())
            {
                List<GetDadosAluno> list = null;

                if (campusUID.HasValue)
                {
                    list = db.GetDadosAluno.OrderBy(x => x.nomeAluno)
                                           .Where(x => x.campusUID == campusUID.Value &&
                                                       x.nomeAluno.Contains(nome))
                                           .Skip(registroInicial)
                                           .Take(quantidadeDeRegistros).ToList();

                    quantidadeTotal = db.GetDadosAluno.OrderBy(x => x.nomeAluno)
                                                      .Count(x => x.campusUID == campusUID.Value &&
                                                                  x.nomeAluno.Contains(nome));
                }
                else
                {
                    list = db.GetDadosAluno.OrderBy(x => x.nomeAluno)
                                           .Where(x => x.nomeAluno.Contains(nome))
                                           .Skip(registroInicial)
                                           .Take(quantidadeDeRegistros).ToList();

                    quantidadeTotal = db.GetDadosAluno.OrderBy(x => x.nomeAluno)
                                                      .Count(x => x.nomeAluno.Contains(nome));
                }

                if (list != null)
                {
                    var result = new List<Aluno>();

                    foreach (var aluno in list)
                    {
                        result.Add(new Aluno(aluno));
                    }

                    return result;
                }
                else
                {
                    return null;
                }
            }
        }
        
        /// <summary>
        /// Pesquisa de alunos inativos por nome
        /// </summary>
        /// <param name="nome"> Nome do aluno a ser pesquisado</param>
        /// <returns>Retorna lista de alunos inativos com os dados pesquisados </returns>
        public List<Aluno> GetAlunosInativosPorNome(string nome)
        {
            try
            {
                if (string.IsNullOrEmpty(nome))
                {
                    throw new FaultException("Nome não pode ser vazio.");
                }

                List<Aluno> alunos = new List<Aluno>();
                using (var db = new DbSigaEntities())
                {
                    var lista = db.SIGAALUNO.Where(x => x.SigaNomeAluno.Contains(nome));

                    if (lista != null)
                    {
                        foreach (var item in lista)
                        {
                            var curso = db.SIGACURSO.Where(x => x.SigaCodCurso == item.SigaCursoAluno).FirstOrDefault();

                            var campi = db.SIGACAMPUS.Where(x => x.SigaCodCampus == curso.SigaCampusCurso).FirstOrDefault();
                            Aluno a = new Aluno(item, curso, campi);
                            alunos.Add(a);
                        }

                        return alunos;
                    }
                    else
                    {
                        throw new FaultException("Nome não encontrado.");
                    }
                }
            }
            catch (Exception)
            {
                throw new FaultException("Falha ao pesquisar por nome.");
            }
        }
        
        /// <summary>
        /// Valida se o RGA informado é um RGA válido
        /// </summary>
        /// <param name="rga">RGA do Aluno a ser validado</param>
        private void EhUmRGADeEstudanteDeGraduacao(string rga)
        {
            if (rga.Any(x => !char.IsDigit(x))) { throw new Exception("RGA de Estudantes de Graduação deve conter somente dígitos."); }

            if (rga.Length != 12) { throw new Exception("RGA de Estudantes de Graduação deve conter doze dígitos."); }
        }
        
        /// <summary>
        /// Registra faltas de estudantes de graduação
        /// </summary>
        /// <param name="usuario"> Nome do usuario que está realizando a ação  </param>
        /// <param name="IP"> IP do usuário que está realizando a ação</param>
        /// <param name="rga"> Numero de Matricula do estudante </param>
        /// <param name="disciplina"> Disciplina em que foi registrada a falta</param>
        /// <param name="periodo">Periodo em que a falta foi registrada</param>
        /// <param name="turma"> Turma do estudante </param>
        /// <param name="faltas">Numero de faltas do estudante a serem gravadas</param>
        /// <returns>0 - atualização realizada 1 - fora do período para lançamento de nota/faltas 99 - erro não tratado</returns>
        public int GravaFaltasEstudanteGraduacao(string usuario, string IP, string rga, string disciplina, int periodo, string turma, int faltas)
        {
            try
            {
                int resultado;

                decimal rgaDecimal = decimal.Parse(rga);
                decimal disciplinaDecimal = decimal.Parse(disciplina);

                using (var db = new ufmt.services.EntityModels.DbSigaEntities())
                {
                    var _retorno = new System.Data.Objects.ObjectParameter("retorno", typeof(int));

                    db.ProcedureGravaFaltas(usuario, IP, rgaDecimal, disciplinaDecimal, periodo, turma, faltas, _retorno);

                    resultado = int.Parse(_retorno.Value.ToString());

                }

                return resultado;
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        /// Registra faltas dos estudantes de graduaçao 
        /// </summary>
        /// <param name="usuario">nome de usuario do que está realizando a ação</param>
        /// <param name="IP">IP do usuario que está realizando a ação </param>
        /// <param name="disciplina">Disciplina em que foi registrado a falta </param>
        /// <param name="periodo">Periodo que a falta foi resgistrada</param>
        /// <param name="turma">Turma do estudante</param>
        /// <param name="faltasPorEstudante">Número de faltas por estudante</param>
        /// <returns></returns>
        public ResultadoFaltasEstudante[] GravaFaltasEstudantesGraduacao(string usuario, string IP, string disciplina, int periodo, string turma, FaltasPorEstudante[] faltasPorEstudante)
        {
            try
            {
                //throw new NotImplementedException();

                var result = new List<ResultadoFaltasEstudante>();

                decimal disciplinaDecimal = decimal.Parse(disciplina);

                //using (var db = new ufmt.services.EntityModels.DbSigaEntities())
                //{
                //    foreach (var fe in faltasPorEstudante)
                //    {
                //        decimal rgaDecimal = decimal.Parse(fe.rga);

                //        //PRODUÇÃO, ATÉ QUE SEJA ATUALIZADA A PROCEDURE LÁ TAMBÉM
                //        //int r = db.ProcedureGravaFaltas(usuario, IP, rgaDecimal, disciplinaDecimal, periodo, turma, fe.faltas);

                //        var _retorno = new System.Data.Objects.ObjectParameter("retorno", typeof(int));

                //        db.ProcedureGravaFaltas(usuario, IP, rgaDecimal, disciplinaDecimal, periodo, turma, fe.faltas, _retorno);

                //        result.Add(new ResultadoFaltasEstudante() { rga = fe.rga, resultado = int.Parse(_retorno.Value.ToString()) });
                //    }
                //}

                using (var db = new DbSigaEntities())
                {
                    foreach (var fe in faltasPorEstudante)
                    {
                        decimal rgaDecimal = decimal.Parse(fe.rga);
                        var _retorno = new System.Data.Objects.ObjectParameter("retorno", typeof(int));

                        if (fe.nota < 0)
                        {
                            db.ProcedureGravaNotaFalta(usuario, IP, rgaDecimal, disciplinaDecimal, periodo, turma, fe.faltas, null, _retorno);
                        }

                        if (fe.faltas < 0)
                        {
                            db.ProcedureGravaNotaFalta(usuario, IP, rgaDecimal, disciplinaDecimal, periodo, turma, null, fe.nota, _retorno);
                        }


                        result.Add(new ResultadoFaltasEstudante() { rga = fe.rga, resultado = int.Parse(_retorno.Value.ToString()) });
                    }

                }

                return result.ToArray();
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        /// Desliga o aluno direto (Desliga o aluno solicitado )
        /// </summary>
        /// <param name="rga">RGA do Aluno a ser desligado</param>
        /// <param name="usuario">Nome do Usuario que está realizando a ação</param>
        /// <param name="ip">IP do Usuario que está realizando a ação</param>
        public void DesligaAlunoSiga(string rga, string usuario, string ip)
        {
            try
            {
                using (var db = new DbSigaEntities())
                {
                    System.Data.Objects.ObjectParameter erroTeste = new System.Data.Objects.ObjectParameter("Erro", 0);
                    var teste = db.DesligamentoVirtual(rga, usuario, ip, erroTeste);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            // Desliga o cara do sisu
            try
            {
                using (var db = new dbinscricaoEntities())
                {
                    System.Data.Objects.ObjectParameter erroTeste = new System.Data.Objects.ObjectParameter("Erro", 0);
                    var retorno = db.DesligamentoVirtual(rga, erroTeste);

                    switch (retorno)
                    {
                        case 0:
                            // 0 - atualização realizada
                            break;
                        case 1:
                        //RGA sem número inscrição
                        case 2:
                        //Candidato não encontrado ou já desligado
                        case 4:
                        //"RGA de período passado"
                        default:
                            break;
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        
        /// <summary>
        /// Inclui Disciplina no Historico do Aluno
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="ip"></param>
        /// <param name="rga"></param>
        /// <param name="disciplina"></param>
        /// <param name="periodo"></param>
        /// <param name="turma"></param>
        /// <param name="protocolo"></param>
        /// <param name="condDisci"></param>
        /// <returns>True -> Disciplina Incluida com sucesso / False -> Disciplina não foi ofertada no período </returns>
        public bool IncluiDisciplinaHistorico(string usuario, string ip, string rga, int disciplina, int periodo, string turma, string protocolo, string condDisci = "")
        {
            try
            {
                if (string.IsNullOrEmpty(condDisci))
                {
                    condDisci = "";
                }
                using (var db = new DbSigaEntities())
                {
                    decimal disciplinaDecimal = decimal.Parse(disciplina.ToString());
                    decimal rgaDecimal = decimal.Parse(rga.ToString());
                    System.Data.Objects.ObjectParameter retorno = new System.Data.Objects.ObjectParameter("retorno", 0);
                    db.ProcedureIncluiDisciplinaHist(usuario, ip, rgaDecimal, disciplinaDecimal, periodo, turma, protocolo, condDisci, retorno);
                    //@retorno = 0 => disciplina incluída no histórico do aluno
                    if (retorno.Value.Equals(0))
                    {
                        return true;
                    }
                    else
                    {
                        //@retorno = 1 => disciplina não foi ofertada no período
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        
        /// <summary>
        /// Remover Disciplina no Historico do Aluno
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="ip"></param>
        /// <param name="rga"></param>
        /// <param name="disciplina"></param>
        /// <param name="periodo"></param>
        /// <param name="turma"></param>
        /// <returns>True -> Disciplina removida do histórico do aluno / False -> Disciplina não localizada no histórico</returns>
        public bool RemoverDisciplinaHistorico(string usuario, string ip, string rga, int disciplina, int periodo, string turma)
        {
            try
            {
                using (var db = new DbSigaEntities())
                {
                    decimal disciplinaDecimal = decimal.Parse(disciplina.ToString());
                    decimal rgaDecimal = decimal.Parse(rga.ToString());
                    System.Data.Objects.ObjectParameter retorno = new System.Data.Objects.ObjectParameter("retorno", 0);
                    db.ProcedureRemoveDisciplinaHist(usuario, ip, rgaDecimal, disciplinaDecimal, periodo, turma, retorno);

                    if (retorno.Value.Equals(0))
                    {
                        //@retorno = 0 => disciplina removida do histórico do aluno
                        return true;
                    }
                    else
                    {
                        //@retorno = 1 => disciplina não localizada no histórico
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        
        /// <summary>
        /// Verifica se há pendencias do Aluno na Biblioteca
        /// </summary>
        /// <param name="parametro">Pode ser passado o RGA ou o CPF do Aluno para verificação</param>
        /// <returns>True -> Há pendencias na Biblioteca / False -> Não há pendencias na Biblioteca</returns>
        public bool VerificaPendenciasBiblioteca(string parametro)
        {
            try
            {
                using (var db = new DbSigaEntities())
                {
                    var retorno = db.Database.SqlQuery<int>("select dbo.ChecaEmprestimo('" + parametro + "')");

                    if (retorno.First().Equals(0))
                    {
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        /// <summary>
        /// Altera a senha tanto de Aluno Graduação
        /// </summary>
        /// <param name="rga">RGA do aluno a ser alterado</param>
        /// <param name="senha_antiga">Senha antiga ou senha atual do aluno</param>
        /// <param name="nova_senha">Nova senha que o aluno utilizará</param>
        /// <returns></returns>
        public void AlterarSenhaAluno(string rga, string senha_antiga, string nova_senha)
        {
            try
            {
                try
                {
                    var rgaDec = Convert.ToDecimal(rga);

                    using (var db = new DbSigaEntities())
                    {
                        var aluno = db.SIGAALUNO.Where(x => x.SigaRgaAluno == rgaDec).FirstOrDefault();

                        if (aluno.SigaSenhaAluno.Trim().Equals(senha_antiga))
                        {
                            aluno.SigaSenhaAluno = nova_senha;
                            db.SaveChanges();
                            //return true;
                        }
                        else
                        {
                            throw new FaultException("Senhas antiga não é válida.");
                        }
                    }
                }
                catch
                {

                }

                try
                {
                    //return true;
                }
                catch
                {


                }
            }
            catch (Exception ex)
            {
                throw new FaultException("Falha ao atualizar senha do aluno solicitado. Stack trace: " + ex.Message.ToString());
            }
        }
        
        /// <summary>
        /// Atualiza os dados cadastrais do Aluno
        /// </summary>
        /// <param name="rga">Rga do Aluno a ser atualizado</param>
        /// <param name="TelefonePrimario"></param>
        /// <param name="Mail"></param>
        /// <param name="linkLattes"></param>
        /// <param name="image"></param>
        /// <param name="content_type"></param>
        /// <returns>
        ///     True -> em caso de sucesso na Atualização da senha
        ///     False -> em caso de falha na Atualização da senha
        /// </returns>
        public bool AlterarDadosCadastrais(string rga, string TelefonePrimario, string Mail, string linkLattes, byte[] image, string content_type)
        {
            try
            {
                var rgaDec = Convert.ToDecimal(rga);

                using (var db = new DbSigaEntities())
                {
                    var aluno = db.SIGAALUNO.Where(x => x.SigaRgaAluno == rgaDec).FirstOrDefault();

                    if (aluno != null)
                    {
                        aluno.SigaFoneAluno = TelefonePrimario;
                        aluno.SigaEmailAluno = Mail;
                    }

                    db.SaveChanges();
                }

                using (var db = new idbufmtEntities())
                {
                    if (image.Length > 0)
                    {
                        var perfilaluno = db.PerfilDiscente.Where(x => x.rga == rga).FirstOrDefault();

                        if (perfilaluno != null)
                        {
                            perfilaluno.linkLattes = linkLattes;
                            perfilaluno.content_type = content_type;
                            perfilaluno.imagem = image;
                        }
                        else
                        {
                            perfilaluno = new PerfilDiscente();

                            perfilaluno.linkLattes = linkLattes;
                            perfilaluno.content_type = content_type;
                            perfilaluno.imagem = image;

                            db.PerfilDiscente.Add(perfilaluno);
                        }
                    }

                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        
        
        /// <summary>
        /// Reenvia a senha para o aluno
        /// </summary>
        /// <param name="rga"></param>
        /// <returns>
        /// True -> Sucesso no envio
        /// </returns>
        public bool ReenvioSenhaAluno(string rga)
        {
            try
            {
                var rgaDec = Convert.ToDecimal(rga);
                using (var db = new idbufmtEntities())
                {
                    var aluno = db.GetDadosAluno.Where(x => x.rga == rgaDec).FirstOrDefault();

                    if (aluno != null)
                    {
                        if (!string.IsNullOrEmpty(aluno.email))
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.Append("A sua senha é: " + aluno.senha.ToString());

                            Email e = new Email();
                            e.EnviarSmtpPadraoVersaoDois(aluno.email, sb.ToString(), "Reenvio de senha");

                            return true;
                        }
                        else
                        {
                            throw new FaultException("Aluno não possui endereço de email cadastrado.");
                        }
                    }
                    else
                    {
                        throw new FaultException("Aluno não encontrado.");
                    }
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }
    }
}