﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ufmt.services.entities;
using ufmt.services.EntityModels;

namespace ufmt.services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AutenticacaoSiga" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select AutenticacaoSiga.svc or AutenticacaoSiga.svc.cs at the Solution Explorer and start debugging.
    /// <summary>
    /// Serviço com regras de autenticação específicas do SIGA
    /// </summary>
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class AutenticacaoSiga : IAutenticacaoSiga
    {
        private DbSigaEntities db = new DbSigaEntities();

        /// <summary>
        /// Retorna o perfil do usuário do SIGA
        /// </summary>
        /// <param name="cpf"></param>
        /// <returns>List de PerfilSigaEntity</returns>
        public List<PerfilSigaEntity> GetPerfil(string cpf)
        {
            try
            {
                List<GetPerfilUsuarioSIGA_Result> perfisResult = this.db.GetPerfilUsuarioSIGA(cpf).ToList();

                var perfis = new List<PerfilSigaEntity>();
                foreach (var p in perfisResult)
                {
                    perfis.Add(new PerfilSigaEntity(p));

                }

                return perfis;
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Gera a sessão de banco de dados do SIGA
        /// </summary>
        /// <param name="id"></param>
        /// <param name="nivel"></param>
        /// <param name="idVinculado"></param>
        /// <returns>AcessoSigaEntity</returns>
        public AcessoSigaEntity GerarSessao(string id, string nivel, string idVinculado)
        {
            try
            {
                string acao = "I";
                string ip = "192.168.109.197";

                var retornoIdAcesso = new ObjectParameter("rETORNO_ID_ACESSO", typeof(Int64?));
                var retornoIdUsuario = new ObjectParameter("rETORNO_IDUSUARIO", typeof(Int64?));
                var retornoChaveAcesso = new ObjectParameter("rETORNO_CHAVE_ACESSO", typeof(string));
                var retornoNivelAcesso = new ObjectParameter("rETORNO_NIVELACESSO", typeof(char?));
                var retornoExcedeuTempo = new ObjectParameter("rETORNO_EXCEDEU_TEMPO", typeof(char?));


                this.db.SP_CONTROLE_ACESSO_LOGIN_REGISTRO(acao, long.Parse(id), "", nivel, ip, null, null, long.Parse(idVinculado),
                    retornoIdAcesso, retornoIdUsuario, retornoChaveAcesso, retornoNivelAcesso, retornoExcedeuTempo);

                return
                new AcessoSigaEntity()
                {
                    IdAcesso = retornoIdAcesso.Value.ToString(),
                    ChaveAcesso = retornoChaveAcesso.Value.ToString()
                };

            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
    }
}
