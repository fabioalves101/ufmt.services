﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ufmt.services.entities;
using System.ServiceModel.Activation;
using ufmt.sig.business;

namespace ufmt.services
{
    /// <summary>
    /// Serviço de consulta as informações de disciplina
    /// </summary>
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Disciplina" in code, svc and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class Disciplina : IDisciplina
    {
        /// <summary>
        ///  Pesquisa ementa por disciplina e periodo 
        /// </summary>
        /// <param name="disciplinaUID">UID da disciplina a ser pesquisada </param>
        /// <param name="periodo"> periodo da ementa a ser pesquisada </param>
        /// <returns> Retorna informações da ementa pesquisada </returns>
        public Ementa GetEmenta(long disciplinaUID, int periodo)
        {
            using (idbufmtEntities db = new idbufmtEntities())
            {
                vwEmentas vwementa =
                db.vwEmentas.
                    Where(e => e.codigoDisciplina == disciplinaUID && e.periodo == periodo)
                    .Where(e => e.ativa == true)
                    .FirstOrDefault();

                if (vwementa != null)
                {
                    Ementa ementa = new Ementa()
                    {
                        ativa = vwementa.ativa.Value,
                        codigoDisciplina = vwementa.codigoDisciplina.Value,
                        cursoSiga = vwementa.cursoSiga.Value,
                        ementaDisciplina = vwementa.ementaDisciplina,
                        ementaUID = vwementa.ementaUID,
                        periodo = vwementa.periodo.Value
                    };

                    return ementa;
                }
                else
                {
                    return null;
                }
            }
        }
        
        /// <summary>
        /// Pesquisa da ultima ementa em um disciplina 
        /// </summary>
        /// <param name="disciplinaUID">UID da disciplina a ser pesquisada </param>
        /// <returns> Retorna a dados da ementa pequisada </returns>
        public Ementa GetLastEmenta(long disciplinaUID)
        {
            using (idbufmtEntities db = new idbufmtEntities())
            {
                vwEmentas vwementa =
                db.vwEmentas.
                    Where(e => e.codigoDisciplina == disciplinaUID)
                    .Where(e => e.ativa == true)
                    .OrderByDescending(e => e.periodo)
                    .FirstOrDefault();

                if (vwementa != null)
                {
                    Ementa ementa = new Ementa()
                    {
                        ativa = vwementa.ativa.Value,
                        codigoDisciplina = vwementa.codigoDisciplina.Value,
                        cursoSiga = vwementa.cursoSiga.Value,
                        ementaDisciplina = vwementa.ementaDisciplina,
                        ementaUID = vwementa.ementaUID,
                        periodo = vwementa.periodo.Value
                    };
                    return ementa;
                }
                else
                {
                    return null;
                }
            }
        }
        
        /// <summary>
        /// Pesquisa de disciplinas de graduação por matriz curricular 
        /// </summary>
        /// <param name="matrizCurricularUID"> UID da matriz curricular </param>
        /// <returns> retorna uma lista de disciplinas com os parametros passados </returns>
        public List<DisciplinaEntity> GetDisciplinasGraduacaoPorMatrizCurricular(long matrizCurricularUID)
        {
            List<DisciplinaEntity> disciplinas = new List<DisciplinaEntity>();
            using (ICursoBO cursoBO = BusinessFactory.GetInstance<ICursoBO>())
            {

                ufmt.sig.entity.MatrizCurricular matrizCurricular = cursoBO.GetMatrizCurricular(matrizCurricularUID);

                int? campusUID = null;
                if (matrizCurricular.Curso.UnidadeOfertante != null)
                {
                    campusUID = matrizCurricular.Curso.UnidadeOfertante.Campus.CampusUID;
                }

                int? nivelCursoUID = null;
                if (matrizCurricular.Curso.NivelCurso != null)
                {
                    nivelCursoUID = matrizCurricular.Curso.NivelCurso.NivelCursoUID;
                }

                IList<ufmt.sig.entity.MatrizDisciplinar> matrizDisciplinar =
                    cursoBO.GetMatrizesDisciplinares(campusUID,
                    nivelCursoUID,
                    matrizCurricular.Curso.CursoUID, matrizCurricularUID, null, null);

                foreach (ufmt.sig.entity.MatrizDisciplinar matriz in matrizDisciplinar)
                {
                    DisciplinaEntity disciplina = new DisciplinaEntity()
                    {
                        MatrizDisciplinaUID = matriz.MatrizDisciplinarUID,
                        Nome = matriz.Nome,
                        PeriodoInicialFuncionamento = matriz.PeriodoInicialFuncionamento,
                        PeriodoFinalFuncionamento = matriz.PeriodoFinalFuncionamento,
                        TipoDisciplina = matriz.TipoDisciplina.Descricao,

                        MatrizCurricularUID = matriz.MatrizCurricular.MatrizCurricularUID,
                        Ementa = matriz.Ementa,
                        CargaHorariaTotal = matriz.CargaHorariaTotal,
                        CargaHorariaTeorica = decimal.Parse(matriz.CargaHorariaTeorica.ToString()),
                        CargaHorariaPraticaComponente = decimal.Parse(matriz.CargaHorariaPraticaComponente.ToString()),
                        CargaHorariaPratica = decimal.Parse(matriz.CargaHorariaPratica.ToString()),
                        CodigoExterno = matriz.CodigoExterno
                    };

                    if (matriz.UnidadeOfertante != null)
                        disciplina.UnidadeOfertanteUID = matriz.UnidadeOfertante.UnidadeUID;

                    if (matriz.TipoOfertaDisciplinar != null)
                        disciplina.TipoOfertaDisciplinar = matriz.TipoOfertaDisciplinar.Descricao;

                    disciplinas.Add(disciplina);
                }
            }

            return disciplinas;
        }
        
        /// <summary>
        /// Pesquisa de disciplinas por código externo e periodo 
        /// </summary>
        /// <param name="codigoExterno"> Código externo da disciplina a ser pesquisada </param>
        /// <param name="periodo"> Período da disciplina a ser pesquisada </param>
        /// <returns>Retorna um instancia de disciplina </returns>
        public DisciplinaEntity GetDisciplinaPorCodigoExternoPeriodo(long codigoExterno, int periodo)
        {
            try
            {
                using (idbufmtEntities db = new idbufmtEntities())
                {
                    MatrizDisciplinar matriz = db.MatrizDisciplinar
                                                .Where(d => d.codigoExterno == codigoExterno)
                                                .FirstOrDefault();

                    DisciplinaEntity disciplina = new DisciplinaEntity(matriz);

                    return disciplina;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        
        /// <summary>
        /// Pesquisa de disciplinas cursadas por rga 
        /// </summary>
        /// <param name="rga"> RGA do Aluno a ser pesquisado</param>
        /// <returns> retorna um lista de disciplinas cursadas pelo aluno </returns>
        public List<DisciplinaAlunoEntity> GetDisciplinasCursadas(string rga)
        {
            try
            {
                using (idbufmtEntities db = new idbufmtEntities())
                {
                    decimal drga = decimal.Parse(rga);
                    var disciplinasAluno
                        = db.ViewDisciplinaAlunoSiga.Where(h => h.rga == drga).ToList();

                    var disciplinaPeriodo = disciplinasAluno
                                                    .Where(d => d.periodo.ToString().Last() != '3')
                                                    .OrderByDescending(d => d.periodo)
                                                    .ToList()
                                                    .First();

                    disciplinasAluno = disciplinasAluno.Where(d => d.periodo == disciplinaPeriodo.periodo).ToList();

                    List<DisciplinaAlunoEntity> disciplinaList = new List<DisciplinaAlunoEntity>();

                    foreach (ViewDisciplinaAlunoSiga disciplinaAluno in disciplinasAluno)
                    {
                        DisciplinaEntity disciplina =
                            this.GetDisciplinaPorCodigoExternoPeriodo((long)disciplinaAluno.codigoDisciplina,
                                                                        disciplinaAluno.periodo);

                        DisciplinaAlunoEntity disciplinaAlunoEntity = new DisciplinaAlunoEntity()
                        {
                            Disciplina = disciplina,
                            Periodo = disciplinaAluno.periodo,
                            CodigoCursoExterno = disciplinaAluno.codigoCurso,
                            CodigoCursoAluno = disciplinaAluno.cursoAluno,
                            CodigoCursoDisciplina = disciplinaAluno.codigoCurso,
                            Turma = disciplinaAluno.turma
                        };

                        disciplinaList.Add(disciplinaAlunoEntity);
                    }

                    return disciplinaList;
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }

        }

        /// <summary>
        /// Pesquisa da disciplina do aluno
        /// </summary>
        /// <param name="rga"> RGA do aluno a ser pesquisado</param>
        /// <param name="situacao">Situaçao da disciplina a ser pesquisado</param>
        /// <returns>Retorna a listas de disciplinas</returns>
        public List<DisciplinaAlunoEntity> GetDisciplinasAluno(string rga, string situacao)
        {
            try
            {
                using (idbufmtEntities db = new idbufmtEntities())
                {
                    decimal drga = decimal.Parse(rga);
                    List<ViewDisciplinaAlunoSiga> disciplinasAluno
                        = db.ViewDisciplinaAlunoSiga.Where(h => h.rga == drga && h.observacao.Equals(situacao)).ToList();

                    List<DisciplinaAlunoEntity> disciplinaList = new List<DisciplinaAlunoEntity>();

                    foreach (ViewDisciplinaAlunoSiga disciplinaAluno in disciplinasAluno)
                    {
                        DisciplinaEntity disciplina =
                            this.GetDisciplinaPorCodigoExternoPeriodo((long)disciplinaAluno.codigoDisciplina,
                                                                        disciplinaAluno.periodo);

                        DisciplinaAlunoEntity disciplinaAlunoEntity = new DisciplinaAlunoEntity()
                        {
                            Disciplina = disciplina,
                            Periodo = disciplinaAluno.periodo,
                            CodigoCursoExterno = disciplinaAluno.codigoCurso,
                            CodigoCursoAluno = disciplinaAluno.cursoAluno,
                            CodigoCursoDisciplina = disciplinaAluno.codigoCurso,
                            Turma = disciplinaAluno.turma
                        };

                        disciplinaList.Add(disciplinaAlunoEntity);
                    }

                    return disciplinaList;
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }

        }
        
        /// <summary>
        /// Pesquisa de professor por disciplina  
        /// </summary>
        /// <param name="codigoDisciplinaExterno"> código da disciplina a ser verificada  </param>
        /// <param name="turma">turma ser verificada </param>
        /// <param name="periodo"> período da ser verificado </param>
        /// <param name="codigoCursoExterno"> código externo do curso a ser verificado </param>
        /// <returns> Retorna um lista de professores das disciplinas pesquisadas</returns>
        public List<PessoaEntity> GetProfessoresPorDisciplina(string codigoDisciplinaExterno, string turma, int periodo, int codigoCursoExterno)
        {
            using (idbufmtEntities db = new idbufmtEntities())
            {
                int codigoDisciplina = int.Parse(codigoDisciplinaExterno);
                List<ViewDisciplinaProfessorSiga> professorDisciplinaList =
                    db.ViewDisciplinaProfessorSiga.Where(p => p.disciplina == codigoDisciplina)
                                                  .Where(p => p.turma == turma)
                                                  .Where(p => p.periodo == periodo)
                                                  .Where(p => p.cursoSiga == codigoCursoExterno)
                                                  .ToList();

                List<PessoaEntity> pessoaEntityList = new List<PessoaEntity>();
                foreach (ViewDisciplinaProfessorSiga professorDisciplina in professorDisciplinaList)
                {
                    Pessoa pessoaService = new Pessoa();

                    //PessoaEntity pessoa = pessoaService.CastPessoaToPessoaEntity();
                    pessoaEntityList.Add(pessoaService.GetPessoaPorServidor(professorDisciplina.servidorUID));
                }

                return pessoaEntityList;
            }
        }

        /// <summary>
        /// Pesquisa de disciplina por matriz disciplinar 
        /// </summary>
        /// <param name="matrizDisciplinarUID">UID da mztriz disciplinar a ser verificada </param>
        /// <returns>Retorna uma instancia de disciplina </returns>
        public DisciplinaEntity GetDisciplina(long matrizDisciplinarUID)
        {
            try
            {
                using (ICursoBO cursoBO = BusinessFactory.GetInstance<ICursoBO>())
                {
                    ufmt.sig.entity.MatrizDisciplinar matrizSig = cursoBO.GetMatrizDisciplinar(matrizDisciplinarUID);
                    DisciplinaEntity disciplina = this.ConvertMatrizDisciplinarToDisciplinaEntity(matrizSig);
                    return disciplina;
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Converte Matriz Disciplinar para Disciplina entity
        /// </summary>
        /// <param name="matrizSig"> Matriz Disciplinar a ser convertida </param>
        /// <returns>Retorna a matiz disciplinar seleciona convertida em Disciplina Entity </returns>
        private DisciplinaEntity ConvertMatrizDisciplinarToDisciplinaEntity(ufmt.sig.entity.MatrizDisciplinar matrizSig)
        {
            return new DisciplinaEntity(matrizSig);
        }
    }
}
