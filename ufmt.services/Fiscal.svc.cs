﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ufmt.services.entities;

namespace ufmt.services
{

    /// <summary>
    /// Serviço de consulta a informações de fiscal 
    /// </summary>
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Fiscal" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Fiscal.svc or Fiscal.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class Fiscal : IFiscal
    {
        /// <summary>
        /// Verifica se o candidato a fiscal já possui cadastro na SARI
        /// </summary>
        /// <param name="cpf">CPF do candidato  à inscrição a ser pesquisado</param>
        /// <returns>Lista de Fiscais a partir do CPF informado</returns>
        public List<PessoaFiscalEntity> ConsultaInscricao(string cpf)
        {
            using (var db = new idbufmtEntities())
            {
                var retorno = db.Consulta_Inscricao_Fiscal_SARI(cpf);
                db.Campus.Where(x => x.campusUID == 1);
                List<PessoaFiscalEntity> pessoas = new List<PessoaFiscalEntity>();
                foreach (var item in retorno)
                {
                    pessoas.Add(new PessoaFiscalEntity(item));
                }

                if (pessoas.Count > 0)
                {
                    return pessoas;
                }
                else
                {
                    throw new FaultException("Nenhuma Informação encontrada com o CPF informado!!");
                }

            }
        }
    }
}
