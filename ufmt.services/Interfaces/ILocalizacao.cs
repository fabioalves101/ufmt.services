﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ufmt.sig.business;
using ufmt.services.entities;

namespace ufmt.services
{
    /// <summary>
    /// Serviço que disponibiliza as informaçoes de unidade
    /// </summary>
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "ILocalizacao" in both code and config file together.
    [ServiceContract]
    public interface ILocalizacao
    {
        [OperationContract]
        List<UnidadeEntity> GetUnidades(string nomeParcial, int? campusUID, bool? apenasAtivos, int? registroInicial,
            int? quantidadeRegistros);

        [OperationContract]
        UnidadeEntity GetUnidade(long unidadeUID);

        [OperationContract]
        List<CampusEntity> GetCampi(string nomeParcial);

        [OperationContract]
        UnidadeEntity GetUnidadePorResponsavel(long pessoaUID);

        [OperationContract]
        List<UnidadeEntity> GetUnidadesPorUnidadeSuperior(long unidadeUID);

        /// <summary>
        /// Pesquisa o nome do Campi de uma determinada Unidade
        /// </summary>
        /// <param name="unidadeUID">UID da unidade a ser pesquisada</param>
        /// <returns>Retorna o nome do Campi da Unidade</returns>
        [OperationContract]
        string GetNomeCampiPorUnidade(long unidadeUID);
    }
}
