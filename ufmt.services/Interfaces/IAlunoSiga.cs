﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ufmt.services.entities;
using System.ServiceModel.Web;

namespace ufmt.services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAlunoSiga" in both code and config file together.
    [ServiceContract]
    public interface IAlunoSiga
    {
        [OperationContract]
        Aluno GetInfoAluno(string rga);

        [OperationContract]
        entities.Aluno GetInfoAlunoInativo(string rga);

        [OperationContract]
        List<Aluno> GetAlunos();

        [OperationContract]
        List<Aluno> GetAlunosPorCpf(string cpf);

        [OperationContract]
        List<Aluno> GetAlunosInativosPorCpf(string cpf);

        [OperationContract]
        List<Aluno> GetAlunosPorCampus(int campusUID);

        [OperationContract]
        List<Aluno> GetAlunosGraduacao();

        [OperationContract]
        List<Aluno> GetAlunosGraduacaoPorCampus(int campusUID);

        [OperationContract]
        List<Aluno> GetAlunosPaginado(int registroInicial, int quantidadeRegistros);

        [OperationContract]
        List<Aluno> GetAlunosPorCampusPaginado(int campusUID, int registroInicial, int quantidadeRegistros);

        /// <summary>
        /// Operação que calcula o coeficiente de rendimento de um estudante de graduação a partir do seu RGA.
        /// </summary>
        /// <param name="rga">O RGA do estudante procurado.</param>
        /// <returns>O coeficiente de rendimento do estudante do qual o RGA corresponde.</returns>
        [OperationContract]
        double GetCoeficiente(string rga);

        /// <summary>
        /// Operação de busca de estudantes por nome.
        /// </summary>
        /// <param name="nome">O nome parcial ou completo do estudante de graduação que está sendo procurado.</param>
        /// <param name="registroInicial">A partir de qual registro será retornado (usado para paginação).</param>
        /// <param name="quantidadeDeRegistros">A quantidade de registros retornados pela operação (usado para paginação).</param>
        /// <param name="campusUID">O campusUID para restringir a busca por Campus (Opcional).</param>
        /// <param name="quantidadeTotal">A quantidade total de registros na consulta, para facilitar o processo de paginação.</param>
        /// <returns>Uma lista de objetos que representas as entradas correspondentes a pesquisa.</returns>
        [OperationContract]
        List<Aluno> GetAlunosPorNome(string nome, int registroInicial, int quantidadeDeRegistros, int? campusUID, out int quantidadeTotal);

        [OperationContract]
        List<Aluno> GetAlunosInativosPorNome(string nome);

        /// <summary>
        /// Operação de consulta genérica de estudantes de graduação a partir de um determinado parâmetro e valor passado.
        /// </summary>
        /// <param name="nomeParametro">O nome do parâmetro que será usado para a busca.</param>
        /// <param name="valorParametro">O valor do parâmetro que será usado para a busca.</param>
        /// <param name="registroInicial">A partir de qual registro será retornado (usado para paginação).</param>
        /// <param name="quantidadeDeRegistros">A quantidade de registros retornados pela operação (usado para paginação).</param>
        /// <param name="campusUID">O campusUID para restringir a busca por Campus (Opcional).</param>
        /// <returns>Uma lista de objetos que representas as entradas correspondentes a pesquisa.</returns>
        [OperationContract]
        List<Aluno> ConsultaGenerica(ParametrosEstudante nomeParametro, string valorParametro, int registroInicial, int quantidadeDeRegistros, int? campusUID);

        /// <summary>
        /// Registra as faltas de um determinado Estudante de Graduação em um curso e turma específico.
        /// </summary>
        /// <param name="usuario">O nome do usuário que está fazendo o lançamento das faltas.</param>
        /// <param name="IP">O endereço IP de origem do lançamento das faltas.</param>
        /// <param name="rga">O RGA do estudante cujas faltas serão gravadas.</param>
        /// <param name="disciplina">O código do SIGA para a disciplina procurada.</param>
        /// <param name="periodo">O período da turma.</param>
        /// <param name="turma">A turma do Estudante.</param>
        /// <param name="faltas">A quantidade de faltas a serem computadas.</param>
        /// <returns>A quantidade de faltas registradas.</returns>
        [OperationContract]
        int GravaFaltasEstudanteGraduacao(string usuario, string IP, string rga, string disciplina, int periodo, string turma, int faltas);

        [OperationContract]
        ResultadoFaltasEstudante[] GravaFaltasEstudantesGraduacao(string usuario, string IP, string disciplina, int periodo, string turma, FaltasPorEstudante[] faltasPorEstudante);
        
        [OperationContract]
        void DesligaAlunoSiga(string rga, string usuario, string ip);

        [OperationContract]
        bool IncluiDisciplinaHistorico(string usuario, string ip, string rga, int disciplina, int periodo, string turma, string protocolo, string condDisci = "I");

        [OperationContract]
        bool RemoverDisciplinaHistorico(string usuario, string ip, string rga, int disciplina, int periodo, string turma);

        [OperationContract]
        bool VerificaPendenciasBiblioteca(string parametro);

        /// <summary>
        /// Altera a senha tanto de Aluno Graduação
        /// </summary>
        /// <param name="rga">RGA do aluno a ser alterado</param>
        /// <param name="senha_antiga">Senha antiga ou senha atual do aluno</param>
        /// <param name="nova_senha">Nova senha que o aluno utilizará</param>
        /// <returns>
        ///     True -> em caso de sucesso na Atualização da senha
        ///     False -> em caso de falha na Atualização da senha
        /// </returns>
        [OperationContract]
        void AlterarSenhaAluno(string rga, string senha_antiga, string nova_senha);

        /// <summary>
        /// Atualiza os dados cadastrais do Aluno
        /// </summary>
        /// <param name="rga">Rga do Aluno a ser atualizado</param>
        /// <param name="TelefonePrimario"></param>
        /// <param name="Mail"></param>
        /// <param name="linkLattes"></param>
        /// <param name="image"></param>
        /// <param name="content_type"></param>
        /// <returns>
        ///     True -> em caso de sucesso na Atualização da senha
        ///     False -> em caso de falha na Atualização da senha
        /// </returns>
        [OperationContract]
        bool AlterarDadosCadastrais(string rga, string TelefonePrimario, string Mail, string linkLattes, byte[] image, string content_type);
        
        /// <summary>
        /// Reenvia a senha para o aluno
        /// </summary>
        /// <param name="rga"></param>
        /// <returns>
        /// True -> Sucesso no envio
        /// </returns>
        [OperationContract]
        bool ReenvioSenhaAluno(string rga);
    }

    [DataContract]
    public class FaltasPorEstudante
    {
        [DataMember]
        public string rga { get; set; }

        [DataMember]
        public double nota { get; set; }

        [DataMember]
        public int faltas { get; set; }
    }

    [DataContract]
    public class ResultadoFaltasEstudante
    {
        [DataMember]
        public string rga { get; set; }

        public double nota { get; set; }

        [DataMember]
        public int resultado { get; set; }
    }

    [DataContract]
    public enum ParametrosEstudante
    {
        [EnumMember]
        rga,
        [EnumMember]
        cpf,
        [EnumMember]
        rg,
        [EnumMember]
        nome
    }
}
