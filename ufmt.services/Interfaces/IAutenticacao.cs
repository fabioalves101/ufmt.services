﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ufmt.sig.entity;
using ufmt.services.entities;

namespace ufmt.services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IAutenticacao
    {
        [OperationContract]
        ufmt.sig.entity.Usuario LoginDelphi(String nomeAcesso, String senha);

        [OperationContract]
        ufmt.sig.entity.Usuario Login(String nomeAcesso, String senha);

        [OperationContract]
        ufmt.sig.entity.Usuario LoginDocente(String nomeAcesso, String senha);

        /// <summary>
        /// Login de usuários com situações específicas. Permite o login de usuários não-ativos ou afastados,
        /// por exemplo.
        /// </summary>
        /// <param name="nomeAcesso">O nome de acesso do usuário.</param>
        /// <param name="senhaAcesso">A senha de acesso do usuário.</param>
        /// <param name="situacoes">conjunto de estados de usuário que permitem login. Se nulo ou vazio, procura apenas por usuários sem situação adversa.</param>
        /// <returns>O Usuário correspondente ao acesso encontrado em alguma das situações indicadas.</returns>
        [OperationContract]
        ufmt.sig.entity.Usuario LoginPorSituacao(string nomeAcesso, string senhaAcesso, SituacaoDoServidor[] situacoes);

        [OperationContract]
        String VerificaDados(String nomeAcesso, String senha);

        [OperationContract]
        sig.entity.Servidor GetVinculoPessoaSimples(long pessoaUID);

        [OperationContract]
        [ServiceKnownType(typeof(sig.entity.Servidor))]
        IList<sig.entity.Servidor> GetVinculoDocente(long pessoaUID);

        [OperationContract]
        PessoaEntity GetPessoa(long pessoaUID);

        [OperationContract]
        IList<ufmt.sig.entity.Pessoa> GetPessoas(string Nome);

        [OperationContract]
        IList<sig.entity.Servidor> GetVinculosPorPessoa(long pessoaUID);

        [OperationContract(Name = "LoginAluno")]
        Aluno LoginAluno(string rga, string senha);

        [OperationContract(Name = "LoginAlunoPorChave")]
        Aluno LoginAluno(string chave);

        [OperationContract]
        Aluno LoginDiscente(string rga, string senha);

        [OperationContract]
        Coordenador LoginCoordenadorCurso(string username, string password);

        [OperationContract]
        ufmt.sig.entity.PermissaoUsuario GetPermissao(long aplicacaoUID, long permissaoUID, long usuarioUID);

        [OperationContract]
        List<PermissaoUsuarioEntity> GetPermissaoPorUsuarioAplicacao(long aplicacaoUID, long usuarioUID);

        [OperationContract]
        Coordenador GetCoordenadorPorChave(string chave);

        [OperationContract]
        PessoaEntity GetColaboradorCAEPorChave(string chave);

        [OperationContract]
        Coordenador GetCoordenadorPorChaveCodigoCurso(string chave, string codigoCurso);

        /// <summary>
        /// Reinicia a senha da pessoa correspondente ao CPF informado.
        /// </summary>
        /// <param name="cpf">O CPF da Pessoa cuja senha deve ser reiniciada.</param>
        [OperationContract]
        void ReiniciarSenha(string cpf);

        [OperationContract]
        void ReiniciarSenhaIntegrado(string cpf);

        /// <summary>
        /// Cria um usuário com o CPF e SIAPE informados. Caso o usuário já exista, apenas atualiza o email.
        /// </summary>
        /// <param name="cpf">CPF da Pessoa procurada.</param>
        /// <param name="siape">O SIAPE do Servidor referente a essa pessoa.</param>
        /// <param name="email">O email que será cadastrado e cuja senha automática será enviada.</param>
        [OperationContract]
        void CriarUsuario(string cpf, string siape, string email);

        [OperationContract]
        void AlterarSenha(string cpf, string senhaAtual, string novaSenha);

        [OperationContract]
        List<PessoaEntity> GetPessoasPorPermissao(long permissaoUID);

        [OperationContract]
        void SalvarPermissaoUsuario(long usuarioUID, long permissaoUID, DateTime? dataLimite);

        [OperationContract]
        sig.entity.Usuario GetUsuarioPorPessoa(long pessoaUID);

        [OperationContract]
        PessoaEntity GetDocentePorChave(string chave);

        /// <summary>
        /// Permite o login de diferentes tipos de estudantes (graduação e pós-graduação, por exemplo)
        /// em um mesmo método.
        /// </summary>
        /// <param name="rga">O RGA para acesso do estudante.</param>
        /// <param name="senha">A senha de acesso do estudante.</param>
        /// <param name="tiposEstudante">Uma lista contendo os tipos de estudante cujo login deve ser tentado.</param>
        /// <returns>O objeto "Aluno" com os dados correspondentes ao acesso solicitado.</returns>
        /// <exception cref="System.ArgumentException">Um ou mais argumentos é inválido (descrição na mensagem interna).</exception>
        [OperationContract]
        Aluno LoginEstudanteGeral(string rga, string senha, TipoEstudante[] tiposEstudante);

        /// <summary>
        /// Permite o login de diferentes tipos de estudantes (graduação e pós-graduação, por exemplo)
        /// em um mesmo método, filtrando-os pelo campus.
        /// </summary>
        /// <param name="rga">O RGA para acesso do estudante.</param>
        /// <param name="senha">A senha de acesso do estudante.</param>
        /// <param name="tiposEstudante">Uma lista contendo os tipos de estudante cujo login deve ser tentado.</param>
        /// <param name="campusUID">Uma lista com os identificadores dos campi que devem ser tentados o login.</param>
        /// <returns>O objeto "Aluno" com os dados correspondentes ao acesso solicitado.</returns>
        [OperationContract(Name="LoginEstudanteGeralPorCampi")]
        Aluno LoginEstudanteGeral(string rga, string senha, TipoEstudante[] tiposEstudante, int[] campusUID);

        /// <summary>
        /// Retorna todas as Permissões de Campus referentes a uma Permissão específica.
        /// </summary>
        /// <param name="permissaoUID">O identificador da Permissão.</param>
        /// <returns>A lista de permissões de campus referentes a Permissão procurada.</returns>
        [OperationContract]
        PermissaoCampusEntity[] GetPermissaoCampusPorPermissao(long permissaoUID);

        [OperationContract]
        Aluno LoginEstudanteDPR(string rga, string senha);

        /// <summary>
        /// Valida o Token de um determinado Usuario
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [OperationContract]
        UsuarioEntity ValidarToken(string token);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        [OperationContract]
        Aluno ValidarTokenAluno(string token);
    }

    /// <summary>
    /// Enumeração dos tipos de estudante.
    /// </summary>
    [DataContract]
    public enum TipoEstudante
    {
        [EnumMember]
        Graduacao = 1,
        [EnumMember]
        PosGraduacao = 2
    }

    [DataContract]
    public enum SituacaoDoServidor
    {
        [EnumMember]
        Ativo = 1,
        [EnumMember]
        Afastado = 2,
        [EnumMember]
        Cedido = 3,
        [EnumMember]
        Inativo = 4,
        [EnumMember]
        Excluido = 5
    }
}
