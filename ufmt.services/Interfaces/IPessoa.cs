﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ufmt.sig.entity;
using ufmt.services.entities;

namespace ufmt.services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IPessoa" in both code and config file together.
    [ServiceContract]
    public interface IPessoa
    {
        [OperationContract]
        PessoaEntity GetPessoa(long pessoaUID);

        //[OperationContract]
        //PessoaEntity GetPessoaPorSituacao(long pessoaUID, SituacaoDoServidor[] situacoes);

        [OperationContract]
        List<ufmt.sig.entity.Pessoa> GetPessoas(string nome);

        [OperationContract]
        ufmt.sig.entity.Pessoa GetPessoaPorUsuario(long usuarioUID);

        /// <summary>
        /// Retorna uma PessoaEntity para um determinado usuarioUID
        /// </summary>
        /// <param name="usuarioUID">id do usuario</param>
        /// <returns></returns>
        [OperationContract]
        ufmt.services.entities.PessoaEntity GetPessoaEntityPorUsuario(long usuarioUID);

        [OperationContract]
        IList<PessoaEntity> GetVinculoDocente(long pessoaUID, bool apenasAtivos);

        [OperationContract]
        ServidorPessoaSimples GetServidorPessoaSimples(string siape);

        [OperationContract]
        ufmt.sig.entity.Pessoa GetPessoaPorCPF(string cpf);

        /// <summary>
        /// Atualiza os dados pessoais de uma determinada pessoa
        /// </summary>
        /// <param name="pessoaUID"></param>
        /// <param name="TelefonePrimario"></param>
        /// <param name="TelefoneSecundario"></param>
        /// <param name="Mail"></param>
        /// <param name="linkLattes"></param>
        /// <param name="image"></param>
        /// <param name="content_type"></param>
        /// <returns>True -> Sucesso na atualização // False -> Falha na atualização</returns>
        [OperationContract]
        bool AtualizarDadosCadastrais(long pessoaUID, string TelefonePrimario, string TelefoneSecundario, string Mail, string linkLattes, byte[] image, string content_type);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cpf"></param>
        /// <param name="situacoes"></param>
        /// <returns></returns>
        [OperationContract]
        PessoaEntity GetPessoaPorCPFeSituacao(string cpf, SituacaoDoServidor[] situacoes);

        [OperationContract]
        List<ufmt.sig.entity.Pessoa> GetProfessores(string nome);

        [OperationContract]
        PessoaEntity GetPessoaPorServidor(long servidorUID);


        [OperationContract]
        List<PessoaEntity> GetPessoasPorUnidade(long unidadeUID);

        [OperationContract]
        PessoaEntity GetPessoaEntity(long pessoaUID);

        [OperationContract]
        List<PessoaEntity> GetPessoasEntity(string nome, int registroInicial, int quantidadeRegistros);

        [OperationContract]
        vwCoordenadores GetCoordenadorSigaPorCodigoCurso(int codigoCurso);

        [OperationContract]
        vwCoordenadores GetCoordenadorSigaPorCursoUID(long cursoUID);

        [OperationContract]
        List<PessoaEntity> ConsultaGenerica(ParametrosPessoa nomeParametro, string valorParametro, SituacaoDoServidor[] situacoes,
            int registroInicial, int quantidadeDeRegistros);

        [OperationContract]
        PessoaEntity GetPessoaEntityPorSituacao(long pessoaUID, SituacaoDoServidor[] situacoes);

        [OperationContract]
        string VerificaVinculo(string cpf);

        /// <summary>
        /// Pesquisa de coordenador por id 
        /// </summary>
        /// <param name="codigoCoordenador"> Codigo do coordenador a ser pesquisado </param>
        /// <returns> Retorna dados do cordenador </returns>
        [OperationContract]
        List<vwCoordenadores> GetCoordenadorSigaPorCodigo(int codigoCoordenador);
    }

    [DataContract(IsReference = true)]
    [KnownType(typeof(ufmt.sig.entity.Unidade))]
    [KnownType(typeof(Cargo))]
    [KnownType(typeof(JornadaTrabalho))]
    [KnownType(typeof(ufmt.sig.entity.Pessoa))]
    [KnownType(typeof(sig.entity.SituacaoServidor))]
    public class CompositeTypeServidor
    {
        [DataMember]
        public Int64 ServidorUID;

        [DataMember]
        public Pessoa Pessoa;

        [DataMember]
        public String Registro;

        [DataMember]
        public Char RegistroDV;

        [DataMember]
        public Char TipoRegistro;

        [DataMember]
        public String RegimeJuridico;

        [DataMember]
        public JornadaTrabalho JornadaTrabalho;

        [DataMember]
        public Cargo Cargo;

        [DataMember]
        public Char? CargoClasse;

        [DataMember]
        public String CargoReferenciaNivelPadrao;

        [DataMember]
        public ufmt.sig.entity.Unidade UnidadeLotacao;

        [DataMember]
        public String AposentadoriaNumeroProcesso;

        [DataMember]
        public int? AposentadoriaAnoPrevisto;

        [DataMember]
        public Char? AposentadoriaOpcaoIntegral;

        [DataMember]
        public sig.entity.SituacaoServidor SituacaoServidor;


    }

    [DataContract]
    public enum ParametrosPessoa
    {
        [EnumMember]
        cpf,
        [EnumMember]
        rg,
        [EnumMember]
        pessoaUID,
        //[EnumMember]
        //usuarioUID,
        //[EnumMember]
        //servidorUID,
        [EnumMember]
        nome
    }
}
