﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ufmt.services.entities;

namespace ufmt.services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IDisciplina" in both code and config file together.
    [ServiceContract]
    public interface IDisciplina
    {
        [OperationContract]
        Ementa GetEmenta(long disciplinaUID, int periodo);

        [OperationContract]
        Ementa GetLastEmenta(long disciplinaUID);

        [OperationContract]
        List<DisciplinaEntity> GetDisciplinasGraduacaoPorMatrizCurricular(long matrizCurricularUID);

        [OperationContract]
        List<DisciplinaAlunoEntity> GetDisciplinasCursadas(string rga);

        [OperationContract]
        List<PessoaEntity> GetProfessoresPorDisciplina(string codigoDisciplinaExterno, string turma, int periodo, int codigoCursoExterno);

        [OperationContract]
        DisciplinaEntity GetDisciplina(long matrizDisciplinarUID);

        [OperationContract]
        List<DisciplinaAlunoEntity> GetDisciplinasAluno(string rga, string situacao);
        
    }
}
