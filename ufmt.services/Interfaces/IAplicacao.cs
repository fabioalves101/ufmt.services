﻿using System.Collections.Generic;
using System.ServiceModel;
using ufmt.services.entities;

namespace ufmt.services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAplicacao" in both code and config file together.
    [ServiceContract]
    public interface IAplicacao
    {
        /// <summary>
        /// Retorna todas as aplicações de acordo com o seu ID
        /// </summary>
        /// <param name="aplicacaoUID"></param>
        /// <returns></returns>
        [OperationContract]
        AplicacaoEntity GetAplicacao(long aplicacaoUID);

        /// <summary>
        /// Pesquisa aplicações de acordo com a situação, ou sigla, ou nome
        /// </summary>
        /// <param name="situacao"></param>
        /// <param name="sigla"></param>
        /// <param name="nome"></param>
        /// <returns></returns>
        [OperationContract]
        List<AplicacaoEntity> GetAplicacoes(string situacao, string sigla, string nome);//, int firstRowIndex, int maximumRows);

        /// <summary>
        /// Retorna todas as aplicações
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<AplicacaoEntity> GetTodasAplicacoes();

        /// <summary>
        /// Retorna todas as aplicações de uma determinada categoria
        /// </summary>
        /// <param name="publicoUID"></param>
        /// <param name="perfilUID"></param>
        /// <returns></returns>
        [OperationContract]
        List<AplicacaoEntity> GetAplicacaoPorCategoria(long publicoUID, long perfilUID);

        /// <summary>
        /// Retorna uma Estatistica das Aplicações Acessadas
        /// </summary>
        /// <returns></returns>
        [OperationContract]
        List<AplicacaoEntity> GetAplicacoesMaisAcessadas();



        /// <summary>
        /// Retorna as aplicações mais acessadas por categoria
        /// </summary>
        /// <param name="publicoUID"></param>
        /// /// <param name="perfilUID"></param>
        /// <returns></returns>
        [OperationContract]
        List<AplicacaoEntity> GetAplicacoesMaisAcessadasPorCategoria(long publicoUID, long perfilUID);

        /// <summary>
        /// Retorna as aplicações acessadas recentemente por um determinado usuario
        /// </summary>
        /// <param name="usuarioUID"></param>
        /// <returns></returns>
        [OperationContract]
        List<AplicacaoEntity> GetAcessadosRecentemente(string usuarioUID, long perfilUID);

        /// <summary>
        /// Retorna todas as Aplicações de um determinado Perfil
        /// </summary>
        /// <param name="perfilUID">IUD dos perfis das aplicações </param>
        /// <returns> Retorna todas as aplicações de um determinado perfil </returns>
        [OperationContract]
        List<AplicacaoEntity> GetAplicacaoPorPerfil(long perfilUID);


        /// <summary>
        /// 
        /// </summary>
        /// <param name="usuarioUID"></param>
        /// <param name="publicoUID"></param>
        /// <param name="perfilUID"></param>
        /// <returns></returns>
        [OperationContract]
        List<AplicacaoEntity> GetAcessadosRecentementePorCategoria(string usuarioUID, long publicoUID, long perfilUID);
    }
}
