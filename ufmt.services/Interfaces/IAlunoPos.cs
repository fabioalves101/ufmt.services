﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

namespace ufmt.services
{
    /// <summary>
    /// Serviços do Aluno de Pos-Graduação
    /// </summary>
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IAlunoPos" in both code and config file together.
    [ServiceContract]
    public interface IAlunoPos
    {
        /// <summary>
        /// Realiza busca de Estudante de um de Pós-Graduação a partir do CPF.
        /// </summary>
        /// <param name="cpf">O CPF do Estudante de Pós-Graduação.</param>
        /// <returns>Objeto "Aluno" com as informações do Estudante de Pós-Graduação.</returns>
        [OperationContract]
        entities.Aluno GetAlunoPosPorCPF(string cpf);

        /// <summary>
        /// Realiza busca de Estudante de um de Pós-Graduação a partir do CPF.
        /// </summary>
        /// <param name="rga">O RGA do Estudante de Pós-Graduação.</param>
        /// <returns>Objeto "Aluno" com as informações do Estudante de Pós-Graduação.</returns>
        [OperationContract]
        entities.Aluno GetAlunoPosPorMatricula(string rga);

        /// <summary>
        /// Atualização de senha do aluno
        /// </summary>
        /// <param name="rga">Do aluno a ser atualizado a senha</param>
        /// <param name="senha_antiga">senha antiga ou senha atual do aluno</param>
        /// <param name="senha_nova">senha nova do aluno</param>
        /// <returns>
        ///     True -> em caso de sucesso na Atualização da senha
        ///     False -> em caso de falha na Atualização da senha
        /// </returns>
        [OperationContract]
        void AtualizaSenhaAluno(string rga, string senha_antiga, string senha_nova);
        
        /// <summary>
        /// Reenvia a senha para o aluno
        /// </summary>
        /// <param name="rga"></param>
        /// <returns>
        /// True -> Sucesso no envio
        /// </returns>
        [OperationContract]
        bool ReenvioSenhaAluno(string rga);
    }
}
