﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ufmt.services.entities;

namespace ufmt.services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IProcesso" in both code and config file together.
    [ServiceContract]
    public interface IProcesso
    {
        [OperationContract]
        ProcessoEntity CadastrarProcesso(string assunto, string requerente, int? nroPaginas, int unidadeDestino);

        [OperationContract]
        ProcessoEntity GetProcesso(string numero);
    }

    [DataContract]
    public enum TipoProcesso
    {
        //1	Tramite Local
        [EnumMember]
        TramiteLocal = 1,
        //2	Parecer/Encaminhamento
        [EnumMember]
        ParecerEncaminhamento = 2,
        //3	Encaminhado/Protocolado
        [EnumMember]
        EncaminhadoProtocolado = 3,
        //4	Arquivado
        [EnumMember]
        Arquivado = 4,
        //5	Apensado
        [EnumMember]
        Apensado = 5,
        //6	Diligência
        [EnumMember]
        Diligencia = 6,
        //7	Diploma Emitido/Retirar
        [EnumMember]
        DiplomaEmitidoRetirar = 7
    }
}
