﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ufmt.services.entities;

namespace ufmt.services
{
    /// <summary>
    /// Serviço de Autenticação do SIGA
    /// </summary>
    [ServiceContract]
    public interface IAutenticacaoSiga
    {
        /// <summary>
        /// Retorna o perfil do usuário do SIGA
        /// </summary>
        /// <param name="cpf"></param>
        [OperationContract]
        List<PerfilSigaEntity> GetPerfil(string cpf);

        /// <summary>
        /// Gera a sessão de banco de dados do SIGA
        /// </summary>
        /// <param name="id"></param>
        /// <param name="nivel"></param>
        /// <param name="idVinculado"></param>
        /// <returns>AcessoSigaEntity</returns>
        [OperationContract]
        AcessoSigaEntity GerarSessao(string id, string nivel, string idVinculado);


    }
}
