﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ufmt.services.entities;

namespace ufmt.services
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IFiscal" in both code and config file together.
    [ServiceContract]
    public interface IFiscal
    {
        [OperationContract]
        List<PessoaFiscalEntity> ConsultaInscricao(string cpf);
    }
}
