﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ufmt.services.entities;
using System.ServiceModel.Activation;
using ufmt.services.extensions;
using System.Data.Common;
using System.Data.SqlClient;
using ufmt.sig.business;

namespace ufmt.services
{
    /// <summary>
    /// Serviço de consulta a informações de curso 
    /// </summary>
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Curso" in code, svc and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class Curso : ICurso
    {
        /// <summary>
        ///  Faz o registro do plano pedagógico de determinado curso 
        /// </summary>
        /// <param name="cursoUID"> UID do curso a ser feito o plano pedagógico</param>
        /// <param name="periodo"> Periodo em que procederá o plano pedagógico </param>
        /// <returns>Retorna os dados do plano pedagógoco</returns>
        public PlanoPedagogico GetPlanoPedagogico(int cursoUID, int periodo)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = 65536 * 5;

            idbufmtEntities db = new idbufmtEntities();
            vwArquivo arquivo = db.vwArquivo.Where(a => a.cursoSiga == cursoUID && a.periodo == periodo).SingleOrDefault();

            PlanoPedagogico ppc = new PlanoPedagogico()
            {
                ppcUID = int.Parse(arquivo.codigoArquivo.ToString()),
                cursoUID = arquivo.cursoSiga,
                periodo = arquivo.periodo,
                conteudo = arquivo.conteudoArquivo,
                nomeArquivo = arquivo.nomeArquivo,
                extensao = arquivo.extensaoArquivo
            };

            return ppc;
        }
        
        /// <summary>
        /// Pesquisa pelo ultimo plano pedagógico de  deternminado curso 
        /// </summary>
        /// <param name="cursoUID">UID do curso a ser verificado </param>
        /// <returns> Retorna Informações do plano pedagógico pesuisado </returns>
        public PlanoPedagogico GetLastPlanoPedagogico(int cursoUID)
        {

            idbufmtEntities db = new idbufmtEntities();

            //(from a in db.vwArquivo where a.cursoSiga == cursoUID orderby a.periodo descending select a).FirstOrDefault();
            vwArquivo arquivo = db.vwArquivo.Where(a => a.cursoSiga == cursoUID).OrderByDescending(a => a.periodo).FirstOrDefault();

            PlanoPedagogico ppc = new PlanoPedagogico()
            {
                ppcUID = int.Parse(arquivo.codigoArquivo.ToString()),
                cursoUID = arquivo.cursoSiga,
                periodo = arquivo.periodo,
                conteudo = arquivo.conteudoArquivo.Compress(),
                nomeArquivo = arquivo.nomeArquivo,
                extensao = arquivo.extensaoArquivo
            };

            return ppc;
        }
        
        /// <summary>
        /// Pesquisa por curso de graduaçao
        /// </summary>
        /// <param name="campus">Campus a ser pesquisado </param>
        /// <returns>Retornar a lista do curso de graduaçao </returns>
        public List<CursoSite> GetCursosGraduacao(int campus)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = 65536 * 2;
            using (idbufmtEntities db = new idbufmtEntities())
            {
                List<CursoSite> listCurso = new List<CursoSite>();

                List<ViewCursosSite> cursos = db.ViewCursosSite
                                                .Where(c => c.sigacampuscurso == campus)
                                                .OrderBy(x => x.siganomecursovest)
                                                .ToList();

                foreach (ViewCursosSite curso in cursos)
                {
                    listCurso.Add(new CursoSite(curso));
                }

                return listCurso;
            }

        }

        /// <summary>
        /// Retorna uma lista de cursos de Pos-graduação por campus, caso não seja passado nada trará todos os campus.
        /// </summary>
        /// <param name="campus">Id do Campus a ser pesquisado</param>
        /// <returns>Lista de Cursos de Pós-Graudação</returns>
        public List<CursoSite> GetCursosPosGraduacao(int campus = 0)
        {
            List<CursoSite> cursos = new List<CursoSite>();
            using (var db = new idbufmtEntities())
            {
                var retorno = db.ProcedureCursosSIPG(campus);

                foreach (var item in retorno)
                {
                    CursoSite curso = new CursoSite()
                    {
                        sigacodcursosiga = item.sipgcodcurcurso.ToString(),
                        siganomecursovest = item.sipgnomecurso,
                        siganomepinstituto = item.SipgnomepInstituto,
                        siganomecp1curso = item.sipgnomecurso,


                        descr01_habilitacao = "",
                        descr02_habilitacao = "",
                        descr03_habilitacao = "",
                        sigaanomaxgradcurso = 0f,
                        sigaanomingradcurso = 0f,
                        sigaautorizacaocurso = "",
                        sigacampuscurso = 0,

                        sigadescrgrau = "",
                        sigadescrregime = "",
                        sigadescrturno = item.Sipgnometurno,
                        sigafonecurso = item.sipgfonecurso,
                        sigahistoricocurso = "",
                        sigamec2reconhecimento = "",
                        sigamecreconhecimento = "",

                        siganomesenha = "",
                        sigaobs2reconhecimento = "",
                        sigaobsreconhecimento = "",
                        sigaperfilcurso = "",
                        sigatipovest = "",
                        totalvagas = 0,
                    };

                    cursos.Add(curso);
                }
            }

            return cursos;
        }

        /// <summary>
        /// Pesquisa de curso de graguaçao por nome
        /// </summary>
        /// <param name="campus">Campus a ser pesquisado</param>
        /// <param name="nome">Nome do curso de graduaçao a ser pesquisado</param>
        /// <returns>Retornar a lista do curso de graduaçao por nome</returns>
        public List<CursoSite> GetCursosGraduacaoPorNome(int campus, string nome)
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = 65536 * 2;
            using (idbufmtEntities db = new idbufmtEntities())
            {
                List<CursoSite> listCurso = new List<CursoSite>();

                List<ViewCursosSite> cursos = db.ViewCursosSite.Where(c => c.sigacampuscurso == campus && c.siganomecursovest.Contains(nome)).ToList();

                foreach (ViewCursosSite curso in cursos)
                {
                    CursoSite c = new CursoSite()
                    {
                        descr01_habilitacao = curso.descr01_habilitacao,
                        descr02_habilitacao = curso.descr02_habilitacao,
                        descr03_habilitacao = curso.descr03_habilitacao,
                        sigaanomaxgradcurso = curso.sigaanomaxgradcurso,
                        sigaanomingradcurso = curso.sigaanomingradcurso,
                        sigaautorizacaocurso = curso.sigaautorizacaocurso,
                        sigacampuscurso = Convert.ToInt32(curso.sigacampuscurso),
                        sigacodcursosiga = curso.sigacodcursosiga,
                        sigadescrgrau = curso.sigadescrgrau,
                        sigadescrregime = curso.sigadescrregime,
                        sigadescrturno = curso.sigadescrturno,
                        sigafonecurso = curso.sigafonecurso,
                        sigahistoricocurso = curso.sigahistoricocurso,
                        sigamec2reconhecimento = curso.sigamec2reconhecimento,
                        sigamecreconhecimento = curso.sigamecreconhecimento,
                        siganomecp1curso = curso.siganomecp1curso,
                        siganomecursovest = curso.siganomecursovest,
                        siganomepinstituto = curso.siganomepinstituto,
                        siganomesenha = curso.siganomesenha,
                        sigaobs2reconhecimento = curso.sigaobs2reconhecimento,
                        sigaobsreconhecimento = curso.sigaobsreconhecimento,
                        sigaperfilcurso = curso.sigaperfilcurso,
                        sigatipovest = curso.sigatipovest,
                        totalvagas = curso.totalvagas
                    };

                    listCurso.Add(c);
                }

                return listCurso;
            }

        }
        
        /// <summary>
        ///  Pesquisa de concorrencia por ano e curso  
        /// </summary>
        /// <param name="ano"> Ano a ser pesquisado </param>
        /// <param name="curso"> Curso a ser pesquisado </param>
        /// <returns> Retorna uma lista com os dados de concorrencia do curso informado </returns>
        public List<ConcorrenciaCurso> GetConcorrencia(int? ano, string curso)
        {
            idbufmtEntities db = new idbufmtEntities();

            List<ViewConcorrencia> listView = new List<ViewConcorrencia>();
            if (ano != 0)
            {
                listView = ((!string.IsNullOrEmpty(curso)) ? db.ViewConcorrencia.Where(c => c.ano == ano.Value && c.nome.Contains(curso)).ToList()
                    : db.ViewConcorrencia.Where(c => c.ano == ano.Value).ToList());
            }
            else
            {
                listView = ((!string.IsNullOrEmpty(curso)) ? db.ViewConcorrencia.Where(c => c.nome.Contains(curso)).ToList()
                    : db.ViewConcorrencia.ToList());
            }

            List<ConcorrenciaCurso> listConcorrencia = new List<ConcorrenciaCurso>();
            foreach (ViewConcorrencia vconc in listView)
            {
                ConcorrenciaCurso concorrencia = new ConcorrenciaCurso()
                {
                    ano = vconc.ano,
                    campus = vconc.campus,
                    concorrencia = vconc.concorrência,
                    inscritos = vconc.inscritos,
                    nomeCurso = vconc.nome,
                    vagas = vconc.vagas
                };

                listConcorrencia.Add(concorrencia);
            }

            return listConcorrencia;
        }
        
        /// <summary>
        /// Pesquisa de planos de ensino 
        /// </summary>
        /// <param name="codigoCurso">Código do curso a ser verificado </param>
        /// <param name="ano"> Ano do plano de ensino a ser pesquisado </param>
        /// <param name="semestre"> Semestre do plano de ensino a ser pesquisado </param>
        /// <returns>Retorna uma lista com os dos planos de ensino com os parametros passados </returns>
        public List<PlanoEnsinoDisciplina> GetPlanosEnsino(int codigoCurso, string ano, string semestre)
        {
            try
            {
                idbufmtEntities db = new idbufmtEntities();
                Periodo periodo = db.Periodo.Where(p => p.ano.Equals(ano) && p.semestre.Equals(semestre)).FirstOrDefault();

                List<PlanoEnsinoDisciplina> planosDisciplina = new List<PlanoEnsinoDisciplina>();
                if (periodo != null)
                {
                    List<PlanoEnsino> planos = new List<PlanoEnsino>();
                    planos =
                        db.PlanoEnsino
                            .Where(pl => pl.periodoUID == periodo.periodoUID && pl.codigoCurso.Equals(codigoCurso))
                            .Where(pl => pl.homologado == true).ToList();

                    foreach (PlanoEnsino plano in planos)
                    {
                        PlanoEnsinoDisciplina ped = new PlanoEnsinoDisciplina()
                        {
                            codigoCurso = plano.codigoCurso,
                            matrizDisciplinarUID = plano.matrizDisciplinarUID,
                            nomeDisciplina = plano.MatrizDisciplinar.nome,
                            periodo = periodo.ano + periodo.semestre,
                            periodoUID = periodo.periodoUID,
                            planoEnsinoUID = plano.planoEnsinoUID,
                            urlDownload = "http://sistemas.ufmt.br/ufmt.planoensino/Download.aspx?disciplinaUID=" + plano.matrizDisciplinarUID + "&codigoCurso=" + codigoCurso + "&periodo=" + periodo.periodoUID

                        };

                        planosDisciplina.Add(ped);
                    }
                }
                else
                {
                    throw new FaultException<Exception>(new Exception("Período Inválido"), new FaultReason("Período Inválido"));
                }

                return planosDisciplina;
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        /// Pesquisa de curso pedágogico por código do curso 
        /// </summary>
        /// <param name="codigoCurso"> Código do curso a ser pesquisado</param>
        /// <returns>Retorna Informações do curso pesquisado </returns>
        public CursoEntity GetCursoPorCodigoSiga(int codigoCurso)
        {
            try
            {
                using (idbufmtEntities db = new idbufmtEntities())
                {
                    ufmt.services.entities.Curso curso = (from c in db.Curso where c.codigoExterno == codigoCurso select c).FirstOrDefault();

                    CursoEntity cursoEntity = new CursoEntity(curso);

                    if (curso.regimeUID.HasValue)
                        cursoEntity.Regime = db.TipoRegime.Find(curso.regimeUID).nome;

                    return cursoEntity;
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        /// Pesquisa de Curso por código assinado 
        /// </summary>
        /// <param name="codigoCurso"> Código do curso a ser pesquisado </param>
        /// <returns>Retorna Informações do curso pesquisado </returns>
        public CursoEntity GetCursoPorCodigoSiged(int codigoCurso)
        {
            try
            {

                idbufmtEntities db = new idbufmtEntities();
                ufmt.services.entities.Curso curso =
                    (from c in db.Curso
                     where c.codigoExterno == codigoCurso && c.tipoIntegracao == "D"
                     select c).FirstOrDefault();

                CursoEntity cursoEntity = new CursoEntity()
                {
                    campusUID = curso.campusUID,
                    codigoExterno = curso.codigoExterno,
                    codigoIntegracao = curso.codigoIntegracao,
                    cursoAssociadoUID = curso.cursoAssociadoUID,
                    cursoUID = curso.cursoUID,
                    dataFundacao = curso.dataFundacao,
                    instituicaoConvenioUID = curso.instituicaoConvenioUID,
                    nivelCursoUID = curso.nivelCursoUID,
                    nome = curso.nome,
                    tipoCursoUID = curso.tipoCursoUID,
                    tipoIntegracao = curso.tipoIntegracao,
                    unidadeOfertanteUID = curso.unidadeOfertanteUID
                };

                return cursoEntity;
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        /// Pesquisa de cursos por UID 
        /// </summary>
        /// <param name="cursoUID"> UID do curso a ser pesquisado </param>
        /// <returns>Informações do curso a ser pesquisado </returns>
        public CursoEntity GetCurso(long cursoUID)
        {
            try
            {
                return getCursoPorCodigoExterno(cursoUID);
            }
            catch (Exception)
            {
                return getCursoPorCodigo(cursoUID);
            }
        }
        
        /// <summary>
        /// Pesquisa um curso pelo codigo do curso
        /// </summary>
        /// <param name="cursoUID">UID do curso a ser pesquisado</param>
        /// <returns>Retorna uma Instancia de curso</returns>
        private CursoEntity getCursoPorCodigo(long cursoUID)
        {
            using (var db = new idbufmtEntities())
            {
                var curso = db.Curso.Where(x => x.cursoUID == cursoUID).First();

                CursoEntity cursoEntity = new CursoEntity(curso);

                return cursoEntity;
            }
        }
        
        /// <summary>
        /// Pesquisa um curso por Codigo Externo
        /// </summary>
        /// <param name="cursoUID">UID do curso a ser pesquisado</param>
        /// <returns>Uma instancia de curso</returns>
        private CursoEntity getCursoPorCodigoExterno(long cursoUID)
        {
            using (var db = new idbufmtEntities())
            {
                var curso = db.Curso.Where(x => x.codigoExterno == cursoUID).First();

                CursoEntity cursoEntity = new CursoEntity(curso);

                return cursoEntity;
            }
        }
        
        /// <summary>
        /// Pesquisa de cursos por campus
        /// </summary>
        /// <param name="campusUID">UID do campus a ser pesquisado</param>
        /// <returns>Retorna as informaçoes do curso a ser pesquisado</returns>
        public List<CursoEntity> GetCursos(int campusUID)
        {
            try
            {
                idbufmtEntities db = new idbufmtEntities();
                List<ufmt.services.entities.Curso> cursos = (from c in db.Curso
                                                             where
                                                                 c.campusUID == campusUID
                                                             select c).ToList();

                List<CursoEntity> cursoEntityList = new List<CursoEntity>();

                foreach (ufmt.services.entities.Curso curso in cursos)
                {
                    CursoEntity cursoEntity = new CursoEntity()
                    {
                        campusUID = curso.campusUID,
                        codigoExterno = curso.codigoExterno,
                        codigoIntegracao = curso.codigoIntegracao,
                        cursoAssociadoUID = curso.cursoAssociadoUID,
                        cursoUID = curso.cursoUID,
                        dataFundacao = curso.dataFundacao,
                        instituicaoConvenioUID = curso.instituicaoConvenioUID,
                        nivelCursoUID = curso.nivelCursoUID,
                        nome = curso.nome,
                        tipoCursoUID = curso.tipoCursoUID,
                        tipoIntegracao = curso.tipoIntegracao,
                        unidadeOfertanteUID = curso.unidadeOfertanteUID
                    };

                    cursoEntityList.Add(cursoEntity);
                }

                return cursoEntityList;
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        protected void GetCursoPosPorCodigo(long cursoUID)
        {
            using (var db = new idbufmtEntities())
            {
                string sql = "Select sipgcodcurcurso, sipgperinicurso,sipgperfimcurso, sipgnomecurso, " +
                                     "sipgcodarea, sipgnomearea,sipgcodcampuscurso, " +
                                     "sipgCodAnoEstrEstrutura,sipgSemOferEstrutura,sipgdescrDisciplina, " +
                                     "sipgcrgHorTeorEstrutura + sipgcrgHorPratEstrutura as totalcrgHoraria " +
                                     "from STI196.dbsipg.dbo.sipgcurso left outer join STI196.dbsipg.dbo.SipgArea On (sipgcodcurcurso = SipgcodcurArea " +
                                     "and sipgnomearea not like '%aluno especial%' and sipgcodsituarea = '6' ) " +
                                     "left outer join STI196.dbsipg.dbo.SipgEstrutura On (sipgCodCurEstrutura = sipgcodcurcurso " +
                                     "and SiPgCodAreaEstrutura = sipgcodarea and Sipgcodcurestrutura =  SipgcodcurArea ) " +
                                     "left outer join STI196.dbsipg.dbo.SipgDisciplina On (SipgCoddiscEstrutura = SipgCodDisciplina) " +
                                     "where  sipgcodcurcurso = @CodCurso and sipgsitucurso = 6 " +
                                     "Order By sipgCodAnoEstrEstrutura,SiPgCodAreaEstrutura, SipgSemOferEstrutura,sipgCodDiscEstrutura";
                var retorno = db.Database.SqlQuery<string>(sql, new { CodCurso = cursoUID }).ToList();
            }
        }
        
        /// <summary>
        /// Pesquisas das Matriz Curriculares por curso 
        /// </summary>
        /// <param name="cursoUID">UID do curso a ser verificado </param>
        /// <returns>Retorna lista de informaçoes da matriz Curricular pesquisada </returns>
        public List<MatrizCurricularEntity> GetMatrizesCurriculares(long cursoUID)
        {
            CursoEntity curso = this.GetCurso(cursoUID);

            List<MatrizCurricularEntity> matrizesE = new List<MatrizCurricularEntity>();

            using (ICursoBO cursoBO = BusinessFactory.GetInstance<ICursoBO>())
            {
                List<ufmt.sig.entity.MatrizCurricular> matrizes =
                    cursoBO.GetMatrizesCurricularesAtivas(curso.campusUID,
                    curso.nivelCursoUID, cursoUID, null, null).ToList();


                foreach (ufmt.sig.entity.MatrizCurricular matriz in matrizes)
                {
                    MatrizCurricularEntity matrizE = new MatrizCurricularEntity()
                    {
                        Descricao = matriz.Descricao,
                        FormatoCursoUID = matriz.FormatoCurso.FormatoCursoUID,
                        MatrizCurricularUID = matriz.MatrizCurricularUID,
                        ModalidadeCursoUID = matriz.ModalidadeCurso.ModalidadeCursoUID,
                        NumeroDeSemanas = matriz.NumeroDeSemanas,
                        PeriodicidadeCursoUID = matriz.PeriodicidadeCurso.PeriodicidadeCursoUID,
                        PeriodoFinalFuncionamento = matriz.PeriodoFinalFuncionamento,
                        PeriodoInicialFuncionamento = matriz.PeriodoInicialFuncionamento,
                        RegimeCursoUID = matriz.RegimeCurso.RegimeCursoUID,
                        SituacaoCursoUID = matriz.SituacaoCurso.SituacaoCursoUID,
                        TurnoCursoUID = matriz.TurnoCurso.TurnoCursoUID,
                        Curso = curso
                    };

                    matrizesE.Add(matrizE);
                }
            }

            return matrizesE;
        }
        
        /// <summary>
        /// Pesuqisa de cursos por coordenador 
        /// </summary>
        /// <param name="codigoCoordenador"> Código do cordenador a ser pesquisado </param>
        /// <returns> Retorna uma lista de Informações do curso pesquisado </returns>
        public List<CursoEntity> GetCursosPorCoordenador(int codigoCoordenador)
        {
            idbufmtEntities db = new idbufmtEntities();
            List<vwCoordenadores> coordenadores = db.vwCoordenadores.Where(c => c.coordenadorUID == codigoCoordenador).ToList();


            List<CursoEntity> cursos = new List<CursoEntity>();
            foreach (vwCoordenadores coordenador in coordenadores)
            {
                ufmt.services.entities.Curso curso = db.Curso.Where(cur => cur.codigoExterno == coordenador.codigoCurso).FirstOrDefault();

                CursoEntity cursoEntity = new CursoEntity()
                {
                    campusUID = curso.campusUID,
                    codigoExterno = curso.codigoExterno,
                    codigoIntegracao = curso.codigoIntegracao,
                    cursoAssociadoUID = curso.cursoAssociadoUID,
                    cursoUID = curso.cursoUID,
                    dataFundacao = curso.dataFundacao,
                    instituicaoConvenioUID = curso.instituicaoConvenioUID,
                    nivelCursoUID = curso.nivelCursoUID,
                    nome = curso.nome,
                    tipoCursoUID = curso.tipoCursoUID,
                    tipoIntegracao = curso.tipoIntegracao,
                    unidadeOfertanteUID = curso.unidadeOfertanteUID
                };

                cursos.Add(cursoEntity);
            }

            return cursos;
        }
        
        /// <summary>
        /// Pequisa de Matrize curricular por código externo 
        /// </summary>
        /// <param name="codigoCurso">Código do curso para verificar as matrizes curriculares </param>
        /// <param name="apenasAtivas">Filtro de atividade da matriz curricular a ser pesquisada </param>
        /// <returns> Retorna uma lista de informações das matrizes curriculares com os valores passados por parâmetro </returns>
        public List<MatrizCurricularEntity> GetMatrizesCurricularesPorCodigoExterno(int codigoCurso, bool apenasAtivas)
        {
            try
            {
                CursoEntity curso = this.GetCursoPorCodigoSiga(codigoCurso);

                List<MatrizCurricularEntity> matrizesE = new List<MatrizCurricularEntity>();

                using (ICursoBO cursoBO = BusinessFactory.GetInstance<ICursoBO>())
                {
                    List<ufmt.sig.entity.MatrizCurricular> matrizes = new List<sig.entity.MatrizCurricular>();

                    if (apenasAtivas)
                    {
                        matrizes = cursoBO.GetMatrizesCurricularesAtivas(curso.campusUID,
                                    curso.nivelCursoUID, curso.cursoUID, null, null).ToList();
                    }
                    else
                    {
                        matrizes = cursoBO.GetMatrizesCurriculares(curso.campusUID,
                                    curso.nivelCursoUID, curso.cursoUID, null, null).ToList();
                    }

                    foreach (ufmt.sig.entity.MatrizCurricular matriz in matrizes)
                    {
                        MatrizCurricularEntity matrizE = new MatrizCurricularEntity()
                        {
                            Descricao = matriz.Descricao,
                            FormatoCursoUID = matriz.FormatoCurso.FormatoCursoUID,
                            MatrizCurricularUID = matriz.MatrizCurricularUID,
                            ModalidadeCursoUID = matriz.ModalidadeCurso.ModalidadeCursoUID,
                            NumeroDeSemanas = matriz.NumeroDeSemanas,
                            PeriodicidadeCursoUID = matriz.PeriodicidadeCurso.PeriodicidadeCursoUID,
                            PeriodoFinalFuncionamento = matriz.PeriodoFinalFuncionamento,
                            PeriodoInicialFuncionamento = matriz.PeriodoInicialFuncionamento,
                            RegimeCursoUID = matriz.RegimeCurso.RegimeCursoUID,
                            SituacaoCursoUID = matriz.SituacaoCurso.SituacaoCursoUID,
                            TurnoCursoUID = matriz.TurnoCurso.TurnoCursoUID,
                            Curso = curso
                        };

                        matrizesE.Add(matrizE);
                    }
                }

                return matrizesE;

            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        ///  Pesquisa de periodos por atividade
        /// </summary>
        /// <param name="apenasAtivos">filtro de  atividade dos periodos pesquisados  </param>
        /// <returns> Retorna uma lista de com informações dos periodos pesquisados </returns>
        public List<PeriodoEntity> GetPeriodos(bool apenasAtivos)
        {
            try
            {
                using (idbufmtEntities db = new idbufmtEntities())
                {
                    List<Periodo> periodoList = new List<Periodo>();
                    if (apenasAtivos)
                        periodoList = db.Periodo.Where(p => p.ativo == true).ToList();
                    else
                        periodoList = db.Periodo.ToList();

                    List<PeriodoEntity> periodoEntityList = new List<PeriodoEntity>();
                    foreach (var periodo in periodoList)
                    {
                        PeriodoEntity periodoEntity = new PeriodoEntity()
                        {
                            Ano = periodo.ano,
                            Ativo = periodo.ativo,
                            PeriodoUID = periodo.periodoUID,
                            Semestre = periodo.semestre
                        };

                        periodoEntityList.Add(periodoEntity);
                    }

                    return periodoEntityList;
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        /// Faz conversão de Curso para cursoEntity
        /// </summary>
        /// <param name="curso"> Curso a ser convertido </param>
        /// <param name="infoSiga"></param>
        /// <returns>Retorna uma instancia de CursoEntity</returns>
        private CursoEntity CastCursoToCursoEntity(ufmt.sig.entity.Curso curso, bool infoSiga)
        {
            CursoEntity cursoEntity = new CursoEntity(curso);

            if (infoSiga)
            {
                using (idbufmtEntities db = new idbufmtEntities())
                {
                    ViewCursoSiga cursoSiga = db.ViewCursoSiga.Where(vc =>
                                                vc.SigaCodCurso == curso.CodigoExterno &&
                                                vc.SigaPerFimCurso == 99999).FirstOrDefault();

                    cursoEntity.Regime = cursoSiga.SigaRegimeCurso.ToString();
                }
            }

            return cursoEntity;
        }
    }
}
