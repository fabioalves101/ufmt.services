﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ufmt.services.entities
{
    interface IPesquisas<TEntity> where TEntity : class
    {
        // IQueryable de TEntity é o retorno no método Get, o qual será
        // aplicado um filtro dinâmico no predicate
        // p => p.Preco > 40
        // c => c.NomeCliente.Contains("a")
        IQueryable<TEntity> Get(Func<TEntity, bool> predicate, idbufmtEntities db, int inicial, int quantity);
    }

    public class PesquisaGenerica<TEntity> : IPesquisas<TEntity> where TEntity : class
    {
        // Func recebe a entidade/classe (ex produto)
        // a ser usada na pesquisa, portanto, é dinâmica
        public IQueryable<TEntity> Get(Func<TEntity, bool> predicate, idbufmtEntities db, int inicial, int quantity)
        {
            //TEntity = é uma classe, ex Produtos, Clientes
            // predicate = é a expressão de filtro
            // p => p.Preco > 10
            // AsQueryable = converte para uma lista consultável
            // .Set<> referencia a entidade dinamicamente
            return db.Set<TEntity>().Where(predicate).Skip(inicial).Take(quantity).AsQueryable();
        }
    }
}