﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace ufmt.services.entities
{
    [DataContract]
    public class AplicacaoEntity
    {
        public AplicacaoEntity() { }

        public AplicacaoEntity(ufmt.services.entities.Aplicacao aplicacaoSig)
        {
            this.AplicacaoUID = aplicacaoSig.aplicacaoUID;
            this.Icone = aplicacaoSig.icone;
            this.Descricao = aplicacaoSig.descricao;
            this.Nome = aplicacaoSig.nome;
            this.Objetivo = aplicacaoSig.objetivo;
            this.Plataforma = aplicacaoSig.plataforma;
            this.RequisitosMinimos = aplicacaoSig.requisitosMinimos;
            this.Sgbd = aplicacaoSig.sgbd;
            this.Sigla = aplicacaoSig.sigla;
            this.SistemaOperacional = aplicacaoSig.sistemaOperacional;
            this.Situacao = aplicacaoSig.situacao;
            this.Url = aplicacaoSig.url;
            this.Versao = aplicacaoSig.versao;
        }

        public AplicacaoEntity(ufmt.services.entities.AplicacaoEntity app)
        {
            this.AplicacaoUID = app.AplicacaoUID;
            this.Descricao = app.Descricao;
            this.Nome = app.Nome;
            this.SiglaNome = app.Sigla + " - " + app.Nome;
            this.Icone = app.Icone;
            this.Objetivo = app.Objetivo;
            this.Plataforma = app.Plataforma;
            this.RequisitosMinimos = app.RequisitosMinimos;
            this.Sgbd = app.Sgbd;
            this.Sigla = app.Sigla;
            this.SistemaOperacional = app.SistemaOperacional;
            this.Situacao = app.Situacao;
            this.Url = app.Url;
            this.Versao = app.Versao;
            this.Chave = app.Chave;
            this.EmailAdministrador = app.EmailAdministrador;
            this.Integrado = app.Integrado;
        }

        public AplicacaoEntity(ufmt.sig.entity.Aplicacao app) {
            this.AplicacaoUID = app.AplicacaoUID;
            this.Descricao = app.Descricao;
            this.Nome = app.Nome;
            this.SiglaNome = app.Sigla + " - " + app.Nome;
            this.Icone = app.Icone;
            this.Objetivo = app.Objetivo;
            this.Plataforma = app.Plataforma;
            this.RequisitosMinimos = app.RequisitosMinimos;
            this.Sgbd = app.Sgbd;
            this.Sigla = app.Sigla;
            this.SistemaOperacional = app.SistemaOperacional;
            this.Situacao = app.Situacao;
            this.Url = app.Url;
            this.Versao = app.Versao;
            this.Chave = "";
            this.EmailAdministrador = "";
        }

        [DataMember]
        public long AplicacaoUID { get; set; }
        [DataMember]
        public string Descricao { get; set; }
        [DataMember]
        public string Nome { get; set; }
        [DataMember]
        public string SiglaNome { get; set; }
        [DataMember]
        public byte[] Icone { get; set; }
        [DataMember]
        public string Objetivo { get; set; }
        [DataMember]
        public string Plataforma { get; set; }
        [DataMember]
        public string RequisitosMinimos { get; set; }
        [DataMember]
        public string Sgbd { get; set; }
        [DataMember]
        public string Sigla { get; set; }
        [DataMember]
        public string SistemaOperacional { get; set; }
        [DataMember]
        public string Situacao { get; set; }
        [DataMember]
        public string Url { get; set; }
        [DataMember]
        public string Versao { get; set; }
        [DataMember]
        public string Chave { get; set; }
        [DataMember]
        public string EmailAdministrador { get; set; }
        [DataMember]
        public int Qtde { get; set; }
        [DataMember]
        public bool Integrado { get; set; }
    }
}