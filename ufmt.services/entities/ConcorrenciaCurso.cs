﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ufmt.services.entities
{
    public class ConcorrenciaCurso
    {
        public string campus;
        public string nomeCurso;
        public int inscritos;
        public int vagas;
        public decimal concorrencia;
        public int ano;
    }
}