﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace ufmt.services.entities
{
    public class Ementa
    {
        [DataMember]
        public long ementaUID;
        [DataMember]
        public string ementaDisciplina;
        [DataMember]
        public int cursoSiga;
        [DataMember]
        public long codigoDisciplina;
        [DataMember]
        public bool ativa;
        [DataMember]
        public int periodo;
    }
}