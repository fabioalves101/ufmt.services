//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ufmt.services.entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class GetDadosAlunoPos1
    {
        public long rga { get; set; }
        public string nomeAluno { get; set; }
        public string maeAluno { get; set; }
        public string paiAluno { get; set; }
        public string curso { get; set; }
        public int codidoCurso { get; set; }
        public string dataNascimento { get; set; }
        public string Sexo { get; set; }
        public string natural { get; set; }
        public string nascionalidade { get; set; }
        public string CPF { get; set; }
        public string RG { get; set; }
        public string telefone1 { get; set; }
        public string telefone2 { get; set; }
        public string Email { get; set; }
        public string raca { get; set; }
        public string senha { get; set; }
        public Nullable<int> campusUID { get; set; }
        public string campus { get; set; }
        public string NomeBanco { get; set; }
        public Nullable<int> codigoBanco { get; set; }
        public string agencia { get; set; }
        public string tipoConta { get; set; }
        public string numeroConta { get; set; }
        public string CodigoFebrabram { get; set; }
        public string enderecoAluno { get; set; }
        public string cidadeAluno { get; set; }
        public string tipoVagaIngresso { get; set; }
    }
}
