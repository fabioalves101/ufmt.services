﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using ufmt.sig.entity;

namespace ufmt.services.entities
{
    [DataContract]
    public class PermissaoUsuarioEntity
    {
        public PermissaoUsuarioEntity() { }

        public PermissaoUsuarioEntity(PermissaoUsuario p)
        {
            this.DataLimite = p.DataLimite;
            this.Permissao = new PermissaoEntity(p.Permissao);
            this.PermissaoUsuarioUID = p.PermissaoUsuarioUID;
            this.Usuario = new UsuarioEntity(p.Usuario);
        }

        [DataMember]
        public DateTime? DataLimite { get; set; }
        [DataMember]
        public PermissaoEntity Permissao { get; set; }
        [DataMember]
        public long PermissaoUsuarioUID { get; set; }
        [DataMember]
        public UsuarioEntity Usuario { get; set; }
    }
}