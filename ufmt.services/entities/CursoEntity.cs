﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ufmt.services.entities
{
    public class CursoEntity
    {
        public long cursoUID { get; set; }
        public Nullable<long> codigoExterno { get; set; }
        public string nome { get; set; }
        public Nullable<System.DateTime> dataFundacao { get; set; }
        public string codigoIntegracao { get; set; }
        public string tipoIntegracao { get; set; }
        public int nivelCursoUID { get; set; }
        public int tipoCursoUID { get; set; }
        public Nullable<long> cursoAssociadoUID { get; set; }
        public Nullable<int> instituicaoConvenioUID { get; set; }
        public int campusUID { get; set; }
        public Nullable<long> unidadeOfertanteUID { get; set; }
        public String Regime { get; set; }

        public CursoEntity() { }

        public CursoEntity(ufmt.services.entities.Curso curso)
        {
            if (curso != null)
            {
                this.campusUID = curso.campusUID;
                this.codigoExterno = curso.codigoExterno;
                this.codigoIntegracao = curso.codigoIntegracao;
                this.cursoAssociadoUID = curso.cursoAssociadoUID;
                this.cursoUID = curso.cursoUID;
                this.dataFundacao = curso.dataFundacao;
                this.instituicaoConvenioUID = curso.instituicaoConvenioUID;
                this.nivelCursoUID = curso.nivelCursoUID;
                this.nome = curso.nome;
                this.tipoCursoUID = curso.tipoCursoUID;
                this.tipoIntegracao = curso.tipoIntegracao;
                this.unidadeOfertanteUID = curso.unidadeOfertanteUID;
            }
        }

        public CursoEntity(ufmt.sig.entity.Curso curso)
        {
            if (curso != null)
            {
                this.campusUID = curso.Campus.CampusUID;
                this.codigoExterno = curso.CodigoExterno;
                this.codigoIntegracao = curso.CodigoIntegracao;
                this.cursoUID = curso.CursoUID;
                this.nome = curso.Nome;
                this.tipoIntegracao = curso.TipoIntegracao.ToString();
                this.cursoAssociadoUID = (curso.CursoAssociado != null) ? curso.CursoAssociado.CursoUID : 0;
                this.dataFundacao = (curso.DataFundacao.HasValue) ? curso.DataFundacao.Value : new DateTime();
                this.nivelCursoUID = (curso.NivelCurso != null) ? curso.NivelCurso.NivelCursoUID : 0;
                this.tipoCursoUID = (curso.TipoCurso != null) ? curso.TipoCurso.TipoCursoUID : 0;
                this.unidadeOfertanteUID = (curso.UnidadeOfertante != null) ? curso.UnidadeOfertante.UnidadeUID : 0;
            }
        }
    }
}