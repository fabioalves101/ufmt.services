﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ufmt.services.entities
{
    public class AcessoSigaEntity
    {
        [DataMember]
        public string IdAcesso { get; set; }
        [DataMember]
        public string ChaveAcesso { get; set; }
    }
}