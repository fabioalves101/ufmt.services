﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ufmt.services.entities
{
    [DataContract]
    public class UsuarioEntity
    {
        public UsuarioEntity(Usuario1 user)
        {
            this.Bloqueado = user.bloqueado;
            this.DataProximaTrocaSenha = user.dataProximaTrocaSenha;
            this.DataUltimoAcesso = user.dataUltimoAcesso;
            this.NomeAcesso = user.nomeAcesso;
            this.Pessoa = new PessoaEntity();
            this.Pessoa.Nome = user.Pessoa.nome;
            this.Pessoa.PessoaUID = user.Pessoa.pessoaUID;
            this.UsuarioUID = user.usuarioUID;
        }

        public UsuarioEntity(ufmt.sig.entity.Usuario user)
        {
            this.Bloqueado = user.Bloqueado;
            this.DataProximaTrocaSenha = user.DataProximaTrocaSenha;
            this.DataUltimoAcesso = user.DataUltimoAcesso;
            this.NomeAcesso = user.NomeAcesso;
            this.Pessoa = new PessoaEntity(user.Pessoa);
            this.UsuarioUID = user.UsuarioUID;
        }

        [DataMember]
        public bool Bloqueado { get; set; }
        [DataMember]
        public DateTime? DataProximaTrocaSenha { get; set; }
        [DataMember]
        public DateTime? DataUltimoAcesso { get; set; }
        [DataMember]
        public string NomeAcesso { get; set; }
        [DataMember]
        public PessoaEntity Pessoa { get; set; }
        [DataMember]
        public long UsuarioUID { get; set; }
    }
}