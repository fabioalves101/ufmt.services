﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ufmt.services.entities
{
    public class PessoaEntity : ufmt.sig.entity.Pessoa
    {
        public UnidadeEntity Unidade { get; set; }
        public string Siape { get; set; }
        public long ServidorUID { get; set; }

        public Tipo TipoPessoa { get; set; }

        public SituacaoDoServidor situacaoDoServidor = SituacaoDoServidor.Ativo;

        public string LinkLattes { get; set; }
        public byte[] ImagemPerfil { get; set; }

        public PessoaEntity() { }

        public PessoaEntity(ufmt.sig.entity.Pessoa Pessoa)
        {
            this.Cpf = Pessoa.Cpf;
            this.Ctps = Pessoa.Ctps;
            this.CtpsSerie = Pessoa.CtpsSerie;
            this.CtpsUf = Pessoa.CtpsUf;
            this.EnderecoBairro = Pessoa.EnderecoBairro;
            this.EnderecoCep = Pessoa.EnderecoCep;
            this.EnderecoComplemento = Pessoa.EnderecoComplemento;
            this.EnderecoLogradouro = Pessoa.EnderecoLogradouro;
            this.EnderecoMunicipio = Pessoa.EnderecoMunicipio;
            this.EnderecoNumero = Pessoa.EnderecoNumero;
            this.EnderecoUf = Pessoa.EnderecoUf;
            this.Mail = Pessoa.Mail;
            this.NascimentoData = Pessoa.NascimentoData;
            this.Nome = Pessoa.Nome;
            this.NomeMae = Pessoa.NomeMae;
            this.NomePai = Pessoa.NomePai;
            this.PessoaUID = Pessoa.PessoaUID;
            this.PisPasep = Pessoa.PisPasep;
            this.Rg = Pessoa.Rg;
            this.RgDataExpedicao = Pessoa.RgDataExpedicao;
            this.RgOrgaoExpedidor = Pessoa.RgOrgaoExpedidor;
            this.RgUf = Pessoa.RgUf;
            this.Sexo = Pessoa.Sexo;
            this.TelefoneComercial = Pessoa.TelefoneComercial;
            this.TelefonePrimario = Pessoa.TelefonePrimario;
            this.TelefoneSecundario = Pessoa.TelefoneSecundario;
            this.TipoTitulacao = Pessoa.TipoTitulacao;
            this.TituloEleitor = Pessoa.TituloEleitor;
            this.TituloEleitorDataEmissao = Pessoa.TituloEleitorDataEmissao;
            this.TituloEleitorSecao = Pessoa.TituloEleitorSecao;
            this.TituloEleitorUf = Pessoa.TituloEleitorUf;
            this.TituloEleitorZona = Pessoa.TituloEleitorZona;

            using (var db = new idbufmtEntities())
            {
                var servidor = //pessoaBO.GetServidorAtivo(Pessoa.pessoaUID);
                    db.Pessoa.Where(x => x.pessoaUID == this.PessoaUID).First().Servidor.LastOrDefault(x => x.situacaoServidorUID == 1);

                if (servidor == null)
                {
                    servidor = db.Pessoa.Where(x => x.pessoaUID == this.PessoaUID).First().Servidor.LastOrDefault();
                }

                if (servidor != null)
                {
                    this.ServidorUID = servidor.servidorUID;
                    this.Siape = servidor.registro;

                    if (Enum.IsDefined(typeof(SituacaoDoServidor), servidor.SituacaoServidor.situacaoServidorUID))
                        this.situacaoDoServidor = (SituacaoDoServidor)Enum.ToObject(typeof(SituacaoDoServidor), servidor.SituacaoServidor.situacaoServidorUID);

                    if (servidor.unidadeLotacaoUID.HasValue)
                        this.Unidade = new Localizacao().GetUnidade(servidor.unidadeLotacaoUID.Value);
                }
            }
        }

        public PessoaEntity(ufmt.services.entities.Pessoa Pessoa)
        {
            this.Cpf = Pessoa.cpf;
            this.Ctps = Pessoa.ctps;
            this.CtpsSerie = Pessoa.ctpsSerie;
            this.CtpsUf = Pessoa.ctpsUF;
            this.EnderecoBairro = Pessoa.enderecoBairro;
            this.EnderecoCep = Pessoa.enderecoCEP;
            this.EnderecoComplemento = Pessoa.enderecoComplemento;
            this.EnderecoLogradouro = Pessoa.enderecoLogradouro;
            this.EnderecoMunicipio = Pessoa.enderecoMunicipio;
            this.EnderecoNumero = Pessoa.enderecoNumero;
            this.EnderecoUf = Pessoa.enderecoUF;
            this.Mail = Pessoa.mail;
            this.NascimentoData = Pessoa.nascimentoData;
            this.Nome = Pessoa.nome;
            this.NomeMae = Pessoa.nomeMae;
            this.NomePai = Pessoa.nomePai;
            this.PessoaUID = Pessoa.pessoaUID;
            this.PisPasep = Pessoa.pisPasep;
            this.Rg = Pessoa.rg;
            this.RgDataExpedicao = Pessoa.rgDataExpedicao;
            this.RgOrgaoExpedidor = Pessoa.rgOrgaoExpedidor;
            this.RgUf = Pessoa.rgUF;
            this.TelefoneComercial = Pessoa.telefoneComercial;
            this.TelefonePrimario = Pessoa.telefonePrimario;
            this.TelefoneSecundario = Pessoa.telefoneSecundario;
            this.TituloEleitor = Pessoa.tituloEleitor;
            this.TituloEleitorSecao = Pessoa.tituloEleitorSecao;
            this.TituloEleitorUf = Pessoa.tituloEleitorUF;
            this.TituloEleitorZona = Pessoa.tituloEleitorZona;
            this.Sexo = Pessoa.sexo.FirstOrDefault();
            //this.TipoTitulacao = Pessoa.tipoTitulacaoUID;
            //this.TituloEleitorDataEmissao = Pessoa.tituloEleitorDataEmissao;

            //using (var pessoaBO = sig.business.BusinessFactory.GetInstance<sig.business.IPessoaBO>())
            using (var db = new idbufmtEntities())
            {
                var servidor = //pessoaBO.GetServidorAtivo(Pessoa.pessoaUID);
                    Pessoa.Servidor.LastOrDefault(x => x.situacaoServidorUID == 1);

                if (servidor == null)
                {
                    servidor = Pessoa.Servidor.LastOrDefault();
                }

                if (servidor != null)
                {
                    this.ServidorUID = servidor.servidorUID;
                    this.Siape = servidor.registro;

                    if (Enum.IsDefined(typeof(SituacaoDoServidor), servidor.SituacaoServidor.situacaoServidorUID))
                        this.situacaoDoServidor = (SituacaoDoServidor)Enum.ToObject(typeof(SituacaoDoServidor), servidor.SituacaoServidor.situacaoServidorUID);

                    if (servidor.unidadeLotacaoUID.HasValue)
                        this.Unidade = new Localizacao().GetUnidade(servidor.unidadeLotacaoUID.Value);
                }

                try
                {
                    var pp = db.PerfilPessoa.Where(x => x.pessoaUID == Pessoa.pessoaUID).First();
                    this.ImagemPerfil = pp.imagem;
                    this.LinkLattes = pp.linkLattes;
                }
                catch (Exception)
                {
                }
            }
        }
    }

    public enum Tipo
    {
        Servidor,
        AlunoGraduacao,
        AlunoPos
    }
}