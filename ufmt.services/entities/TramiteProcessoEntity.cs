﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ufmt.services.EntityModels;

namespace ufmt.services.entities
{
    public class TramiteProcessoEntity
    {
        public TramiteProcessoEntity() { }

        public TramiteProcessoEntity(tbTramite tramite)
        {
            this.Controle = tramite.CONTROLE;
            this.Numero = tramite.NUMERO;
            this.Origem = tramite.ORIGEM;
            this.DataHoraEntrada = Convert.ToDateTime(tramite.DATAENT.ToString().Replace("00:00:00", tramite.HORAENT.ToString()));
            this.Parecer = tramite.PARECER;
            try
            {
                this.DataHoraSaida = Convert.ToDateTime(tramite.DATASAI.ToString() + " " + tramite.HORASAI.ToString());
            }
            catch (Exception)
            {
                this.DataHoraSaida = null;
            }
            
            this.Destino = tramite.DESTINO;
            this.Situacao = tramite.SITUACAO;
            this.Usuario = tramite.USUARIO;
            this.Imprime = tramite.imprime;
            this.ProcessoId = tramite.processoID;
            this.OrigemId = tramite.origemID;
            this.DestinoId = tramite.destinoID;
        }

        public int? Controle { get; set; }
        public string Numero { get; set; }
        public double? Origem { get; set; }
        public DateTime DataHoraEntrada { get; set; }
        public string Parecer { get; set; }
        public DateTime? DataHoraSaida { get; set; }
        public double? Destino { get; set; }
        public short? Situacao { get; set; }
        public string Usuario { get; set; }
        public string Imprime { get; set; }
        public int? ProcessoId { get; set; }
        public int? TramiteId { get; set; }
        public int? OrigemId { get; set; }
        public int? DestinoId { get; set; }
    }
}