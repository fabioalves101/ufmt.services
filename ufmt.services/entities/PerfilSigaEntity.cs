﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using ufmt.services.EntityModels;

namespace ufmt.services.entities
{
    public class PerfilSigaEntity
    {
        private GetPerfilUsuarioSIGA_Result p;

        public PerfilSigaEntity() { }

        public PerfilSigaEntity(GetPerfilUsuarioSIGA_Result p)
        {
            this.CPF = p.CPF;
            this.Campus = p.CAMPUS;
            this.Id = p.IDUSER.ToString();
            this.IdVinculado = p.IDVINCULADO.ToString();
            this.NomeUsuario = p.SigaNomeTProf;
            this.Perfil = p.PERFIL.ToCharArray().FirstOrDefault();
            this.Descricao = p.DescricaoPerfil;
        }

        [DataMember]
        public string CPF { get; set; }
        [DataMember]
        public char Perfil { get; set; }
        [DataMember]
        public string Descricao { get; set; }
        [DataMember]
        public string Id { get; set; }
        [DataMember]
        public string IdVinculado { get; set; }
        [DataMember]
        public string NomeUsuario { get; set; }
        [DataMember]
        public string Campus { get; set; }

    }
}