﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace ufmt.services.entities
{
    [DataContract]
    public class Aluno
    {
        [DataMember]
        public string Rga;

        [DataMember]
        public decimal Matricula;

        [DataMember]
        public string NomeAluno;

        [DataMember]
        public string Curso;

        [DataMember]
        public int CursoUID;

        [DataMember]
        public DateTime DataNascimento;

        [DataMember]
        public string Sexo;

        [DataMember]
        public string NomeCampus;

        [DataMember]
        public int CodigoCurso;

        [DataMember]
        public string Email;

        [DataMember]
        public string Cpf;

        [DataMember]
        public string Rg;

        [DataMember]
        public string Endereco;

        [DataMember]
        public string Cidade;

        [DataMember]
        public string UF;

        [DataMember]
        public string CEP;

        [DataMember]
        public int? CampusUID;

        [DataMember]
        public string Agencia;

        [DataMember]
        public string NumeroConta;

        [DataMember]
        public string CodigoBanco;

        [DataMember]
        public string NomeBanco;

        [DataMember]
        public string ano;

        [DataMember]
        public string semestre;

        //[DataMember]
        //public string raca;

        private string senha;

        public string Senha
        {
            get { return senha; }
            set { senha = value; }
        }

        [DataMember]
        public string TipoGraduacao { get; set; }

        [DataMember]
        public string Turno { get; set; }

        [DataMember]
        public string Telefone { get; set; }

        //DataMember incluído em 29/12/2015 para que o tipo "Aluno"
        //possa representar estudantes de graduação e de Pós-Graduação.
        [DataMember]
        public TipoEstudante tipoEstudante = TipoEstudante.Graduacao;

        [DataMember]
        public string MaeAluno { get; set; }

        [DataMember]
        public string PaiAluno  { get; set; }

        public Aluno() { }

        //Métodos Construtores que recebem as entidades do banco:
        public Aluno(GetDadosAluno aluno)
        {
            if (aluno != null)
            {
                this.CampusUID = aluno.campusUID;
                this.Cpf = aluno.cpf;
                this.Curso = aluno.curso;
                this.DataNascimento = DateTime.Parse(aluno.dataNascimento);
                this.Email = aluno.email;
                this.NomeAluno = aluno.nomeAluno;
                this.NomeCampus = aluno.campus;
                this.Rga = aluno.rga.ToString();
                this.Sexo = aluno.sexo;
                this.CodigoCurso = aluno.codidoCurso;
                this.Agencia = aluno.agencia;
                this.NumeroConta = aluno.numeroConta;
                this.NomeBanco = aluno.NomeBanco;
                this.CodigoBanco = aluno.CodigoFebrabram;
                this.Rg = aluno.rg;
                this.Endereco = aluno.enderecoAluno;
                this.Cidade = aluno.cidadeAluno;
                this.UF = aluno.ufAluno;
                this.CEP = aluno.cepAluno;
                this.ano = aluno.ano;
                this.semestre = aluno.semestre;
                this.tipoEstudante = TipoEstudante.Graduacao;
                this.TipoGraduacao = aluno.tipoGraduacao;
                this.Turno = aluno.turno;
                this.Telefone = aluno.telefone1;

                this.MaeAluno = aluno.maeAluno;
                this.PaiAluno = aluno.paiAluno;
            }
        }

        public Aluno(vwAlunoSiga aluno)
        {
            if (aluno != null)
            {
                this.CampusUID = aluno.CodCampus;
                this.Cpf = aluno.CPF;
                this.Curso = aluno.Curso;
                this.DataNascimento = DateTime.Parse(aluno.DataNasc);
                this.Email = aluno.Email;
                this.NomeAluno = aluno.Nome;
                this.NomeCampus = aluno.Campus;
                this.Rga = aluno.Matricula.ToString();
                this.Sexo = aluno.Sexo;
                this.CodigoCurso = aluno.CodCurso;
                this.Agencia = aluno.Agencia;
                this.NumeroConta = aluno.ContaCorrente;
                this.NomeBanco = aluno.NomeBanco;
                this.CodigoBanco = aluno.CodigoFebrabram;
                this.Rg = "";
                this.Endereco = "";
                this.Cidade = "";
                this.UF = "";
                this.CEP = "";
                this.ano = "";
                this.semestre = "";
                this.tipoEstudante = TipoEstudante.Graduacao;
                this.TipoGraduacao = "";
                this.Turno = "";
                this.Telefone = aluno.telefone1;

                this.MaeAluno = aluno.nomeMae;
                this.PaiAluno = aluno.nomePai;
            }
        }

        public Aluno(GetDadosAlunoPos aluno)
        {
            if (aluno != null)
            {
                this.CampusUID = aluno.campusUID;
                this.Cpf = aluno.CPF;
                this.Curso = aluno.curso;
                this.DataNascimento = aluno.dataNascimento;
                this.Email = aluno.Email;
                this.NomeAluno = aluno.nomeAluno;
                this.NomeCampus = aluno.campus;
                this.Rga = aluno.rga.ToString();
                this.Sexo = aluno.Sexo;
                this.CodigoCurso = aluno.codidoCurso;
                this.Agencia = aluno.agencia;
                this.NumeroConta = aluno.numeroConta;
                this.NomeBanco = aluno.NomeBanco;
                this.CodigoBanco = aluno.CodigoFebrabram;
                this.Rg = aluno.RG;
                this.Endereco = aluno.enderecoAluno;
                this.Cidade = aluno.cidadeAluno;
                this.tipoEstudante = TipoEstudante.PosGraduacao;
                //this.raca = aluno.raca;

                this.MaeAluno = aluno.maeAluno;
                this.PaiAluno = aluno.paiAluno;

                //NÃO TEM DADOS EQUIVALENTES:
                this.UF = string.Empty;
                this.CEP = string.Empty;
                this.ano = string.Empty;
                this.semestre = string.Empty;
            }
        }

        public Aluno(vwAlunoPos aluno)
        {
            //throw new NotImplementedException();

            if (aluno != null)
            {
                //this.CampusUID = aluno.Campus;
                this.Cpf = aluno.CPF;
                //this.Curso = aluno.curso;
                //this.DataNascimento = aluno.dataNascimento;
                this.Email = aluno.Email;
                //this.NomeAluno = aluno.nomeAluno;
                //this.NomeCampus = aluno.campus;
                //this.Rga = aluno.rga.ToString();
                this.Sexo = aluno.Sexo;
                //this.CodigoCurso = aluno.codidoCurso;
                //this.Agencia = aluno.agencia;
                //this.NumeroConta = aluno.numeroConta;
                //this.NomeBanco = aluno.NomeBanco;
                //this.CodigoBanco = aluno.CodigoFebrabram;
                //this.Rg = aluno.RG;
                //this.Endereco = aluno.enderecoAluno;
                //this.Cidade = aluno.cidadeAluno;
                this.tipoEstudante = TipoEstudante.PosGraduacao;
                //this.raca = aluno.raca;

                //NÃO TEM DADOS EQUIVALENTES:
                this.UF = string.Empty;
                this.CEP = string.Empty;
                this.ano = string.Empty;
                this.semestre = string.Empty;
            }
        }

        public Aluno(ViewAlunosGraduacaoDPR aluno)
        {
            this.NomeAluno = aluno.aluno;
            this.Rga = aluno.rga.ToString();
            this.Cpf = aluno.cpf;
        }

        public Aluno(EntityModels.SIGAALUNO aluno, EntityModels.SIGACURSO curso, EntityModels.SIGACAMPUS campi)
        {
            if (aluno != null)
            {
                this.CampusUID = campi.SigaCodCampus;
                this.Cpf = aluno.SigaCPFAluno;
                this.Curso = aluno.SigaTxtComplNomeCurso;
                this.DataNascimento = DateTime.Parse(aluno.SigaDtNascAluno);
                this.Email = aluno.SigaEmailAluno;
                this.NomeAluno = aluno.SigaNomeAluno;
                this.NomeCampus = campi.SigaNomeCampus;
                this.Rga = aluno.SigaRgaAluno.ToString();
                this.Sexo = aluno.SigaSexoAluno;
                this.CursoUID = aluno.SigaCursoAluno;
                this.Curso = curso.SigaNomeCP1Curso;
                this.CodigoCurso = aluno.SigaCursoAluno;
                this.Agencia = aluno.SigaAgBancoAluno;
                this.NumeroConta = aluno.SigaNrContaAluno;
                this.NomeBanco = string.Empty;
                this.CodigoBanco = aluno.SigaCodBanAluno.ToString();
                this.Rg = aluno.SigaNroIdentAluno;
                this.Endereco = aluno.SigaEnderecoAluno;
                this.Cidade = aluno.SigaCidadeAluno;
                this.UF = aluno.SigaUFAluno;
                this.CEP = aluno.SigaCEPAluno;
                this.ano = string.Empty;
                
                this.semestre = string.Empty;
                this.tipoEstudante = TipoEstudante.Graduacao;
                this.TipoGraduacao = aluno.SigaTipo3GrauAluno;
                this.Turno = curso.SigaTurnoCurso.ToString();
                this.Telefone = string.Empty;

                this.MaeAluno = aluno.SigaMaeAluno;
                this.PaiAluno = aluno.SigaPaiAluno;
            }
        }
    }
}
