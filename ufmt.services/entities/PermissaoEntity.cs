﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ufmt.services.entities
{
    [DataContract]
    public class PermissaoEntity
    {
        public PermissaoEntity() { }

        public PermissaoEntity(ufmt.sig.entity.Permissao p) {
            this.Aplicacao = new AplicacaoEntity(p.Aplicacao);
            this.Descricao = p.Descricao;
            this.Nome = p.Nome;
            this.PermissaoUID = p.PermissaoUID;
            this.UrlRelativa = p.UrlRelativa;
        }

        public PermissaoEntity(ufmt.services.entities.Permissao1 p)
        {
            this.Aplicacao = new AplicacaoEntity(p.Aplicacao);
            this.Descricao = p.descricao;
            this.Nome = p.nome;
            this.PermissaoUID = p.permissaoUID;
            this.UrlRelativa = p.urlRelativa;
        }

        [DataMember]
        public AplicacaoEntity Aplicacao { get; set; }
        [DataMember]
        public string Descricao { get; set; }
        [DataMember]
        public string Nome { get; set; }
        [DataMember]
        public long PermissaoUID { get; set; }
        [DataMember]
        public string UrlRelativa { get; set; }
    }
}