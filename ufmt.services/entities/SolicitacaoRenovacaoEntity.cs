﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ufmt.services.entities
{
    [DataContract]
    public class SolicitacaoRenovacaoEntity
    {
        [DataMember]
        public int solicitacaoRenovacaoUID { get; private set; }

        [DataMember]
        public string rga { get; private set; }

        [DataMember]
        public string nomeEstudante { get; private set; }

        [DataMember]
        public string campusEstudante { get; private set; }

        [DataMember]
        public List<int> lista_auxilioUID { get; private set; }

        public SolicitacaoRenovacaoEntity(idbufmtEntities db, SolicitacaoRenovacao sr)
        {
            this.solicitacaoRenovacaoUID = sr.solicitacaoRenovacaoUID;

            this.rga = sr.rga;

            string nomeEstudante = string.Empty;
            string campusEstudante = string.Empty;

            try
            {
                decimal rgaDecimal = decimal.Parse(this.rga);

                var estudante = db.GetDadosAluno.FirstOrDefault(x => x.rga == rgaDecimal);

                if (estudante != null)
                {
                    nomeEstudante = estudante.nomeAluno;
                    campusEstudante = estudante.campus;
                }
            }
            catch
            {
                nomeEstudante = "[Não encontrado]";
                campusEstudante = "[Não encontrado]";
            }

            this.nomeEstudante = nomeEstudante;
            this.campusEstudante = campusEstudante;

            lista_auxilioUID = new List<int>();

            foreach (var rts in sr.RenovacaoTipoSolicitacao)
            {
                if (rts.TipoSolicitacao.auxilioUID.HasValue)
                    lista_auxilioUID.Add(rts.TipoSolicitacao.auxilioUID.Value);
            }
        }
    }
}