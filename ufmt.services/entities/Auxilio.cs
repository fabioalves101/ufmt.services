//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ufmt.services.entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class Auxilio
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Auxilio()
        {
            this.TipoSolicitacao = new HashSet<TipoSolicitacao>();
        }
    
        public int auxilioUID { get; set; }
        public string nome { get; set; }
        public decimal valor { get; set; }
        public bool ativo { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TipoSolicitacao> TipoSolicitacao { get; set; }
    }
}
