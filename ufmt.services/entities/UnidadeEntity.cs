﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ufmt.sig.entity;

namespace ufmt.services.entities
{
    public class UnidadeEntity
    {
        public UnidadeEntity() { }

        public UnidadeEntity(ufmt.sig.entity.Unidade unidade)
        {
            this.CampusUID = unidade.Campus.CampusUID;
            this.Campus = unidade.Campus.Nome;
            this.Hierarquia = unidade.Hierarquia;
            this.Nome = unidade.Nome;
            this.Sigla = unidade.Sigla;
            this.TipoUnidade = (int)unidade.TipoUnidade;
            try
            {
                this.UnidadeSuperiorUID = (long)unidade.UnidadeSuperior.UnidadeUID;
            }
            catch (Exception)
            {
                this.UnidadeSuperiorUID = 0;
            }
            this.UnidadeUID = unidade.UnidadeUID;
        }

        public UnidadeEntity(ufmt.services.entities.Unidade unidade)
        {
            this.CampusUID = unidade.Campus.campusUID;
            this.Hierarquia = unidade.hierarquia;
            this.Campus = unidade.Campus.nome;
            this.Nome = unidade.nome;
            this.Sigla = unidade.sigla;
            try
            {
                this.TipoUnidade = (int)unidade.tipoUnidade;
            }
            catch (Exception)
            {
                this.TipoUnidade = 0;
            }
            this.UnidadeUID = unidade.unidadeUID;
            this.Ativa = unidade.ativa.ToCharArray().First();
        }

        public long CampusUID { get; set; }
        public string Campus { get; set; }
        public string Hierarquia { get; set; }
        public string Nome { get; set; }
        public string Sigla { get; set; }

        public int TipoUnidade { get; set; }
        public long UnidadeSuperiorUID { get; set; }
        public long UnidadeUID { get; set; }
        public char Ativa { get; set; }
    }
}