﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ufmt.sig.entity;

namespace ufmt.services.entities
{
    public class ConversorModelParaDLL
    {
        public static sig.entity.Usuario Usuario(idbufmtEntities db, ufmt.services.entities.Usuario1 usuario)
        {
            sig.entity.Usuario resultado = null;

            if (usuario != null)
            {
                resultado = new sig.entity.Usuario()
                {
                    UsuarioUID = usuario.usuarioUID,
                    Bloqueado = usuario.bloqueado,
                    DataProximaTrocaSenha = usuario.dataProximaTrocaSenha,
                    DataUltimoAcesso = usuario.dataUltimoAcesso,
                    NomeAcesso = usuario.nomeAcesso,
                    SenhaAcesso = usuario.senhaAcesso,
                    Pessoa = Pessoa(db, db.Pessoa.Find(usuario.pessoaUID))
                };
            }

            return resultado;
        }

        public static sig.entity.Pessoa Pessoa(idbufmtEntities db, ufmt.services.entities.Pessoa pessoa)
        {
            sig.entity.Pessoa resultado = null;

            if (pessoa != null)
            {
                resultado = new sig.entity.Pessoa()
                {
                    PessoaUID = pessoa.pessoaUID,

                    //Dados Pessoais
                    Cpf = pessoa.cpf,
                    Nome = pessoa.nome,
                    NomeMae = pessoa.nomeMae,
                    NomePai = pessoa.nomePai,
                    Rg = pessoa.rg,
                    RgDataExpedicao = pessoa.rgDataExpedicao,
                    RgOrgaoExpedidor = pessoa.rgOrgaoExpedidor,
                    RgUf = pessoa.rgUF,
                    NascimentoData = pessoa.nascimentoData,
                    Sexo = pessoa.sexo[0],
                    PisPasep = pessoa.pisPasep,

                    //Contato
                    Mail = pessoa.mail,
                    TelefonePrimario = pessoa.telefonePrimario,
                    TelefoneSecundario = pessoa.telefoneSecundario,
                    TelefoneComercial = pessoa.telefoneComercial,

                    //Carteira de Trabalho
                    Ctps = pessoa.ctps,
                    CtpsUf = pessoa.ctpsUF,
                    CtpsSerie = pessoa.ctpsSerie,

                    //Endereço
                    EnderecoBairro = pessoa.enderecoBairro,
                    EnderecoCep = pessoa.enderecoCEP,
                    EnderecoComplemento = pessoa.enderecoComplemento,
                    EnderecoLogradouro = pessoa.enderecoLogradouro,
                    EnderecoMunicipio = pessoa.enderecoMunicipio,
                    EnderecoNumero = pessoa.enderecoNumero,
                    EnderecoUf = pessoa.enderecoUF,

                    //Título de Eleitor
                    TituloEleitor = pessoa.tituloEleitor,
                    TituloEleitorSecao = pessoa.tituloEleitorSecao,
                    TituloEleitorUf = pessoa.tituloEleitorUF,
                    TituloEleitorZona = pessoa.tituloEleitorZona,
                    TituloEleitorDataEmissao
                    = null
                    //= DateTime.Parse(pessoa.tituloEleitorDataEmissao)
                };
            }

            return resultado;
        }
    }
}