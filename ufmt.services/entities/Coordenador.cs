﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace ufmt.services.entities
{
    [DataContract]
    public class Coordenador
    {
        public Coordenador()
        {

        }

        public Coordenador(vwCoordenadores result)
        {
            CampusUID = result.campusUID.Value;
            CodigoCurso = result.codigoCurso;
            CoordenadorUID = result.coordenadorUID;
            cpf = result.cpf;
            Email = result.email;
            Nome = result.nomeCoordenador;
            Usuario = result.usuario;
        }

        [DataMember]
        public int CoordenadorUID;
        [DataMember]
        public string Nome;
        [DataMember]
        public string Usuario;
        [DataMember]
        public int CodigoCurso;
        [DataMember]
        public int CampusUID;
        [DataMember]
        public string Email;
        [DataMember]
        public long? Siape;
        [DataMember]
        public string cpf;

        private string senha;

        public string Senha
        {
            get { return senha; }
            set { senha = value; }
        }

    }
}