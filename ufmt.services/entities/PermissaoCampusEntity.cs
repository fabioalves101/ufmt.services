﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ufmt.services.entities
{
    [DataContract]
    public class PermissaoCampusEntity
    {
        [DataMember]
        public long permissaoCampusUID { get; set; }

        [DataMember]
        public int campusUID { get; set; }

        [DataMember]
        public long permissaoUID { get; set; }

        [DataMember]
        public string nomeCampus { get; set; }

        public PermissaoCampusEntity(PermissaoCampus pc)
        {
            permissaoCampusUID = pc.permissaoCampusUID;
            campusUID = pc.campusUID;
            permissaoUID = pc.permissaoUID;
            nomeCampus = pc.Campus.nome;
        }
    }
}