﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ufmt.services.entities
{
    public class DisciplinaEntity
    {
        public DisciplinaEntity() { }

        public DisciplinaEntity(ufmt.services.entities.MatrizDisciplinar matriz)
        {
            this.MatrizDisciplinaUID = matriz.matrizDisciplinarUID;
            this.Nome = matriz.nome;
            this.PeriodoInicialFuncionamento = matriz.periodoInicialFuncionamento;
            this.PeriodoFinalFuncionamento = matriz.periodoFinalFuncionamento;
            this.MatrizCurricularUID = matriz.matrizCurricularUID;
            this.Ementa = matriz.ementa;

            this.TipoDisciplina = matriz.tipoDisciplinaUID.ToString();
            this.CargaHorariaTotal = float.Parse((matriz.cargaHorariaTeorica + matriz.cargaHorariaPratica + matriz.cargaHorariaPraticaComponente).ToString());

            this.CargaHorariaTeorica = matriz.cargaHorariaTeorica;
            this.CargaHorariaPraticaComponente = matriz.cargaHorariaPraticaComponente;
            this.CargaHorariaPratica = matriz.cargaHorariaPratica;
            this.CodigoExterno = matriz.codigoExterno;
        }
        
        public DisciplinaEntity(ufmt.sig.entity.MatrizDisciplinar matriz)
        {
            this.MatrizDisciplinaUID = matriz.MatrizDisciplinarUID;
            this.Nome = matriz.Nome;
            this.PeriodoInicialFuncionamento = matriz.PeriodoInicialFuncionamento;
            this.PeriodoFinalFuncionamento = matriz.PeriodoFinalFuncionamento;
            this.MatrizCurricularUID = matriz.MatrizCurricular.MatrizCurricularUID;
            this.Ementa = matriz.Ementa;

            this.TipoDisciplina = matriz.TipoDisciplina.Descricao;
            this.CargaHorariaTotal = matriz.CargaHorariaTotal;

            this.CargaHorariaTeorica = decimal.Parse(matriz.CargaHorariaTeorica.ToString());
            this.CargaHorariaPraticaComponente = decimal.Parse(matriz.CargaHorariaPraticaComponente.ToString());
            this.CargaHorariaPratica = decimal.Parse(matriz.CargaHorariaPratica.ToString());
            this.CodigoExterno = matriz.CodigoExterno;
        }

        public String Nome;
        public String Ementa;
        public long UnidadeOfertanteUID;
        public int? PeriodoInicialFuncionamento;
        public int? PeriodoFinalFuncionamento;
        public long MatrizDisciplinaUID;
        public long MatrizCurricularUID;
        public decimal CargaHorariaTeorica;
        public decimal CargaHorariaPraticaComponente;
        public decimal CargaHorariaPratica;
        public long? CodigoExterno;
        
        public String TipoDisciplina;
        public String TipoOfertaDisciplinar;
        public float CargaHorariaTotal;
        public List<PessoaEntity> professorResponsavel;
    }
}
