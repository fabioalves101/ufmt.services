﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ufmt.services.entities
{
    [DataContract]
    public class DisciplinaAlunoEntity
    {
        public DisciplinaAlunoEntity() { }

        [DataMember]
        public DisciplinaEntity Disciplina { get; set; }
        [DataMember]
        public string Turma { get; set; }
        [DataMember]
        public int Periodo { get; set; }
        [DataMember]
        public int CodigoCursoExterno { get; set; }
        [DataMember]
        public int CodigoCursoAluno { get; set; }
        [DataMember]
        public int CodigoCursoDisciplina { get; set; }
    }
}