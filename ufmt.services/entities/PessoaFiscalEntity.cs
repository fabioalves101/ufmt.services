﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ufmt.services.entities
{
    [DataContract]
    public class PessoaFiscalEntity
    {
        public PessoaFiscalEntity() { }

        public PessoaFiscalEntity(Consulta_Inscricao_Fiscal_SARI_Result consulta)
        {
            this.Campus = consulta.CAMPUS;
            this.Rga = consulta.MATRICULARGA;
            this.Nome = consulta.NOME;
            this.DataNascimento = consulta.SigaDtNascAluno;
            this.CPF = consulta.CPF;
            this.Email = consulta.EMAIL;
            this.TipoVinculo = consulta.TIPOVINCULO;
            this.Lotacao = consulta.LOTACAO;
            this.Situacao = consulta.SITUACAO;
            this.Cidade = consulta.SigaCidadeAluno;
            this.Logradouro = consulta.Logradouro;
            this.UF = consulta.UF;
            this.Bairro = consulta.Bairro;
            this.CEP = consulta.SigaCEPAluno;
        }

        [DataMember]
        public string Campus { get; set; }
        [DataMember]
        public decimal Rga { get; set; }
        [DataMember]
        public string Nome { get; set; }
        [DataMember]
        public string DataNascimento { get; set; }
        [DataMember]
        public string CPF { get; set; }
        [DataMember]
        public string Email { get; set; }
        [DataMember]
        public string TipoVinculo { get; set; }
        [DataMember]
        public string Lotacao { get; set; }
        [DataMember]
        public string Situacao { get; set; }
        [DataMember]
        public string Cidade { get; set; }
        [DataMember]
        public string Bairro { get; set; }
        [DataMember]
        public string Logradouro { get; set; }
        [DataMember]
        public string UF { get; set; }
        [DataMember]
        public string CEP { get; set; }
    }
}