﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;
using ufmt.services.EntityModels;

namespace ufmt.services.entities
{
    [DataContract]
    public class ProcessoEntity
    {
        [DataMember]
        public int resposta { get; set; }

        [DataMember]
        public string numero { get; set; }

        [DataMember]
        public DateTime dataAp { get; set; }

        [DataMember]
        public string hora { get; set; }
        
        //Parâmetros definidos a partir da tabela "tbProcessos"
        [DataMember]
        public int processoID { get; set; }

        //[DataMember]
        //public Nullable<short> TIPO { get; set; }

        [DataMember]
        public string requerente { get; set; }

        [DataMember]
        public string assunto { get; set; }

        [DataMember]
        public Nullable<int> numeroDePaginas { get; set; }

        [DataMember]
        public Nullable<System.DateTime> dataCriacao { get; set; }

        [DataMember]
        public string arquivo { get; set; }

        [DataMember]
        public string armario { get; set; }

        [DataMember]
        public string divisao { get; set; }

        [DataMember]
        public string caixa { get; set; }

        [DataMember]
        public string usuario { get; set; }

        public Nullable<double> UNIDADE { get; set; }
        public Nullable<int> removido { get; set; }
        public Nullable<int> unidadeCriacaoID { get; set; }
        public Nullable<int> usuarioCriaID { get; set; }
        public Nullable<long> ultimoTramiteID { get; set; }

        //Parâmetros com tipos diferentes da tabela "tbProcessos", que representam os dados de forma mais legível
        [DataMember]
        public TipoProcesso Tipo = TipoProcesso.TramiteLocal; //public Nullable<short> TIPO { get; set; }

        [DataMember]
        public List<TramiteProcessoEntity> tramites = new List<TramiteProcessoEntity>();

        public ProcessoEntity(tbProcessos processo)
        {
            //throw new NotImplementedException();

            if (processo != null)
            {
                preencheModel(processo);   
            }
        }

        private void preencheModel(tbProcessos processo)
        {
            this.processoID = processo.processoID;
            this.numero = processo.NUMERO;
            this.requerente = processo.REQUERENTE;
            this.assunto = processo.ASSUNTO;
            this.numeroDePaginas = processo.nroPaginas;
            this.dataCriacao = processo.dataCriacao;
            this.arquivo = processo.ARQUIVO;
            this.armario = processo.ARMARIO;
            this.divisao = processo.DIVISAO;
            this.caixa = processo.CAIXA;
            this.usuario = processo.USUARIO;

            if (processo.TIPO.HasValue)
                this.Tipo = (TipoProcesso)processo.TIPO.Value;
        }

        public ProcessoEntity(tbProcessos processo, List<TramiteProcessoEntity> tramites)
        {
            if (processo != null)
            {
                preencheModel(processo);
                this.tramites.AddRange(tramites);
            }
        }
    }
}