﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;

namespace ufmt.services.entities
{
    [DataContract]
    public class CursoSite
    {
        public CursoSite() { }

        public CursoSite(ViewCursosSite curso)
        {
            descr01_habilitacao = curso.descr01_habilitacao;
            descr02_habilitacao = curso.descr02_habilitacao;
            descr03_habilitacao = curso.descr03_habilitacao;
            sigaanomaxgradcurso = curso.sigaanomaxgradcurso;
            sigaanomingradcurso = curso.sigaanomingradcurso;
            sigaautorizacaocurso = curso.sigaautorizacaocurso;
            sigacampuscurso = Convert.ToInt32(curso.sigacampuscurso);
            sigacodcursosiga = curso.sigacodcursosiga;
            sigadescrgrau = curso.sigadescrgrau;
            sigadescrregime = curso.sigadescrregime;
            sigadescrturno = curso.sigadescrturno;
            sigafonecurso = curso.sigafonecurso;
            sigahistoricocurso = curso.sigahistoricocurso;
            sigamec2reconhecimento = curso.sigamec2reconhecimento;
            sigamecreconhecimento = curso.sigamecreconhecimento;
            siganomecp1curso = curso.siganomecp1curso;
            siganomecursovest = curso.siganomecursovest;
            siganomepinstituto = curso.siganomepinstituto;
            siganomesenha = curso.siganomesenha;
            sigaobs2reconhecimento = curso.sigaobs2reconhecimento;
            sigaobsreconhecimento = curso.sigaobsreconhecimento;
            sigaperfilcurso = curso.sigaperfilcurso;
            sigatipovest = curso.sigatipovest;
            totalvagas = curso.totalvagas;
        }

        [DataMember]
        public string sigacodcursosiga;
        [DataMember]
        public string sigatipovest;
        [DataMember]
        public string siganomecursovest;
        [DataMember]
        public int? totalvagas;
        [DataMember]
        public string siganomecp1curso;
        [DataMember]
        public string siganomepinstituto;
        [DataMember]
        public string siganomesenha;
        [DataMember]
        public string sigafonecurso;
        [DataMember]
        public string sigadescrgrau;
        [DataMember]
        public string descr01_habilitacao;
        [DataMember]
        public string descr02_habilitacao;
        [DataMember]
        public string descr03_habilitacao;
        [DataMember]
        public string sigadescrregime;
        [DataMember]
        public string sigadescrturno;
        [DataMember]
        public double? sigaanomingradcurso;
        [DataMember]
        public double? sigaanomaxgradcurso;
        [DataMember]
        public string sigamecreconhecimento;
        [DataMember]
        public string sigamec2reconhecimento;
        [DataMember]
        public string sigaobsreconhecimento;
        [DataMember]
        public string sigaobs2reconhecimento;
        [DataMember]
        public string sigahistoricocurso;
        [DataMember]
        public string sigaperfilcurso;
        [DataMember]
        public string sigaautorizacaocurso;
        [DataMember]
        public int sigacampuscurso;
    }
}