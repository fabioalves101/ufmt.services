//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ufmt.services.entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class vwDisciplinaProfessor
    {
        public decimal CodigoDisciplina { get; set; }
        public string DescricaoDisciplina { get; set; }
        public string CPFProfessor { get; set; }
        public int Periodo { get; set; }
        public string turmaHorario { get; set; }
        public int cursohorario { get; set; }
        public string chave { get; set; }
        public int estrutura { get; set; }
        public short CargaTeorica { get; set; }
        public short CargaPratica { get; set; }
        public string NomeInstituto { get; set; }
        public string NomeCampus { get; set; }
        public string TipoDisciplina { get; set; }
        public int Siape { get; set; }
        public string Professor { get; set; }
    }
}
