﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace ufmt.services.entities
{
    [DataContract]
    public class HorarioEntity
    {
        public HorarioEntity(ProcedureHistoricoAluno_Result historico) {
            this.Periodo = historico.SigaPeriodoHistorico;
            this.CodigoDisciplina = historico.SigaCodDiscHistorico;
            this.NomeDisciplina = historico.SigaDescrTDisciplina;
            this.Turma = historico.SigaTurmaHistorico;
            this.DiaSemana = historico.SigaNoABrTSemana;
            this.QtdeCreditos = historico.SigaCreditoHistorico;
            this.HorarioInicio = historico.SigaHorIniDiaHora;
            this.HorarioFim = historico.SigaHorFimDiaHora;
            this.TipoAula = historico.SigaTipoAulaDiaHora;
            this.CargaHoraria = historico.SigaCrgHorHistorico;
            this.Sala = historico.SigaSalaDiaHora;
            this.Semestre = historico.SigadiscSemHorario;
        }
        
        [DataMember]
        public int Periodo { get; set; }
        [DataMember]
        public decimal CodigoDisciplina { get; set; }
        [DataMember]
        public string NomeDisciplina { get; set; }
        [DataMember]
        public string Turma { get; set; }
        [DataMember]
        public string DiaSemana { get; set; }
        [DataMember]
        public short QtdeCreditos { get; set; }
        [DataMember]
        public double HorarioInicio { get; set; }
        [DataMember]
        public double HorarioFim { get; set; }
        [DataMember]
        public string TipoAula { get; set; }
        [DataMember]
        public short CargaHoraria { get; set; }
        [DataMember]
        public string Sala { get; set; }
        [DataMember]
        public byte Semestre { get; set; }
    }
}