﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Net.Mail;
using System.Net;
using System.ServiceModel.Activation;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

namespace ufmt.services
{
    /// <summary>
    /// Serviço de envio Email
    /// </summary>
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class Email : IEmail
    {
        //<add key="SMTP_DISPLAY_NAME" value="..."/>
        private string SMTP_display_name { get { return System.Configuration.ConfigurationManager.AppSettings["SMTP_DISPLAY_NAME"].ToString(); } }

        //<add key="SMTP_ADRESS_NAME" value="..."/>
        private string SMTP_adress_name { get { return System.Configuration.ConfigurationManager.AppSettings["SMTP_ADRESS_NAME"].ToString(); } }

        //<add key="SMTP_HOST" value="..."/>
        private string SMTP_host { get { return System.Configuration.ConfigurationManager.AppSettings["SMTP_HOST"].ToString(); } }

        //<add key="SMTP_PORT" value="..."/>
        private int SMTP_port { get { return Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SMTP_PORT"]); } }

        //<add key="SMTP_USERNAME" value="..."/>
        private string SMTP_username { get { return System.Configuration.ConfigurationManager.AppSettings["SMTP_USERNAME"].ToString(); } }

        //<add key="SMTP_PASSWORD" value="..."/>
        private string SMTP_password { get { return System.Configuration.ConfigurationManager.AppSettings["SMTP_PASSWORD"].ToString(); } }

        public void EnviarSmtpPadrao(string enderecoEmail, string mensagem)
        {
            this.enviarEmail(enderecoEmail, mensagem);
        }
        /// <summary>
        /// Envia Smtp Padrão versão 2 por Email 
        /// </summary>
        /// <param name="enderecoEmail">Endereço de Email a ser enviado </param>
        /// <param name="mensagem">mensagem a ser enviada </param>
        /// <param name="subject">Sistema de Solicitações de Alunos da UFMT a ser enviado</param>
        public void EnviarSmtpPadraoVersaoDois(string enderecoEmail, string mensagem, string subject)
        {
            string destino = enderecoEmail;

            MailMessage message = new System.Net.Mail.MailMessage();
            message.To.Add(destino);
            message.Subject = subject;
            message.IsBodyHtml = true;
            message.From = new MailAddress(SMTP_adress_name, SMTP_display_name);
            message.Body = mensagem + "<br><br> ---- <br />NÃO RESPONDA A ESSE E-MAIL.";

            try
            {
                SmtpClient smtp = new SmtpClient(SMTP_host, SMTP_port);
                smtp.EnableSsl = true;
                NetworkCredential cred = new NetworkCredential(SMTP_username, SMTP_password);
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = cred;

                ServicePointManager.ServerCertificateValidationCallback =
                    delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
                    { return true; };

                smtp.Send(message);
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        /// <summary>
        /// Sistema de solicitaçoes de alunos da UFMT
        /// </summary>
        /// <param name="enderecoEmail">Endereço de Email a ser enviado</param>
        /// <param name="msg">Mensagem a ser recebido</param>
        private void enviarEmail(string enderecoEmail, string msg)
        {
            this.EnviarSmtpPadraoVersaoDois(enderecoEmail, msg, "Sistema de Solicitações de Alunos da UFMT");

            //string destino = enderecoEmail;
            //MailMessage message = new System.Net.Mail.MailMessage();
            //message.To.Add(destino);
            //message.Subject = "Sistema de Solicitações de Alunos da UFMT";
            //message.IsBodyHtml = true;
            //message.From = new MailAddress("gpsoft@ufmt.br", "SIGA/UFMT");
            //message.Body = msg + "<br><br> ---- <br />NÃO RESPONDA A ESSE E-MAIL.";
            //try
            //{
            //    SmtpClient smtp = new SmtpClient("200.17.60.1", 25);
            //    smtp.EnableSsl = true;
            //    NetworkCredential cred = new NetworkCredential("gpsoft@ufmt.br", "wk165vx");
            //    smtp.UseDefaultCredentials = false;
            //    smtp.Credentials = cred;
            //    //SmtpClient smtp = new SmtpClient("smtp.gmail.com", 587);
            //    //smtp.EnableSsl = true;
            //    //NetworkCredential cred = new NetworkCredential("ufmt.sti.cesgea@gmail.com", "UFMTanalise");
            //    //smtp.UseDefaultCredentials = false;
            //    //smtp.Credentials = cred;
            //    ServicePointManager.ServerCertificateValidationCallback =
            //        delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
            //        { return true; };

            //    smtp.Send(message);
            //}
            //catch (Exception) { }
        }
    }
}
