﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using ufmt.sig.business;
using ufmt.sig.entity;
using System.ServiceModel.Activation;
using ufmt.services.entities;
using System.Linq.Expressions;
using System.Linq.Dynamic;

namespace ufmt.services
{
    /// <summary>
    ///  Métodos de consulta às informações de pessoas 
    /// </summary>
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Pessoa" in code, svc and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class Pessoa : IPessoa
    {
        /// <summary>
        /// Pesquisa de Pessoa por UID 
        /// </summary>
        /// <param name="pessoaUID"> UID da pessoa a ser pesquisada </param>
        /// <returns> Retorna dados da pessoa com o UID informado  </returns>
        public PessoaEntity GetPessoa(long pessoaUID)
        {
            try
            {
                using (var db = new idbufmtEntities())
                {
                    var pessoa = db.Pessoa.Where(x => x.pessoaUID == pessoaUID).First();
                    return new PessoaEntity(pessoa);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        ///  Pesquisa de pessoa por usuario 
        /// </summary>
        /// <param name="usuarioUID">UID de usuario da pessoa a ser pesquisada </param>
        /// <returns> Retorna informações da pessoa com o UID de usuario informado </returns>
        public sig.entity.Pessoa GetPessoaPorUsuario(long usuarioUID)
        {
            try
            {
                ufmt.sig.entity.Usuario usuario = null;
                ufmt.sig.entity.Pessoa pessoa = null;

                using (IAutenticacaoBO autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
                {
                    usuario = autenticacaoBO.GetUsuario(usuarioUID);
                }

                if (usuario != null)
                {
                    using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                    {
                        pessoa = pessoaBO.GetPessoa(usuario.Pessoa.PessoaUID);
                    }
                }

                if (pessoa != null)
                    return pessoa;
                else
                    throw new Exception("Pessoa não encontrada com o ID de usuário fornecido");
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Pesquisa pessoa entity por usuario 
        /// </summary>
        /// <param name="usuarioUID"> UID de  usuario da pessoa a ser pesquisada </param>
        /// <returns>Retorna uma instancia de pessoa entity com o UID de usuario informado </returns>
        public ufmt.services.entities.PessoaEntity GetPessoaEntityPorUsuario(long usuarioUID)
        {
            try
            {
                using (var db = new idbufmtEntities())
                {
                    var usuario = db.Usuario1Set.Find(usuarioUID);

                    if (usuario == null)
                    {
                        throw new FaultException("Usuário não Encontrado!");
                    }

                    if (usuario.Pessoa == null)
                    {
                        throw new FaultException("Usuário não possui pessoa!");
                    }

                    return new PessoaEntity(usuario.Pessoa);
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Pesquisa vinculos de um determinado docente 
        /// </summary>
        /// <param name="pessoaUID"> UID da pessoa a ser pesquisado </param>
        /// <param name="apenasAtivos">Situação de atividade do servidor a ser pesquisado </param>
        /// <returns>Retorna Lista de vinculos do docente informado  </returns>
        public IList<PessoaEntity> GetVinculoDocente(long pessoaUID, bool apenasAtivos)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    List<PessoaEntity> obj = new List<PessoaEntity>();
                    var listServidor = pessoaBO.GetVinculosDocenciaPorPessoa(pessoaUID);

                    int situacaoServidorAtivoUID =
                        int.Parse(System.Configuration.ConfigurationManager
                            .AppSettings["SituacaoServidorAtivo"].ToString());

                    if (apenasAtivos)
                        listServidor = listServidor
                            .Where(s => s.SituacaoServidor.SituacaoServidorUID == situacaoServidorAtivoUID)
                            .ToList();

                    foreach (var servidor in listServidor)
                    {
                        PessoaEntity p = new PessoaEntity(servidor.Pessoa);
                        obj.Add(p);
                    }

                    return obj;
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Pesquisade de servidor por pessoa simples 
        /// </summary>
        /// <param name="siape">siape da pessoa a ser pesquisada</param>
        /// <returns>Retorna informações do servidor pesquisado</returns>
        public entities.ServidorPessoaSimples GetServidorPessoaSimples(string siape)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    sig.entity.Servidor servidor = pessoaBO.GetServidor(siape, 'S');

                    entities.ServidorPessoaSimples obj = new entities.ServidorPessoaSimples()
                    {
                        Nome = servidor.Pessoa.Nome,
                        Siape = servidor.Registro,
                        Lotacao = (servidor.UnidadeLotacao != null ? servidor.UnidadeLotacao.Nome : "Sem Lotação Cadastrada"),
                        Cpf = servidor.Pessoa.Cpf,
                        Cargo = servidor.Cargo.Nome
                    };

                    return obj;
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Pesquisa de pessoas por cpf
        /// </summary>
        /// <param name="cpf"> cpf da pessoa a ser pesquisada </param>
        /// <returns> Retorna uma instancia de pessoa com o cpf informado </returns>
        public ufmt.sig.entity.Pessoa GetPessoaPorCPF(string cpf)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    return pessoaBO.GetPessoa(cpf);
                }
            }
            catch
            {
                throw;
            }
        }

        /// <summary>
        /// Atualiza os dados pessoais de uma determinada pessoa
        /// </summary>
        /// <param name="pessoaUID"></param>
        /// <param name="TelefonePrimario"></param>
        /// <param name="TelefoneSecundario"></param>
        /// <param name="Mail"></param>
        /// <param name="linkLattes"></param>
        /// <param name="image"></param>
        /// <param name="content_type"></param>
        /// <returns>True -> Sucesso na atualização // False -> Falha na atualização</returns>
        public bool AtualizarDadosCadastrais(long pessoaUID, string TelefonePrimario, string TelefoneSecundario, string Mail, string linkLattes, byte[] image, string content_type)
        {
            try
            {
                using (var db = new idbufmtEntities())
                {
                    var pessoinha = db.Pessoa.Where(p => p.pessoaUID == pessoaUID).FirstOrDefault();

                    if (pessoinha != null)
                    {
                        pessoinha.telefonePrimario = TelefonePrimario.Replace("(","").Replace(")","").Replace(" ", "").Replace("-","");
                        pessoinha.telefoneSecundario = TelefoneSecundario.Replace("(", "").Replace(")", "").Replace(" ", "").Replace("-", "");
                        pessoinha.mail = Mail;

                        var perfil = db.PerfilPessoa.Where(x => x.pessoaUID == pessoaUID).FirstOrDefault();


                        if (perfil != null)
                        {
                            perfil.linkLattes = linkLattes;

                            if (image.Length > 0)
                            {
                                perfil.imagem = image;
                                perfil.content_type = content_type;
                            }
                        }
                        else
                        {
                            perfil = new ufmt.services.entities.PerfilPessoa();
                            perfil.linkLattes = linkLattes;
                            if (image.Length > 0)
                            {
                                perfil.imagem = image;
                                perfil.content_type = content_type;
                            }
                            perfil.pessoaUID = pessoaUID;
                            db.PerfilPessoa.Add(perfil);
                        }



                        db.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        /// <summary>
        /// Pesquisa de pessoa por nome 
        /// </summary>
        /// <param name="nome">Nome da pessoa a ser pesquisada </param>
        /// <returns>Retorna uma lista de pessoas com o nome informado </returns>
        public List<sig.entity.Pessoa> GetPessoas(string nome)
        {
            using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
            {
                return pessoaBO.GetPessoas(nome).ToList();
            }
        }

        /// <summary>
        /// Pesquisa de professores por nome 
        /// </summary>
        /// <param name="nome"> Nome do professor a ser pesquisado </param>
        /// <returns>Retorna uma lista de professores com o nome informado </returns>
        public List<sig.entity.Pessoa> GetProfessores(string nome)
        {
            using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
            {
                IList<ufmt.sig.entity.Servidor> servidores = pessoaBO.GetServidores(nome, null, true, null, null);

                return (from s in servidores select s.Pessoa).ToList();
            }
        }

        /// <summary>
        /// Pesquisa de pessoas por unidade 
        /// </summary>
        /// <param name="unidadeUID"> Unidade da pessoa a ser pesquisada </param>
        /// <returns> Retorna uma lista de pessoas na unidade informada </returns>
        public List<PessoaEntity> GetPessoasPorUnidade(long unidadeUID)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    List<sig.entity.Servidor> servidores =
                        pessoaBO.GetServidoresPorUnidadeLotacional(unidadeUID, null, null, false, null, null)
                        .ToList();

                    List<ufmt.sig.entity.Pessoa> pessoas = servidores.Select(p => p.Pessoa).ToList();

                    List<PessoaEntity> pessoasEntity = new List<PessoaEntity>();

                    foreach (ufmt.sig.entity.Pessoa pessoa in pessoas)
                    {
                        PessoaEntity pessoaEntity = new PessoaEntity()
                        {
                            Cpf = pessoa.Cpf,
                            Ctps = pessoa.Ctps,
                            CtpsSerie = pessoa.CtpsSerie,
                            CtpsUf = pessoa.CtpsUf,
                            EnderecoBairro = pessoa.EnderecoBairro,
                            EnderecoCep = pessoa.EnderecoCep,
                            EnderecoComplemento = pessoa.EnderecoComplemento,
                            EnderecoLogradouro = pessoa.EnderecoLogradouro,
                            EnderecoMunicipio = pessoa.EnderecoMunicipio,
                            EnderecoNumero = pessoa.EnderecoNumero,
                            EnderecoUf = pessoa.EnderecoUf,
                            Mail = pessoa.Mail,
                            NascimentoData = pessoa.NascimentoData,
                            Nome = pessoa.Nome,
                            NomeMae = pessoa.NomeMae,
                            NomePai = pessoa.NomePai,
                            PessoaUID = pessoa.PessoaUID,
                            PisPasep = pessoa.PisPasep,
                            Rg = pessoa.Rg,
                            RgDataExpedicao = pessoa.RgDataExpedicao,
                            RgOrgaoExpedidor = pessoa.RgOrgaoExpedidor,
                            RgUf = pessoa.RgUf,
                            Sexo = pessoa.Sexo,
                            TelefoneComercial = pessoa.TelefoneComercial,
                            TelefonePrimario = pessoa.TelefonePrimario,
                            TelefoneSecundario = pessoa.TelefoneSecundario,
                            TipoTitulacao = pessoa.TipoTitulacao,
                            TituloEleitor = pessoa.TituloEleitor,
                            TituloEleitorDataEmissao = pessoa.TituloEleitorDataEmissao,
                            TituloEleitorSecao = pessoa.TituloEleitorSecao,
                            TituloEleitorUf = pessoa.TituloEleitorUf,
                            TituloEleitorZona = pessoa.TituloEleitorZona
                        };

                        pessoasEntity.Add(pessoaEntity);
                    }

                    return pessoasEntity;

                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Pesquisa de pessoa entity por UID 
        /// </summary>
        /// <param name="pessoaUID"> UID da pessoa a ser pesquisada </param>
        /// <returns>Retorna uma instancia de pessoa com o UID informado </returns>
        public PessoaEntity GetPessoaEntity(long pessoaUID)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    ufmt.sig.entity.Pessoa pessoa = pessoaBO.GetPessoa(pessoaUID);

                    return new PessoaEntity(pessoa);
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Pesquisa do servidor da pessoa 
        /// </summary>
        /// <param name="pessoa">Informaçoes da pessoa a ser pesquisado</param>
        /// <returns>retorna ao servidor pessoa</returns>
        public PessoaEntity CastPessoaToPessoaEntity(ufmt.sig.entity.Pessoa pessoa)
        {
            PessoaEntity pessoaEntity = new PessoaEntity()
            {
                Cpf = pessoa.Cpf,
                Ctps = pessoa.Ctps,
                CtpsSerie = pessoa.CtpsSerie,
                CtpsUf = pessoa.CtpsUf,
                EnderecoBairro = pessoa.EnderecoBairro,
                EnderecoCep = pessoa.EnderecoCep,
                EnderecoComplemento = pessoa.EnderecoComplemento,
                EnderecoLogradouro = pessoa.EnderecoLogradouro,
                EnderecoMunicipio = pessoa.EnderecoMunicipio,
                EnderecoNumero = pessoa.EnderecoNumero,
                EnderecoUf = pessoa.EnderecoUf,
                Mail = pessoa.Mail,
                NascimentoData = pessoa.NascimentoData,
                Nome = pessoa.Nome,
                NomeMae = pessoa.NomeMae,
                NomePai = pessoa.NomePai,
                PessoaUID = pessoa.PessoaUID,
                PisPasep = pessoa.PisPasep,
                Rg = pessoa.Rg,
                RgDataExpedicao = pessoa.RgDataExpedicao,
                RgOrgaoExpedidor = pessoa.RgOrgaoExpedidor,
                RgUf = pessoa.RgUf,
                Sexo = pessoa.Sexo,
                TelefoneComercial = pessoa.TelefoneComercial,
                TelefonePrimario = pessoa.TelefonePrimario,
                TelefoneSecundario = pessoa.TelefoneSecundario,
                TipoTitulacao = pessoa.TipoTitulacao,
                TituloEleitor = pessoa.TituloEleitor,
                TituloEleitorDataEmissao = pessoa.TituloEleitorDataEmissao,
                TituloEleitorSecao = pessoa.TituloEleitorSecao,
                TituloEleitorUf = pessoa.TituloEleitorUf,
                TituloEleitorZona = pessoa.TituloEleitorZona
            };

            using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
            {
                ufmt.sig.entity.Servidor servidor = pessoaBO.GetServidorAtivo(pessoa.PessoaUID.ToString());
                if (servidor != null)
                {
                    pessoaEntity.ServidorUID = servidor.ServidorUID;
                    pessoaEntity.Siape = servidor.Registro;
                    if (servidor.UnidadeLotacao != null)
                        pessoaEntity.Unidade = new Localizacao().CastUnidadeToUnidadeEntity(servidor.UnidadeLotacao);
                }
            }

            return pessoaEntity;
        }

        /// <summary>
        /// /Pesquisa de pessoa por servidor
        /// </summary>
        /// <param name="servidorUID">UID do servidor a ser pesquisado</param>
        /// <returns>retorna uma instancia de pessoa</returns>
        public PessoaEntity GetPessoaPorServidor(long servidorUID)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    sig.entity.Servidor servidor = pessoaBO.GetServidor(servidorUID);


                    PessoaEntity pessoa = this.ConvertServidorToPessoaEntity(servidor);

                    if (servidor.UnidadeLotacao != null)
                        pessoa.Unidade = new Localizacao().GetUnidade(servidor.UnidadeLotacao.UnidadeUID);

                    return pessoa;
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nome"> Nome a ser pesquisado</param>
        /// <param name="registroInicial">Registro inicial a ser pesquisado</param>
        /// <param name="quantidadeRegistros">Quantidade de registro a ser informado</param>
        /// <returns>Retorna lista de dados da pessoa</returns>
        public List<PessoaEntity> GetPessoasEntity(string nome, int registroInicial, int quantidadeRegistros)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    IEnumerable<ufmt.sig.entity.Pessoa> pessoasList = pessoaBO.GetPessoas(nome).
                        OrderBy(p => p.Nome)
                        .Skip(registroInicial)
                        .Take(quantidadeRegistros);

                    List<PessoaEntity> pessoaEntityList = new List<PessoaEntity>();
                    foreach (var pessoa in pessoasList)
                    {
                        pessoaEntityList.Add(CastPessoaToPessoaEntity(pessoa));
                    }

                    return pessoaEntityList;
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Converte servidor para Pessoa entity 
        /// </summary>
        /// <param name="servidor"> Servidor a ser convertido </param>
        /// <returns>Retorna servidor convertido para pessoa entity </returns>
        protected PessoaEntity ConvertServidorToPessoaEntity(ufmt.sig.entity.Servidor servidor)
        {
            ufmt.sig.entity.Pessoa pessoa = servidor.Pessoa;
            PessoaEntity pessoaEntity = new PessoaEntity()
            {
                Cpf = pessoa.Cpf,
                Ctps = pessoa.Ctps,
                CtpsSerie = pessoa.CtpsSerie,
                CtpsUf = pessoa.CtpsUf,
                EnderecoBairro = pessoa.EnderecoBairro,
                EnderecoCep = pessoa.EnderecoCep,
                EnderecoComplemento = pessoa.EnderecoComplemento,
                EnderecoLogradouro = pessoa.EnderecoLogradouro,
                EnderecoMunicipio = pessoa.EnderecoMunicipio,
                EnderecoNumero = pessoa.EnderecoNumero,
                EnderecoUf = pessoa.EnderecoUf,
                Mail = pessoa.Mail,
                NascimentoData = pessoa.NascimentoData,
                Nome = pessoa.Nome,
                NomeMae = pessoa.NomeMae,
                NomePai = pessoa.NomePai,
                PessoaUID = pessoa.PessoaUID,
                PisPasep = pessoa.PisPasep,
                Rg = pessoa.Rg,
                RgDataExpedicao = pessoa.RgDataExpedicao,
                RgOrgaoExpedidor = pessoa.RgOrgaoExpedidor,
                RgUf = pessoa.RgUf,
                Sexo = pessoa.Sexo,
                TelefoneComercial = pessoa.TelefoneComercial,
                TelefonePrimario = pessoa.TelefonePrimario,
                TelefoneSecundario = pessoa.TelefoneSecundario,
                TipoTitulacao = pessoa.TipoTitulacao,
                TituloEleitor = pessoa.TituloEleitor,
                TituloEleitorDataEmissao = pessoa.TituloEleitorDataEmissao,
                TituloEleitorSecao = pessoa.TituloEleitorSecao,
                TituloEleitorUf = pessoa.TituloEleitorUf,
                TituloEleitorZona = pessoa.TituloEleitorZona,
                ServidorUID = servidor.ServidorUID,
                Siape = servidor.Registro
            };

            return pessoaEntity;
        }

        /// <summary>
        /// Pesquisa de coordenador do Curso Informado 
        /// </summary>
        /// <param name="codigoCurso"> Codigo do curso a ser pesquisado </param>
        /// <returns> Retorna dados do cordenador do curso informado </returns>
        public vwCoordenadores GetCoordenadorSigaPorCodigoCurso(int codigoCurso)
        {
            try
            {
                using (idbufmtEntities db = new idbufmtEntities())
                {
                    vwCoordenadores coordenador = db.vwCoordenadores.Where(c => c.codigoCurso == codigoCurso).FirstOrDefault();

                    return coordenador;
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Pesquisa de coordenador por id 
        /// </summary>
        /// <param name="codigoCoordenador"> Codigo do coordenador a ser pesquisado </param>
        /// <returns> Retorna dados do cordenador </returns>
        public List<vwCoordenadores> GetCoordenadorSigaPorCodigo(int codigoCoordenador)
        {
            try
            {
                using (idbufmtEntities db = new idbufmtEntities())
                {
                    List<vwCoordenadores> coordenador = db.vwCoordenadores.Where(c => c.coordenadorUID == codigoCoordenador).ToList();

                    return coordenador;
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        /// <summary>
        /// Pesuqisa de coordenador por curso
        /// </summary>
        /// <param name="cursoUID">UID do curso a ser pesquisado</param>
        /// <returns>Retorna informações do coordenador </returns>
        public vwCoordenadores GetCoordenadorSigaPorCursoUID(long cursoUID)
        {
            try
            {
                ufmt.sig.entity.Curso curso;
                using (ICursoBO cursoBO = BusinessFactory.GetInstance<ICursoBO>())
                {
                    curso = cursoBO.GetCurso(cursoUID);
                }

                using (idbufmtEntities db = new idbufmtEntities())
                {
                    vwCoordenadores coordenador = db.vwCoordenadores.Where(c => c.codigoCurso == curso.CodigoExterno).FirstOrDefault();

                    return coordenador;
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Pesquisa pessoa por CPF e Situação   
        /// </summary>
        /// <param name="cpf">CPF da pessoa a ser pesquisada </param>
        /// <param name="situacoes">Situação do servidor a ser pesquisado</param>
        /// <returns>Retorna uma instancia de Pessoa, com as verificações de AlunoGraduação, Servidor e AlunoPos</returns>
        public PessoaEntity GetPessoaPorCPFeSituacao(string cpf, SituacaoDoServidor[] situacoes)
        {
            try
            {
                if (situacoes == null || !situacoes.Any())
                {
                    situacoes = new SituacaoDoServidor[] { SituacaoDoServidor.Ativo };
                }

                var situacoesUID = SituacoesUID(situacoes);

                using (var db = new idbufmtEntities())
                {
                    var p = db.Pessoa.FirstOrDefault(x => x.cpf == cpf
                                                    && x.Servidor.Any(y => situacoesUID.Contains(y.situacaoServidorUID)));

                    if (p != null)
                    {
                        PessoaEntity pessoa = new PessoaEntity(p);

                        // Verifico se a pessoa possui uma instancia de Servidor
                        if (p.Servidor.Count > 0)
                        {
                            pessoa.TipoPessoa = Tipo.Servidor;
                        }


                        return pessoa;
                    }
                    else
                    {
                        //throw new ArgumentException("Nenhuma pessoa encontrada com os dados informados.");
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Pesquisa a situação de um determinado servidor por UID
        /// </summary>
        /// <param name="situacoes">Situaçoes do servidor a ser pesquisado </param>
        /// <returns>Retorna uma lista de servidores com a situações passadas por parametro </returns>
        private int[] SituacoesUID(SituacaoDoServidor[] situacoes)
        {
            //throw new NotImplementedException();

            var result = new List<int>();

            foreach (var situacao in situacoes)
            {
                result.Add((int)situacao);
            }

            return result.ToArray();
        }

        /// <summary>
        /// Pesquisa de pessoa por situação  
        /// </summary>
        /// <param name="pessoaUID">UID da pessoa a ser pesquisada </param>
        /// <param name="situacoes">Situaçoes do servidor a ser pesquisada</param>
        /// <returns>retorna uma instancia de pessoa </returns>
        public PessoaEntity GetPessoaEntityPorSituacao(long pessoaUID, SituacaoDoServidor[] situacoes)
        {
            using (var db = new idbufmtEntities())
            {
                var situacoesUID = SituacoesUID(situacoes);

                var pessoa =
                    db.Pessoa.Where(p => p.pessoaUID == pessoaUID)
                        .Where(s => s.Servidor.Any(y => situacoesUID.Contains(y.situacaoServidorUID)))
                        .FirstOrDefault();

                return new PessoaEntity(pessoa);
            }
        }

        /// <summary>
        /// Consulta genérica de pessoas 
        /// </summary>
        /// <param name="nomeParametro">  Nome da pessoa a ser pesquisada </param>
        /// <param name="valorParametro"> Valor do Parametro a ser pesquisado </param>
        /// <param name="situacoes"> situação do servidor a ser verificado </param>
        /// <param name="registroInicial"> Registro inicial do parametro a ser </param>
        /// <param name="quantidadeDeRegistros">Quantidade de registros a ser verificado </param>
        /// <returns>Retorna uma lista de pessoas com os parametros informados </returns>
        public List<PessoaEntity> ConsultaGenerica(ParametrosPessoa nomeParametro, string valorParametro,
            SituacaoDoServidor[] situacoes, int registroInicial, int quantidadeDeRegistros)
        {
            try
            {
                if (String.IsNullOrEmpty(valorParametro))
                {
                    throw new ArgumentException("O valor do parâmetro não pode ser vazio.");
                }

                if (registroInicial < 0)
                {
                    throw new ArgumentException("Registro inicial não pode ser negativo.");
                }

                if (quantidadeDeRegistros <= 0)
                {
                    throw new ArgumentException("Quantidade de Registros deve ser maior que zero.");
                }

                if (situacoes == null || situacoes.Count() == 0)
                {
                    situacoes = new SituacaoDoServidor[] { SituacaoDoServidor.Ativo };
                }

                var situacoesUID = SituacoesUID(situacoes);

                using (var db = new idbufmtEntities())
                {
                    //var pesq = new entities.PesquisaGenerica<ufmt.services.entities.Pessoa>();

                    List<entities.Pessoa> resultadoPesquisa = null;

                    string expressao = string.Empty;

                    switch (nomeParametro)
                    {
                        case ParametrosPessoa.pessoaUID:
                            {
                                expressao = nomeParametro.ToString() + "=" + valorParametro;
                                break;
                            }
                        case ParametrosPessoa.rg:
                        case ParametrosPessoa.cpf:
                            {
                                expressao = nomeParametro.ToString() + " = \"" + valorParametro + "\"";
                                break;
                            }
                        case ParametrosPessoa.nome:
                            {
                                expressao = nomeParametro.ToString() + " LIKE '%" + valorParametro + "%'";
                                break;
                            }
                        //case ParametrosPessoa.servidorUID:
                        //    {
                        //        throw new NotImplementedException();
                        //    }
                        //case ParametrosPessoa.usuarioUID:
                        //    {
                        //        expressao = "pessoaUID IN (SELECT pessoaUID FROM Usuario WHERE usuarioUID = " + valorParametro + ")";
                        //        break;
                        //        //throw new NotImplementedException();
                        //        var p = GetPessoaPorUsuario(Convert.ToInt64(valorParametro));
                        //        if (p != null)
                        //        {
                        //            var result = new List<PessoaEntity>();
                        //            result.Add(new PessoaEntity(p));
                        //            return result;
                        //        }
                        //        else
                        //        {
                        //            return null;
                        //        }
                        //    }
                        default:
                            {
                                throw new ArgumentException("Parâmetro desconhecido.");
                            }
                    }

                    resultadoPesquisa = db.Pessoa.Where(x =>
                                x.Servidor.FirstOrDefault() != null
                                && x.Servidor.Any(y => situacoesUID.Contains(y.situacaoServidorUID))
                                ).Where(expressao).OrderBy(nomeParametro.ToString()).Skip(registroInicial).Take(quantidadeDeRegistros).ToList();

                    if (resultadoPesquisa != null && resultadoPesquisa.Count > 0)
                    {
                        var result = new List<PessoaEntity>();

                        foreach (var resultado in resultadoPesquisa)
                        {
                            result.Add(new PessoaEntity(resultado));
                        }

                        return result;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (ArgumentException ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        /// <summary>
        /// Verifica se o cpf informado possui algum vinculo com a UFMT
        /// </summary>
        /// <param name="cpf">CPF a ser pesquisado</param>
        /// <returns>String com o tipo de vinculo com a UFMT</returns>
        public string VerificaVinculo(string cpf)
        {
            try
            {
                // Verifico se é um Aluno de Graduacao
                AlunoSiga s = new AlunoSiga();
                var aluno = s.GetAlunosPorCpf(cpf);
                if (aluno.Count > 0)
                {
                    return "AlunoGraduacao";
                }
                return "";
            }
            catch { }

            try
            {
                // Verifico se é um Aluno de Pós-Graduação
                AlunoPos ps = new AlunoPos();
                var retorno = ps.GetAlunoPosPorCPF(cpf);
                if (!string.IsNullOrEmpty(retorno.Cpf.ToString()))
                {
                    return "AlunoPos";
                }
            }
            catch
            {
            }

            try
            {
                using (var db = new idbufmtEntities())
                {
                    var p = db.Pessoa.FirstOrDefault(x => x.cpf == cpf
                                                    && x.Servidor.Any(y => y.situacaoServidorUID == 1));

                    if (p != null)
                    {
                        return "Servidor";
                    }
                }
            }
            catch (Exception)
            {
            }

            return "Não foi possível encontrar registros";
        }
    }
}
