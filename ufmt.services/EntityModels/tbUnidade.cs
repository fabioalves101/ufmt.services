
//------------------------------------------------------------------------------
// <auto-generated>
//    O código foi gerado a partir de um modelo.
//
//    Alterações manuais neste arquivo podem provocar comportamento inesperado no aplicativo.
//    Alterações manuais neste arquivo serão substituídas se o código for gerado novamente.
// </auto-generated>
//------------------------------------------------------------------------------


namespace ufmt.services.EntityModels
{

using System;
    using System.Collections.Generic;
    
public partial class tbUnidade
{

    public tbUnidade()
    {

        this.tbProcessos = new HashSet<tbProcessos>();

    }


    public int UnidadeID { get; set; }

    public double CODIGO { get; set; }

    public double CODIGO_OR { get; set; }

    public string DESCRICAO { get; set; }

    public Nullable<double> REQUERIMENTO { get; set; }

    public Nullable<double> OFICIO { get; set; }

    public string TELEFONE { get; set; }

    public Nullable<int> CODIGOPROAD { get; set; }

    public Nullable<int> ATIVA { get; set; }

    public Nullable<int> grupoID { get; set; }

    public Nullable<int> recebe { get; set; }



    public virtual ICollection<tbProcessos> tbProcessos { get; set; }

}

}
