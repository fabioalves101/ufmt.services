﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ufmt.services.entities;

namespace ufmt.services
{
    /// <summary>
    /// WebService que disponibiliza Informações de Alunos de Pós-Graduação
    /// </summary>
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "AlunoPos" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select AlunoPos.svc or AlunoPos.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class AlunoPos : IAlunoPos
    {
        /// <summary>
        /// Retorna as Informações do Aluno de Pos-Graduação pelo CPF
        /// </summary>
        /// <param name="cpf">CPF a ser pesquisado</param>
        /// <returns>Primeiro aluno encontrado com o CPF informado</returns>
        public Aluno GetAlunoPosPorCPF(string cpf)
        {
            try
            {
                Aluno resultado = null;

                using (var db = new idbufmtEntities())
                {
                    var pos = db.GetDadosAlunoPos.FirstOrDefault(x => x.CPF == cpf);

                    if (pos != null)
                    {
                        resultado = new Aluno(pos);
                    }
                    else
                    {
                        throw new FaultException("Nenhum estudante de pós-graduação encontrado com os dados informados.");
                    }
                }

                return resultado;
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Retorno as Informações do Aluno de Pós-Graduação pelo RGA
        /// </summary>
        /// <param name="rga">RGA a ser pesquisado</param>
        /// <returns>Primeiro aluno encontrado com o RGA informado</returns>
        public Aluno GetAlunoPosPorMatricula(string rga)
        {
            try
            {
                long matricula;

                if (!long.TryParse(rga, out matricula))
                {
                    throw new FaultException("Não é uma matrícula válida.");
                }

                Aluno resultado = null;

                using (var db = new idbufmtEntities())
                {
                    var alunoPos = db.GetDadosAlunoPos.FirstOrDefault(x => x.rga == matricula);

                    if (alunoPos != null)
                    {
                        resultado = new Aluno(alunoPos);
                    }
                    else
                    {
                        throw new FaultException("Nenhum estudante de pós-graduação encontrado com os dados informados.");
                    }
                }
                
                return resultado;
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Atualização de senha do aluno
        /// </summary>
        /// <param name="rga">Do aluno a ser atualizado a senha</param>
        /// <param name="senha_antiga">senha antiga ou senha atual do aluno</param>
        /// <param name="senha_nova">senha nova do aluno</param>
        /// <returns>
        ///     True -> em caso de sucesso na Atualização da senha
        ///     False -> em caso de falha na Atualização da senha
        /// </returns>
        public void AtualizaSenhaAluno(string rga, string senha_antiga, string senha_nova)
        {
            AlunoSiga aluno = new AlunoSiga();
            aluno.AlterarSenhaAluno(rga, senha_antiga, senha_nova);
        }
        
        /// <summary>
        /// Reenvia a senha do Aluno
        /// </summary>
        /// <param name="rga"></param>
        /// <returns></returns>
        public bool ReenvioSenhaAluno(string rga)
        {
            try
            {
                var rgaDec = Convert.ToDecimal(rga);
                using (var db = new idbufmtEntities())
                {
                    var aluno = db.GetDadosAlunoPos.Where(x => x.rga == rgaDec).FirstOrDefault();

                    if (aluno != null)
                    {
                        if (!string.IsNullOrEmpty(aluno.Email))
                        {
                            StringBuilder sb = new StringBuilder();
                            sb.Append("A sua senha é: " + aluno.senha.ToString());

                            Email e = new Email();
                            e.EnviarSmtpPadraoVersaoDois(aluno.Email, sb.ToString(), "Reenvio de senha");

                            return true;
                        }
                        else
                        {
                            throw new FaultException("Aluno não possui endereço de email cadastrado.");
                        }
                    }
                    else
                    {
                        throw new FaultException("Aluno não encontrado.");
                    }
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }
    }
}
