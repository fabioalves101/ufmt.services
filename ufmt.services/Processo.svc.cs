﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ufmt.services.entities;
using ufmt.services.EntityModels;

namespace ufmt.services
{
    /// <summary>
    /// Métodos de consulta a informações de processos  
    /// </summary>
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Processo" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Processo.svc or Processo.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class Processo : IProcesso
    {
        private const int usuarioUIDProtocolo = 33; //33 -- fixo: usuário do protocolo central, Cuiabá
        //private const int destino = 135; //135 -- fixo: STI - CAE - SECRETARIA DA COORDENAÇÃO
        private const string ip = "192.168.226.231";
       
        /// <summary>
        ///  Cadastra Processos 
        /// </summary>
        /// <param name="assunto"> Assunto do processo a ser cadastrado </param>
        /// <param name="requerente"> Requerente do processo a ser cadastrado </param>
        /// <param name="nroPaginas"> Numero de paginas do processo a ser cadastrado</param>
        /// <param name="unidadeDestino"> Unidade de destino do processo </param>
        /// <returns> Retorna uma instancia de processos </returns>
        public ProcessoEntity CadastrarProcesso(string assunto, string requerente, int? nroPaginas, int unidadeDestino)
        {
            try
            {
                //throw new NotImplementedException();

                var _numero = new ObjectParameter("numero", typeof(string));
                var _datap = new ObjectParameter("datap", typeof(DateTime));
                var _hora = new ObjectParameter("hora", typeof(string));
                var _resposta = new ObjectParameter("resposta", typeof(int));

                using (var db = new sreprocessoEntities())
                {
                    var a = db.spCadastraProcesso(usuarioUIDProtocolo,
                         assunto,
                         requerente,
                         unidadeDestino,
                         ip,
                         nroPaginas,
                         _resposta, _numero, _datap, _hora
                         );
                }

                return GetProcesso(_numero.Value.ToString());
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Pesquisa de processo por Número 
        /// </summary>
        /// <param name="numero">Numero do processo  a ser pesquisado </param>
        /// <returns> Retorna uma instancia de processo </returns>
        public ProcessoEntity GetProcesso(string numero)
        {
            try
            {
                if (string.IsNullOrEmpty(numero))
                {
                    throw new ArgumentException("Número não pode ser vazio.");
                }

                using (var db = new sreprocessoEntities())
                {
                    var processo = db.tbProcessos.FirstOrDefault(x => x.NUMERO == numero);
                    var tramites = db.tbTramite.Where(x => x.processoID == processo.processoID).ToList();
                    List<TramiteProcessoEntity> trmts = new List<TramiteProcessoEntity>();
                    foreach (var item in tramites)
                    {
                        trmts.Add(new TramiteProcessoEntity(item));
                    }
                    if (processo != null)
                    {
                        return new ProcessoEntity(processo, trmts);
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
    }
}
