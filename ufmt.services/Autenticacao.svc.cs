﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using ufmt.sig.business;
using ufmt.sig.entity;
using System.ServiceModel.Activation;
using ufmt.services.entities;
using System.Security.Cryptography;
using System.Data;
using ufmt.services.EntityModels;
using System.DirectoryServices.Protocols;
using System.Net;

namespace ufmt.services
{
    /// <summary>
    ///  Serviço de autenticaçao de usuarios 
    /// </summary>
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class Autenticacao : IAutenticacao
    {
        /// <summary>
        /// Faz autenticação do usuário por nome e senha 
        /// </summary>
        /// <param name="nomeAcesso">Nome de acesso para fazer o login do usuário</param>
        /// <param name="senha"> senha de acesso para fazer login de usuário </param>
        /// <returns>Retorna os dados do usuario </returns>
        public ufmt.sig.entity.Usuario Login(string nomeAcesso, string senha)
        {
            try
            {
                nomeAcesso = NormalizaCPF(nomeAcesso);

                using (IAutenticacaoBO autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
                {
                    try
                    {
                        ufmt.sig.entity.Usuario usuario = autenticacaoBO.GetUsuario(nomeAcesso, senha);
                        return usuario;
                    }
                    catch (Exception ex)
                    {
                        throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
                    }
                }

            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Faz autenticaçao do usuario docente por login e senha
        /// </summary>
        /// <param name="nomeAcesso">nome de acesso do usuario </param>
        /// <param name="senha">Senha de acesso do usuario </param>
        /// <returns> retorna autenticaçao do usuario</returns>
        public ufmt.sig.entity.Usuario LoginDocente(string nomeAcesso, string senha)
        {
            try
            {
                using (IAutenticacaoBO autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
                {
                    try
                    {
                        ufmt.sig.entity.Usuario usuario = autenticacaoBO.GetUsuario(nomeAcesso, senha);

                        using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                        {
                            IList<sig.entity.Servidor> list = pessoaBO.GetVinculosDocenciaPorPessoa(usuario.Pessoa.PessoaUID);
                            if (list.Count > 0)
                            {
                                return usuario;
                            }
                            else
                            {
                                throw new FaultException<Exception>(new Exception("O usuário não é um docente"), new FaultReason("O usuário não é um docente"));
                            }
                        }

                    }
                    catch (FaultException<Exception> ex)
                    {
                        throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
                    }
                }

            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Pesquisa os dados apartir do UID de uma pessoa 
        /// </summary>
        /// <param name="pessoaUID"></param>
        /// <returns>Retorna as informações do servidor encontrado </returns>
        public sig.entity.Servidor GetVinculoPessoaSimples(long pessoaUID)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    sig.entity.Servidor servidor = pessoaBO.GetVinculosPorPessoa(pessoaUID, true).First();

                    sig.entity.Servidor s = new sig.entity.Servidor();
                    s.Registro = servidor.Registro;
                    s.TipoRegistro = servidor.TipoRegistro;

                    return s;
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Pesquisa dados de pessoas por UID 
        /// </summary>
        /// <param name="pessoaUID">UID do usuário a ser pesquisado </param>
        /// <returns> Retorna uma lista com os dados dos servidores  </returns>
        public IList<sig.entity.Servidor> GetVinculosPorPessoa(long pessoaUID)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    IList<sig.entity.Servidor> servidorList = pessoaBO.GetVinculosPorPessoa(pessoaUID, true);

                    IList<sig.entity.Servidor> newServidorList = new List<sig.entity.Servidor>();

                    foreach (sig.entity.Servidor servidor in servidorList)
                    {
                        servidor.UnidadeLotacao = null;
                        newServidorList.Add(servidor);

                    }
                    return newServidorList;
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Pesquisa pessoas por nome
        /// </summary>
        /// <param name="Nome">Nome a ser pesquisado</param>
        /// <returns></returns>
        public IList<ufmt.sig.entity.Pessoa> GetPessoas(string Nome)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    IList<ufmt.sig.entity.Pessoa> p = pessoaBO.GetPessoas(Nome);
                    return p;
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Pesquisa dados de pessoas por UID 
        /// </summary>
        /// <param name="pessoaUID"> UID da pessoa a ser pesquisada</param>
        /// <returns> Retorna dados de uma pessoa </returns>
        public PessoaEntity GetPessoa(long pessoaUID)
        {
            try
            {
                using (var db = new idbufmtEntities())
                {
                    var pessoa = db.Pessoa.Where(x => x.pessoaUID == pessoaUID).First();
                    return new PessoaEntity(pessoa);
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Pesquisa dados de docente por UID 
        /// </summary>
        /// <param name="pessoaUID"> UID do docente a ser pesuisado </param>
        /// <returns>Retorna uma lista com os dados dos Servidores </returns>
        public IList<sig.entity.Servidor> GetVinculoDocente(long pessoaUID)
        {
            try
            {
                using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
                {
                    IList<sig.entity.Servidor> servidorList = pessoaBO.GetVinculosDocenciaPorPessoa(pessoaUID);

                    IList<sig.entity.Servidor> newServidorList = new List<sig.entity.Servidor>();

                    foreach (sig.entity.Servidor servidor in servidorList)
                    {
                        servidor.UnidadeLotacao = null;
                        newServidorList.Add(servidor);
                    }
                    return newServidorList;
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Faz autenticaçao de um usuário aluno por nome e senha  
        /// </summary>
        /// <param name="rga"> Numero de matrícula do aluno </param>
        /// <param name="senha">Senha de acesso </param>
        /// <returns> Retorna os dados do aluno </returns>
        public entities.Aluno LoginAluno(string rga, string senha)
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {
                    decimal matricula = decimal.Parse(rga);

                    GetDadosAluno al =
                        (from c in contexto.GetDadosAluno
                         where c.rga == matricula &&
                               c.senha.Trim().Equals(senha)
                         select c).FirstOrDefault();

                    if (al != null)
                    {
                        Aluno aluno = new Aluno(al);

                        return aluno;
                    }
                    else
                    {
                        string msg = "Nenhum aluno encontrado com os dados informados";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }

        }

        /// <summary>
        /// Faz autenticação de discentes por matrícula e senha
        /// </summary>
        /// <param name="rga"> Número de matrícula do discente</param>
        /// <param name="senha"> Senha de acesso do discente</param>
        /// <returns>Retorna os dados do discente </returns>
        public entities.Aluno LoginDiscente(string rga, string senha)
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {
                    decimal matricula = decimal.Parse(rga);

                    GetDadosAluno al = (from c in contexto.GetDadosAluno
                                        where c.rga == matricula &&
                                              c.senha.Trim().Equals(senha)
                                        select c).FirstOrDefault();

                    if (al != null)
                    {
                        Aluno aluno = new Aluno(al);

                        return aluno;
                    }
                    else
                    {
                        string msg = "Nenhum aluno encontrado com os dados informados";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException ex)
            {
                throw new FaultException<Exception>(
                    new Exception(ex.Message),
                    new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// verica a permissão de um usuário para uma determinada aplicação atraves de UID  
        /// </summary>
        /// <param name="aplicacaoUID">O UID da aplicaçao a ser verificada </param>
        /// <param name="permissaoUID"> O UID de permissao</param>
        /// <param name="usuarioUID">UID do usuário a ser autenticado </param>
        /// <returns> Retorna os dados de permissão do usuário para a aplicação selecionada</returns>
        public ufmt.sig.entity.PermissaoUsuario GetPermissao(long aplicacaoUID, long permissaoUID, long usuarioUID)
        {
            using (IAutenticacaoBO autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
            {
                ufmt.sig.entity.PermissaoUsuario permissaoUsuario =
                    autenticacaoBO.GetPermissaoUsuario(aplicacaoUID, permissaoUID, usuarioUID);
                return permissaoUsuario;
            }
        }

        /// <summary>
        /// Faz autenticação da permissao de um usuário ou aplicação por UID
        /// </summary>
        /// <param name="aplicacaoUID">UID da aplicação a ser autenticado</param>
        /// <param name="usuarioUID">UID do usuário a ser autenticado</param>
        /// <returns> Retorna uma lista de permissões do usuário para uma determinada aplicação </returns>
        public List<PermissaoUsuarioEntity> GetPermissaoPorUsuarioAplicacao(long aplicacaoUID, long usuarioUID)
        {
            try
            {
                using (var db = new idbufmtEntities())
                {
                    var permison = from a in db.Permissao1Set
                                   join b in db.PermissaoUsuario1Set on a.permissaoUID equals b.permissaoUID
                                   join c in db.Usuario1Set on b.usuarioUID equals c.usuarioUID
                                   join d in db.Aplicacao on a.aplicacaoUID equals d.aplicacaoUID
                                   where d.aplicacaoUID == aplicacaoUID && c.usuarioUID == usuarioUID
                                   select new
                                   {
                                       DataLimite = b.dataLimite,
                                       Permissao = b.Permissao,
                                       PermissaoUsuarioUID = b.permissaoUsuarioUID,
                                       PermissaoUID = b.permissaoUID,
                                       Usuario = b.Usuario
                                   };

                    if (permison != null)
                    {
                        List<PermissaoUsuarioEntity> permissoes = new List<PermissaoUsuarioEntity>();
                        foreach (var item in permison)
                        {
                            permissoes.Add(new PermissaoUsuarioEntity()
                            {
                                DataLimite = item.DataLimite,
                                PermissaoUsuarioUID = item.PermissaoUsuarioUID,
                                Permissao = new PermissaoEntity(item.Permissao),
                                Usuario = new UsuarioEntity(item.Usuario)
                            });
                        }

                        return permissoes;
                    }
                    else
                    {
                        throw new FaultException("Não há permissoes para a Aplicação ou para o Usuario Informado.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Verifica dados de usuário por nome e senha de acesso
        /// </summary>
        /// <param name="nomeAcesso"> Nome de acesso do usuário </param>
        /// <param name="senha">Senha de acesso do usuário </param>
        /// <returns>Retorna dados do usuário verificado </returns>
        public string VerificaDados(string nomeAcesso, string senha)
        {
            return "nomeAcesso='" + nomeAcesso + "' e senha='" + senha + "'";
        }

        /// <summary>
        /// Verificar Delphi por nome e senha do usuario
        /// </summary>
        /// <param name="nomeAcesso">Nome de acesso do usuario</param>
        /// <param name="senha">Senha de acesso do usuario</param>
        /// <returns>Retorna os dados do usuário </returns>
        public ufmt.sig.entity.Usuario LoginDelphi(string nomeAcesso, string senha)
        {
            try
            {
                return this.Login(nomeAcesso, senha);
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Valida os dados dos coordenadores de curso 
        /// </summary>
        /// <param name="username">Nome de acesso do cordenador </param>
        /// <param name="password">Senha de acesso do cordenador </param>
        /// <returns>Retorna as informações do cordenador</returns>
        public Coordenador LoginCoordenadorCurso(string username, string password)
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {
                    var result =
                        (from c in contexto.vwCoordenadores
                         where c.usuario == username && c.senha == password
                         select c).FirstOrDefault();

                    if (result != null)
                    {

                        Coordenador coordenador = new Coordenador(result);

                        if (result.siape.HasValue)
                            coordenador.Siape = result.siape.Value;

                        return coordenador;
                    }
                    else
                    {
                        string msg = "Nenhum usuário encontrado com os dados informados";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        ///  verica os dados dos coordenadores por chave 
        /// </summary>
        /// <param name="chave"> Chave de acesso do cordenador</param>
        /// <returns> Retorna os dados do cordenador </returns>
        public Coordenador GetCoordenadorPorChave(string chave)
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {
                    Guid chaveGuid = new Guid(chave);
                    var result =
                        (from c in contexto.vwCoordenadores
                         where c.chave.Value == chaveGuid
                         select c).FirstOrDefault();

                    if (result != null)
                    {

                        Coordenador coordenador = new Coordenador(result);

                        if (result.siape.HasValue)
                            coordenador.Siape = result.siape.Value;

                        return coordenador;
                    }
                    else
                    {
                        string msg = "Nenhum usuário encontrado com os dados informados";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        /// Verifica os dados dos coordenadores por chave e codigo do Curso
        /// </summary>
        /// <param name="chave">Chave de acesso do cordenador </param>
        /// <param name="codigoCurso">Codigo do curso do cordenador</param>
        /// <returns>Retorna as informações do cordenador </returns>
        public Coordenador GetCoordenadorPorChaveCodigoCurso(string chave, string codigoCurso)
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {
                    int iCodigoCurso = int.Parse(codigoCurso);

                    Guid chaveGuid = new Guid(chave);
                    var result =
                        (from c in contexto.vwCoordenadores
                         where c.chave.Value == chaveGuid && c.codigoCurso == iCodigoCurso
                         select c).FirstOrDefault();

                    if (result != null)
                    {
                        Coordenador coordenador = this.ConvertVwCoordenadoresToCoordenador(result);

                        if (result.siape.HasValue)
                            coordenador.Siape = result.siape.Value;

                        return coordenador;
                    }
                    else
                    {
                        string msg = "Nenhum usuário encontrado com os dados informados";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        /// Verifica os dados de docentes por chave
        /// </summary>
        /// <param name="chave"> chave de acesso do docente</param>
        /// <returns>Retorna os dados do docente </returns>
        public PessoaEntity GetDocentePorChave(string chave)
        {
            try
            {
                using (idbufmtEntities contexto = new idbufmtEntities())
                {


                    Guid chaveGuid = new Guid(chave);
                    var result =
                        (from c in contexto.ViewSigaProfessor
                         where c.SigaChaveAcessoDocente.Value == chaveGuid
                         select c).FirstOrDefault();

                    if (result != null)
                    {
                        var pessoa = contexto.Pessoa.Where(x => x.cpf == result.SigaCpfProf).FirstOrDefault();

                        PessoaEntity pessoaEntity = new PessoaEntity(pessoa);

                        return pessoaEntity;
                    }
                    else
                    {
                        string msg = "Nenhum usuário encontrado com os dados informados";
                        throw new FaultException<Exception>(
                            new Exception(msg),
                            new FaultReason(msg));
                    }
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        /// Verifica os dados do colaborador CAEP por chave
        /// </summary>
        /// <param name="chave">Chave de acesso do colaborador CAEP</param>
        /// <returns>Retorna a informaçao do colaborador</returns>
        public PessoaEntity GetColaboradorCAEPorChave(string chave)
        {
            try
            {
                using (var db = new DbSigaEntities())
                {
                    Guid g = new Guid(chave);
                    var cae = db.SIGATSENHA.Where(x => x.SigaChaveAcesso == g).FirstOrDefault();

                    if (cae != null)
                    {
                        Pessoa p = new Pessoa();
                        var pessoa = p.GetPessoaPorCPF(cae.SigaCPFSenha);

                        return new PessoaEntity(pessoa);
                    }
                    else
                    {
                        throw new FaultException("Não Foi Possível encontrar colaborador");
                    }
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException("Não Foi Possível encontrar colaborador");
            }

        }
        
        /// <summary>
        /// Verifica os dados do usuario do aluno por chave 
        /// </summary>
        /// <param name="chave">Chave de acesso do usuaria do aluno</param>
        /// <returns>Retorna dados do aluno</returns>
        public Aluno LoginAluno(string chave)
        {
            try
            {
                using (idbufmtEntities db = new idbufmtEntities())
                {
                    Guid guidChave = new Guid(chave);

                    GetDadosAluno al = db.GetDadosAluno.Where(a => a.chave == guidChave).FirstOrDefault();

                    Aluno aluno = new Aluno(al);

                    return aluno;
                }
            }
            catch (FaultException<Exception> ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Realiza uma converçao vwCoordenadores para coordenadores 
        /// </summary>
        /// <param name="result">Valor a ser convertido</param>
        /// <returns>Retorna os dados do coordenanor</returns>
        public Coordenador ConvertVwCoordenadoresToCoordenador(vwCoordenadores result)
        {
            Coordenador coordenador = new Coordenador()
            {
                CampusUID = result.campusUID.Value,
                CodigoCurso = result.codigoCurso,
                CoordenadorUID = result.coordenadorUID,
                cpf = result.cpf,
                Email = result.email,
                Nome = result.nomeCoordenador,
                Usuario = result.usuario
            };

            return coordenador;
        }
        
        /// <summary>
        /// Reinicia a senha do cpf passado por parametro
        /// </summary>
        /// <param name="cpf"> cpf do usuario a ser alterado </param>
        public void ReiniciarSenha(string cpf)
        {
            try
            {
                cpf = NormalizaCPF(cpf);

                string novaSenha = string.Empty;

                using (var db = new idbufmtEntities())
                {
                    //Busca a Pessoa com o CPF informado.
                    var pessoa = db.Pessoa.FirstOrDefault(x => x.cpf == cpf);

                    //Se não há uma pessoa registrada com esse CPF...
                    if (pessoa == null)
                    {
                        //...lança uma exceção para o consumidor do serviço.
                        throw new Exception("Não há registro de servidor com este cpf.");
                    }

                    //Tento pegar o último registro de usuário da Pessoa que não esteja bloqueado...
                    var user = pessoa.Usuario1.LastOrDefault(x => !x.bloqueado);

                    //Se não existir usuário NÃO BLOQUEADO...
                    if (user == null)
                    {
                        //...Tento encontrar qualquer usuário, mesmo bloqueado.
                        user = pessoa.Usuario1.LastOrDefault();

                        string mensagem = string.Empty;

                        //Neste caso, a verificação se o Usuário é nulo é apenas para escolher a mensagem da exceção que será passada ao consumidor do serviço.
                        if (user == null)
                            mensagem = "Não há usuário para o servidor encontrado. Cadastre seu usuário na área de \"Cadastrar novo Usuário\" do SIA (Sistema Integrado de Acesso).";
                        else
                        {
                            mensagem = "Usuário está bloqueado.";
                        }

                        throw new Exception(mensagem);
                    }

                    novaSenha = NovaSenhaValida();

                    user.senhaAcesso = CriptografarSenha(novaSenha);

                    db.SaveChanges();

                    try
                    {
                        var emailServ = new Email();

                        emailServ.EnviarSmtpPadraoVersaoDois(pessoa.mail, MensagemDeSenha(novaSenha), "Solicitação de Reinicio de Senha - SIA UFMT");
                    }
                    catch (Exception ex)
                    {
                        throw new Exception("Problema no envio de email. Detalhes do Erro: " + ex.Message);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        //public void CriarUsuario(string cpf, string email)
        //{
        //    try
        //    {
        //        using (IAutenticacaoBO autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
        //        {
        //            sig.entity.Pessoa pessoa = null;
        //            using (IPessoaBO pessoaBO = BusinessFactory.GetInstance<IPessoaBO>())
        //            {
        //                pessoa = pessoaBO.GetPessoa(cpf);
        //                pessoa.Mail = email;
        //                pessoaBO.SalvePessoa(pessoa);
        //            }
        //            if (pessoa != null)
        //                autenticacaoBO.CrieUsuario(pessoa, pessoa.Cpf, null);
        //            else
        //                throw new Exception("Não foi encontrado registro de pessoa com os dados informados");
        //        }
        //    }
        //    catch (FaultException<Exception> ex)
        //    {
        //        throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
        //    }
        //}

        /// <summary>
        /// Faz cadastro de usuário  
        /// </summary>
        /// <param name="cpf">cpf do usuario a ser cadastrado </param>
        /// <param name="siape"> siape do usuario a ser cadastrado </param>
        /// <param name="email"> email do usuario a ser cadastrado </param>
        public void CriarUsuario(string cpf, string siape, string email)
        {
            try
            {
                string novaSenha = string.Empty;

                using (var db = new idbufmtEntities())
                {
                    //Recupera informações da pessoa pelo CPF e siape.
                    var pessoa = db.Pessoa.FirstOrDefault(x => x.cpf == cpf && x.Servidor.Any(y => y.registro == siape));

                    //Caso não haja pessoa com o cpf e siape informados, lança exceção.
                    if (pessoa == null)
                    {
                        throw new ArgumentException("Não existe pessoa nos registros com o CPF e SIAPE informados. Verifique as informações digitadas. Caso seu email esteja desatualizado, contate a STI.");
                    }

                    //Procura um servidor ativo ligado aquela pessoa.
                    var servidor = pessoa.Servidor.FirstOrDefault(x => x.situacaoServidorUID == 1);

                    //caso não encontra um servidor ativo ligado a pessoa...
                    if (servidor == null)
                    {
                        //Procura a última entrada de servidor em qualquer situação possível.
                        servidor = pessoa.Servidor.LastOrDefault();

                        //Se mesmo assim não há servidor com estes dados, lança uma exceção.
                        if (servidor == null)
                        {
                            throw new ArgumentException("Não existe nenhum dado de servidor para esse CPF.");
                        }
                    }

                    //Procuro pela última entrada regristrada desse usuário.
                    var usuario = pessoa.Usuario1.LastOrDefault();

                    //Caso não haja nenhuma entrada...
                    if (usuario == null)
                    {
                        //Crio uma nova entrada.
                        usuario = new Usuario1()
                        {
                            usuarioUID = 0,
                            pessoaUID = pessoa.pessoaUID,
                            bloqueado = false,
                            nomeAcesso = cpf,
                            dataProximaTrocaSenha = DateTime.Now,
                            dataUltimoAcesso = DateTime.Now
                        };

                        //adiciono no banco de dados.
                        db.Usuario1Set.Add(usuario);
                    }

                    //cria uma senha aleatória.
                    novaSenha = NovaSenhaValida();

                    //Criptografa e salva a senha nova.
                    usuario.senhaAcesso = CriptografarSenha(novaSenha);

                    //Atualiza o email da pessoa.
                    pessoa.mail = email;

                    //Salva todas as alterações.
                    db.SaveChanges();
                }

                try
                {
                    var emailService = new Email();

                    emailService.EnviarSmtpPadraoVersaoDois(email, MensagemDeSenha(novaSenha), "Criação de Usuário de Acesso Integrado - SIA UFMT");
                }
                catch (Exception ex)
                {
                    throw new Exception("Usuário criado com sucesso, mas houve um problema no envio do email. Reinicie sua senha para recuperar seu acesso. Detalhes do Erro de envio: " + ex.Message);
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        /// Verifica a situaçao do servidor
        /// </summary>
        /// <param name="situacoes">Tipo de situações a serem pesquisadas </param>
        /// <returns>retorna um array de situações</returns>
        private int[] vetorDeSituacaoServidorUID(SituacaoDoServidor[] situacoes)
        {
            var aux = new List<int>();

            foreach (var s in situacoes) { aux.Add((int)s); }

            return aux.ToArray();
        }
        
        /// <summary>
        /// Faz alteração da senha de um usuario identificado por cpf 
        /// </summary>
        /// <param name="cpf"> cpf de usuario </param>
        /// <param name="senhaAtual">Senha que usuario está usando atualmente </param>
        /// <param name="novaSenha">Nova senha para o usuario </param>
        public void AlterarSenha(string cpf, string senhaAtual, string novaSenha)
        {
            try
            {
                cpf = NormalizaCPF(cpf);

                if (string.IsNullOrEmpty(novaSenha) || novaSenha.Length < 6)
                {
                    throw new ArgumentException("A nova senha deve conter no mínimo 6 caracteres.");
                }

                using (var db = new idbufmtEntities())
                {
                    var pessoa = db.Pessoa.FirstOrDefault(x => x.cpf == cpf);

                    if (pessoa == null)
                    {
                        throw new NotImplementedException("Nenhuma pessoa encontrada com este CPF.");
                    }

                    var user = pessoa.Usuario1.LastOrDefault(x => !x.bloqueado);

                    if (user == null)
                    {
                        pessoa.Usuario1.LastOrDefault();
                    }

                    if (user != null)
                    {
                        if (user.bloqueado)
                        {
                            throw new Exception("Usuário encontrado está bloqueado.");
                        }

                        if (SenhaValida(user.senhaAcesso, senhaAtual))
                        {
                            // Altera a senha no Banco de Dados IDBUFMT
                            var senhaAlterada = CriptografarSenha(novaSenha);
                            user.senhaAcesso = senhaAlterada;
                            user.dataProximaTrocaSenha = DateTime.Today.AddYears(1);
                            db.SaveChanges();

                            // Altera a senha no LDAP SEI
                            var sucesso = this.AlterarSenhaLdap(cpf, senhaAlterada);
                        }
                        else
                        {
                            throw new Exception("Senha informada não coincide com a senha atual do usuário.");
                        }
                    }
                    else
                    {
                        throw new Exception("Nenhum usuário encontrado com o login informado.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Altera a senha no LDAP
        /// </summary>
        /// <param name="cpf">CPF a ser alterado</param>
        /// <param name="senhaAlterada">Senha a ser alterada no LDAP</param>
        protected bool AlterarSenhaLdap(string cpf, string senhaAlterada)
        {
            try
            {
                var serverId = new LdapDirectoryIdentifier("192.168.109.167", 389);
                var conn = new LdapConnection(serverId);
                conn.AuthType = AuthType.Basic;
                conn.SessionOptions.ProtocolVersion = 3;

                var credenciais = new NetworkCredential("cn=admin,dc=sei-ldap,dc=ufmt,dc=br", "Sei@ldap#2017");

                conn.Timeout = TimeSpan.Parse("300");

                conn.Bind(credenciais);

                DirectoryAttributeModification mod = new DirectoryAttributeModification();
                mod.Operation = DirectoryAttributeOperation.Replace;
                mod.Name = "userPassword";
                mod.Add("{MD5}" + senhaAlterada);

                ModifyRequest modr = new ModifyRequest("uid=" + cpf + ",ou=people,dc=sei-ldap,dc=ufmt,dc=br", mod);
                conn.SendRequest(modr);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// Pesquisa pessoas por UID 
        /// </summary>
        /// <param name="permissaoUID"> UID de permissão da pessoa a ser pesquisada  </param>
        /// <returns>Retorna uma lista com os dados da pessoa pesquisada </returns>
        public List<PessoaEntity> GetPessoasPorPermissao(long permissaoUID)
        {
            try
            {
                List<ufmt.services.entities.Pessoa> listaDePessoas;

                using (idbufmtEntities db = new idbufmtEntities())
                {
                    listaDePessoas = db.Pessoa.Where(x =>
                    x.Usuario1.Any(y => y.PermissaoUsuario.Any(z => z.permissaoUID == permissaoUID)
                    )).ToList();

                    var resultado = new List<PessoaEntity>();

                    foreach (var pessoa in listaDePessoas)
                    {
                        resultado.Add(new PessoaEntity(pessoa));
                    }

                    return resultado;
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        ///  Salva permissão de um usuario 
        /// </summary>
        /// <param name="usuarioUID">UID do usuário solicitado </param>
        /// <param name="permissaoUID"> UID de permssão </param>
        /// <param name="dataLimite">Data limite da permissão </param>
        public void SalvarPermissaoUsuario(long usuarioUID, long permissaoUID, DateTime? dataLimite)
        {
            try
            {
                using (var autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
                {
                    var Permissao = autenticacaoBO.GetPermissao(permissaoUID);

                    if (Permissao == null)
                    {
                        throw new ArgumentException("Permissão não existe.");
                    }

                    var Usuario = autenticacaoBO.GetUsuario(usuarioUID);

                    if (Usuario == null || Usuario.Bloqueado)
                    {
                        throw new ArgumentException("Usuário não existe ou está bloqueado.");
                    }

                    autenticacaoBO.SalvePermissaoUsuario(Permissao, Usuario, dataLimite);
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Pesquisa um usuario de uma Pessoa
        /// </summary>
        /// <param name="pessoaUID">UID da pessoa a ser pesquisada </param>
        /// <returns>Retorna uma instancia de Usuario Ativo</returns>
        public sig.entity.Usuario GetUsuarioPorPessoa(long pessoaUID)
        {
            try
            {
                sig.entity.Usuario resultado;

                using (var autenticacaoBO = BusinessFactory.GetInstance<IAutenticacaoBO>())
                {
                    resultado = autenticacaoBO.GetUsuarioPorPessoa(pessoaUID);
                }

                return resultado;
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Verifica se uma senha é valida atraves de uma senha de acesso    
        /// </summary>
        /// <param name="senhaAcesso"> senha de acesso </param>
        /// <param name="senhaInformada">Senha a ser verificada  </param>
        /// <returns> retorna dados da senha verificada </returns>
        private bool SenhaValida(string senhaAcesso, string senhaInformada)
        {
            return senhaAcesso.Equals(CriptografarSenha(senhaInformada));
        }

        /// <summary>
        ///   Criptografa senhas 
        /// </summary>
        /// <param name="senhaInformada">Senha a ser criptografada </param>
        /// <returns> Retorma senha criptografada </returns>
        private string CriptografarSenha(string senhaInformada)
        {
            return Convert.ToBase64String(new MD5CryptoServiceProvider().ComputeHash(Encoding.GetEncoding("iso-8859-1").GetBytes(senhaInformada)));
        }
        
        /// <summary>
        /// Cria uma nova senha válida 
        /// </summary>
        /// <returns> Uma nova senha válida </returns>
        private string NovaSenhaValida()
        {
            Guid g = Guid.NewGuid();
            string GuidString = Convert.ToBase64String(g.ToByteArray());

            //Eliminando caracteres indesejados.
            GuidString = GuidString.Replace("=", "");
            GuidString = GuidString.Replace("+", "");
            GuidString = GuidString.Replace("/", "");

            //Fazendo todos os caracteres serem minúsculos.
            GuidString = GuidString.ToLowerInvariant();

            return new string(GuidString.Take(8).ToArray());
        }
        
        /// <summary>
        /// Exibe mensagens com informações da senha 
        /// </summary>
        /// <param name="senha"> senha a ser verificada </param>
        /// <returns>Retorna mensagem de informação da senha  </returns>
        private string MensagemDeSenha(string senha)
        {
            var sb = new StringBuilder();

            sb.Append("Por favor, acesse novamente o sistema informando seu CPF como nome de login e a senha abaixo.");
            sb.Append("<br>");
            sb.Append("Na primeira vez, o sistema lhe pedirá para trocar a senha, assim você poderá colocar a senha que achar melhor.");
            sb.Append("<br>");
            sb.Append("<br>");
            sb.Append("Atenciosamente,");
            sb.Append("<br>");
            sb.Append("<br>"); ;
            sb.Append("Secretaria de Tecnologia da Informação");
            sb.Append("<br>");
            sb.Append("Universidade Federal de Mato Grosso");
            sb.Append("<br>");
            sb.Append("Sua senha no sistema é: ->");
            sb.Append(senha);
            sb.Append("<- (caracteres compreendidos entre as setas).");

            return sb.ToString();

        }
        
        /// <summary>
        /// Mensagem padrão a ser enviada em caso de troca de senha
        /// </summary>
        /// <param name="senha"> Senha a ser enviada para o cliente</param>
        /// <returns>Mensagem a ser enviada por email</returns>
        private string MensagemDeSenhaPadraoIntegrado(string senha)
        {
            var sb = new StringBuilder();

            sb.Append("Por favor, acesse o seguinte endereço: http://sistemas.ufmt.br/ufmt.sia/acesso/" + senha);
            sb.Append("<br>");
            sb.Append("Será solicitado o seu CPF para confirmação da Alteração da senha.");
            sb.Append("<br>");
            sb.Append("<br>");
            sb.Append("Atenciosamente,");
            sb.Append("<br>");
            sb.Append("<br>"); ;
            sb.Append("Secretaria de Tecnologia da Informação");
            sb.Append("<br>");
            sb.Append("Universidade Federal de Mato Grosso");

            return sb.ToString();
        }
        
        /// <summary>
        /// Login por situação do Servidor
        /// </summary>
        /// <param name="nomeAcesso">Nome de acesso do usuario a ser pesquisado</param>
        /// <param name="senhaAcesso">Senha de acesso do usuario a ser pessoquido</param>
        /// <param name="situacoes"> Situaçao atual do usuario a ser pesquisado</param>
        /// <returns>Retorna um usuario de acordo com a situação passada por parametro</returns>
        public sig.entity.Usuario LoginPorSituacao(string nomeAcesso, string senhaAcesso, SituacaoDoServidor[] situacoes)
        {
            nomeAcesso = NormalizaCPF(nomeAcesso);

            //Caso não haja nenhuma situação específica para login, é utilizado o método padrão.
            if (situacoes == null || situacoes.Count() == 0)
            {
                //Tento o login normal ou lanço uma exceção?
                return Login(nomeAcesso, senhaAcesso);
            }
            else
            {
                var situacoesUID = vetorDeSituacaoServidorUID(situacoes);

                sig.entity.Usuario resultado = null;

                using (var db = new idbufmtEntities())
                {
                    var usuario = db.Usuario1Set.FirstOrDefault(x => x.nomeAcesso == nomeAcesso &&
                                                                     x.Pessoa.Servidor.Any(y => situacoesUID.Contains(y.SituacaoServidor.situacaoServidorUID)));

                    if (usuario != null)
                    {
                        if (usuario.dataProximaTrocaSenha.HasValue && usuario.dataProximaTrocaSenha.Value.CompareTo(DateTime.Now) < 0)
                        {
                            throw new Exception("A data de troca de senha do usuário expirou. Acesse o Sistema Integrado de Acesso (SIA) para trocar sua senha.");
                        }

                        if (!SenhaValida(usuario.senhaAcesso, senhaAcesso))
                        {
                            throw new Exception("Senha inválida para o usuário encontrado.");
                        }

                        resultado = ConversorModelParaDLL.Usuario(db, usuario);

                        usuario.dataUltimoAcesso = DateTime.Now;

                        db.SaveChanges();
                    }
                    else
                    {
                        throw new Exception("Nenhum usuário encontrado com os dados informados.");
                    }
                }

                return resultado;
            }

        }
        
        /// <summary>
        /// verifica se uma senha é valida atraves da senha de acesso 
        /// </summary>
        /// <param name="senhaAcesso">Senha de acesso </param>
        /// <param name="senhaInformada">Senha a ser verificada </param>
        /// <returns> Retorna o resultado da validade da senha </returns>
        private bool SenhaValidaAluno(string senhaAcesso, string senhaInformada)
        {
            return senhaAcesso.TrimEnd(' ') == senhaInformada;
        }
        
        /// <summary>
        /// Faz login de um usuario estudante geral 
        /// </summary>
        /// <param name="rga">RGA do estudante a ser pesquisado </param>
        /// <param name="senha">Senha do estudante a ser pesquisado</param>
        /// <param name="tiposEstudante">Tipo de estudante a ser pesquisado</param>
        /// <returns>Retorna dados do estudante pesquisado  </returns>
        public Aluno LoginEstudanteGeral(string rga, string senha, TipoEstudante[] tiposEstudante)
        {
            try
            {
                if (string.IsNullOrEmpty(rga) || string.IsNullOrEmpty(senha))
                {
                    throw new ArgumentException("RGA ou senha não informados.");
                }

                if (tiposEstudante == null || tiposEstudante.Count() == 0)
                {
                    throw new ArgumentException("Nenhum tipo de estudante para tentativa de login.");
                }

                const string mensagemDeSenha = "Senha inválida para o estudante encontrado.";
                decimal rgaDecimal = Convert.ToDecimal(rga);
                Aluno resultado = null;

                using (var db = new idbufmtEntities())
                {
                    if (tiposEstudante.Contains(TipoEstudante.Graduacao))
                    {
                        var estudante = db.vwAlunoSiga.Where(x => x.Matricula == rgaDecimal).FirstOrDefault();

                        if (estudante != null)
                        {
                            if (SenhaValidaAluno(estudante.senha, senha))
                            {
                                resultado = new Aluno(estudante);
                            }
                            else
                            {
                                throw new ArgumentException(mensagemDeSenha);
                            }
                        }
                    }

                    if (tiposEstudante.Contains(TipoEstudante.PosGraduacao) && resultado == null)
                    {
                        var estudante = db.GetDadosAlunoPos.FirstOrDefault(x => x.rga == rgaDecimal);

                        if (estudante != null)
                        {
                            if (SenhaValidaAluno(estudante.senha, senha))
                            {
                                resultado = new Aluno(estudante);
                            }
                            else
                            {
                                throw new ArgumentException(mensagemDeSenha);
                            }
                        }
                    }
                }

                if (resultado != null)
                {
                    return resultado;
                }
                else
                {
                    throw new Exception("Nenhum usuário encontrado com os dados informados.");
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        /// Pesquisa de usuarios estudante geral 
        /// </summary>
        /// <param name="rga"> Matricula do estudante a ser pesquisado </param>
        /// <param name="senha">Senha de acesso do estudante </param>
        /// <param name="tiposEstudante"> Tipo de estudante a ser pesquisado </param>
        /// <param name="campusUID"> UID do Campos a ser pesquisado </param>
        /// <returns>Retorna as informações do estudante </returns>
        public Aluno LoginEstudanteGeral(string rga, string senha, TipoEstudante[] tiposEstudante, int[] campusUID)
        {
            try
            {
                //throw new NotImplementedException();

                if (string.IsNullOrEmpty(rga) || string.IsNullOrEmpty(senha)) { throw new ArgumentException("RGA ou senha não informados."); }

                if (tiposEstudante == null || tiposEstudante.Count() == 0) { throw new ArgumentException("Nenhum tipo de estudante para tentativa de login."); }

                if (campusUID == null || campusUID.Count() == 0) { throw new ArgumentException("Nenhum campus informado para tentativa de login."); }

                const string mensagemDeSenha = "Senha inválida para o estudante encontrado.";
                decimal rgaDecimal = Convert.ToDecimal(rga);
                Aluno resultado = null;
                byte campusss = Convert.ToByte(campusUID.FirstOrDefault());
                using (var db = new idbufmtEntities())
                {
                    if (tiposEstudante.Contains(TipoEstudante.Graduacao))
                    {
                        var estudante = db.vwAlunoSiga.Where(x => x.Matricula == rgaDecimal && x.CodCampus.Equals(campusss)).FirstOrDefault();

                        if (estudante != null)
                        {
                            if (SenhaValidaAluno(estudante.senha, senha))
                            {
                                resultado = new Aluno(estudante);
                            }
                            else
                            {
                                throw new ArgumentException(mensagemDeSenha);
                            }
                        }
                    }

                    if (tiposEstudante.Contains(TipoEstudante.PosGraduacao) && resultado == null)
                    {
                        var estudante = db.GetDadosAlunoPos.FirstOrDefault(x => x.rga == rgaDecimal);

                        if (estudante != null)
                        {
                            if (SenhaValidaAluno(estudante.senha, senha))
                            {
                                resultado = new Aluno(estudante);
                            }
                            else
                            {
                                throw new ArgumentException(mensagemDeSenha);
                            }
                        }
                    }
                }

                if (resultado != null)
                {
                    return resultado;
                }
                else
                {
                    throw new Exception("Nenhum usuário encontrado com os dados informados.");
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        /// Verifica se uma determinada permissão existe 
        /// </summary>
        /// <param name="permissaoUID">UID de permissão a ser verificada </param>
        /// <returns>Retorna uma lista de com os dados passados por parametro </returns>
        public PermissaoCampusEntity[] GetPermissaoCampusPorPermissao(long permissaoUID)
        {
            try
            {
                //throw new NotImplementedException();

                using (var db = new idbufmtEntities())
                {
                    var permissao = db.Permissao1Set.Find(permissaoUID);

                    if (permissao == null)
                    {
                        throw new ArgumentException("Permissão procurada não existe.");
                    }

                    var resultado = new List<PermissaoCampusEntity>();

                    foreach (var pc in permissao.PermissaoCampus)
                    {
                        if (pc != null) { resultado.Add(new PermissaoCampusEntity(pc)); }
                    }

                    return resultado.ToArray();
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        /// Verfica a Autenticidade do CPF Informado
        /// </summary>
        /// <param name="cpf">cpf a ser autenticado </param>
        /// <returns> Retorna o resultado da autenticação do cpf </returns>
        private string NormalizaCPF(string cpf)
        {
            return IsCpf(cpf);
        }
        
        /// <summary>
        /// Verifica se o cpf é válido
        /// </summary>
        /// <param name="cpf"> cpf a ser verificado </param>
        /// <returns>Retorna o resultado da validaçaõ do cpf informado  </returns>
        private string IsCpf(string cpf)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;


            if (string.IsNullOrEmpty(cpf))
                throw new FaultException("O CPF não foi informado.");

            cpf = cpf.Replace(".", "").Replace("-", "").Trim();
            
            if (cpf.Length != 11)
                throw new FaultException("O CPF informado está com tamanho incorreto.");

            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];

            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = resto.ToString();

            tempCpf = tempCpf + digito;

            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];

            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;

            digito = digito + resto.ToString();

            if (cpf.EndsWith(digito))
            {
                return cpf;
            }
            else
            {
                throw new FaultException("O CPF informado não é um CPF válido.");
            }
        }
        
        /// <summary>
        ///  Login de  estudantes DPR por rga e senha  
        /// </summary>
        /// <param name="rga"> Numero de matricula do estudante </param>
        /// <param name="senha"> senha de acesso do estudante </param>
        /// <returns>Retorna os dados do estudante verificado </returns>
        public Aluno LoginEstudanteDPR(string rga, string senha)
        {
            try
            {
                //throw new NotImplementedException();

                decimal rgaDecimal = decimal.Parse(rga);

                using (var db = new idbufmtEntities())
                {
                    var aluno = db.ViewAlunosGraduacaoDPR.FirstOrDefault(x => x.rga == rgaDecimal);

                    if (aluno == null)
                    {
                        throw new Exception("Nenhum estudante encontrado com este RGA.");
                    }

                    if (!SenhaValidaAluno(aluno.senha, senha))
                    {
                        throw new Exception("Senha inválida para o estudante encontrado.");
                    }

                    return new Aluno(aluno);
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        /// Valida o Token de um determinado Usuario
        /// </summary>
        /// <param name="token"> Token do usuario a ser validado </param>
        public UsuarioEntity ValidarToken(string token)
        {
            try
            {
                using (var db = new idbufmtEntities())
                {
                    var result = db.Autorizacao.Where(x => x.Token == token &&
                                                           x.ativo == 0).FirstOrDefault();
                    if (result != null)
                    {
                        if (result.usuarioUID == null)
                            return null; //É UM DISCENTE

                        result.Usuario = db.Usuario1Set.Where(x => x.usuarioUID == result.usuarioUID).FirstOrDefault();
                        result.Usuario.dataUltimoAcesso = DateTime.Now;
                        UsuarioEntity user = new UsuarioEntity(result.Usuario);

                        result.ativo = 1;

                        db.SaveChanges();

                        return user;
                    }
                    else
                    {
                        throw new FaultException("Token não localizado, realize login no portal de sistemas.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Valida o Token de um determinado Aluno
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public Aluno ValidarTokenAluno(string token)
        {
            try
            {
                using (var db = new idbufmtEntities())
                {
                    var result = db.Autorizacao.Where(x => x.Token == token &&
                                                           x.ativo == 0).FirstOrDefault();

                    var rgaDec = Convert.ToDecimal(result.rga);

                    if (result != null)
                    {
                        if (result.rga == null)
                            return null; //É UM SERVIDOR
                        var aluno = db.GetDadosAluno.Where(x => x.rga == rgaDec).FirstOrDefault();

                        return new Aluno(aluno);
                    }
                    else
                    {
                        throw new FaultException("Token não localizado, realize login no portal de sistemas.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        ///  Reinicia a senha do cpf passado por parametro
        /// </summary>
        /// <param name="cpf"> cpf do usuario a ser alterado </param>
        public void ReiniciarSenhaIntegrado(string cpf)
        {
            using (var db = new idbufmtEntities())
            {
                //Busca a Pessoa com o CPF informado.
                var pessoa = db.Pessoa.FirstOrDefault(x => x.cpf == cpf);

                //Se não há uma pessoa registrada com esse CPF...
                if (pessoa == null)
                {
                    //...lança uma exceção para o consumidor do serviço.
                    throw new Exception("Não há registro de servidor com este cpf.");
                }

                //Tento pegar o último registro de usuário da Pessoa que não esteja bloqueado...
                var user = pessoa.Usuario1.LastOrDefault(x => !x.bloqueado);

                //Se não existir usuário NÃO BLOQUEADO...
                if (user == null)
                {
                    //...Tento encontrar qualquer usuário, mesmo bloqueado.
                    user = pessoa.Usuario1.LastOrDefault();

                    string mensagem = string.Empty;

                    //Neste caso, a verificação se o Usuário é nulo é apenas para escolher a mensagem da exceção que será passada ao consumidor do serviço.
                    if (user == null)
                        mensagem = "Não há usuário para o servidor encontrado. Cadastre seu usuário na área de \"Cadastrar novo Usuário\" do SIA (Sistema Integrado de Acesso).";
                    else
                    {
                        mensagem = "Usuário está bloqueado.";
                    }

                    throw new Exception(mensagem);
                }

                string novaSenha = CriptografarSenha(NovaSenhaValida());

                //user.senhaAcesso = CriptografarSenha(novaSenha);

                //db.SaveChanges();

                try
                {
                    var emailServ = new Email();

                    emailServ.EnviarSmtpPadraoVersaoDois(pessoa.mail, MensagemDeSenhaPadraoIntegrado(novaSenha), "Solicitação de Reinicio de Senha - SIA UFMT");
                }
                catch (Exception ex)
                {
                    throw new Exception("Problema no envio de email. Detalhes do Erro: " + ex.Message);
                }
            }
        }
    }
}
