﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.ServiceModel.Activation;
using ufmt.services.entities;

namespace ufmt.services
{
    /// <summary>
    /// Serviço de aplicaçao 
    /// </summary>
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Aplicacao" in code, svc and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class Aplicacao : IAplicacao
    {
        /// <summary>
        /// Recupera as informações de uma Aplicação atraves do seu ID
        /// </summary>
        /// <param name="aplicacaoUID">ID da aplicação em questão </param>
        /// <returns>Informações da aplicação</returns>
        public AplicacaoEntity GetAplicacao(long aplicacaoUID)
        {
            using (var db = new idbufmtEntities())
            {
                var aplicacao = db.Aplicacao.Where(x => x.aplicacaoUID == aplicacaoUID).FirstOrDefault();

                if (aplicacao != null)
                {
                    AplicacaoEntity aplicacaoEntity = new AplicacaoEntity(aplicacao);
                    return aplicacaoEntity;
                }
                else
                {
                    throw new FaultException("Não foi possível encontrar aplicação com o id informado.");
                }
            }
        }

        /// <summary>
        /// Pesquisa por Situação, Sigla, Nome e Numero de Linhas 
        /// </summary>
        /// <param name="situacao"> Situação da aplicação a ser pesquisada.</param>
        /// <param name="sigla"> Sigla da aplicação</param>
        /// <param name="nome"> Nome da aplicação a ser pesquisada</param>
        /// <param name="firstRowIndex"> Primeira linha do Index </param>
        /// <param name="maximumRows"> Numero másximo de linhas da aplicação a ser pesquisada </param>
        /// <returns></returns>
        public List<AplicacaoEntity> GetAplicacoes(string situacao, string sigla, string nome)//int firstRowIndex, int maximumRows)
        {
            try
            {
                using (var db = new idbufmtEntities())
                {
                    var listAplicacao = db.Aplicacao.Where(x => x.nome.Contains(nome) ||
                                                                x.sigla.Contains(sigla) ||
                                                                x.situacao.Contains(situacao))
                                                    .OrderBy(x => x.nome);

                    List<AplicacaoEntity> listAplicacaoEntity = new List<AplicacaoEntity>();

                    foreach (var aplicacao in listAplicacao)
                    {
                        listAplicacaoEntity.Add(new AplicacaoEntity(aplicacao));
                    }

                    return listAplicacaoEntity;
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Retorna uma Lista de Aplicacoes disponivel no banco.
        /// </summary>
        /// <returns>Lista de Aplicacoes</returns>
        public List<AplicacaoEntity> GetTodasAplicacoes()
        {
            try
            {
                using (var db = new idbufmtEntities())
                {
                    var aplicacoes = from apc in db.Aplicacao
                                     join pa in db.PublicoAplicacao on apc.aplicacaoUID equals pa.aplicacaoUID
                                     join p in db.Publico on pa.publicoUID equals p.publicoUID
                                     where apc.situacao.Trim().Equals("PRODUÇÃO") &&
                                           !string.IsNullOrEmpty(apc.url)
                                     orderby apc.nome ascending
                                     select new AplicacaoEntity
                                     {
                                         AplicacaoUID = apc.aplicacaoUID,
                                         Nome = apc.nome,
                                         Url = apc.url,
                                         Sigla = apc.sigla,
                                         Integrado = apc.integrado.Equals(null) ? false : (bool)apc.integrado
                                     };


                    if (aplicacoes != null)
                    {
                        return aplicacoes.ToList();
                    }
                    else
                    {
                        throw new FaultException();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Retorna todas as Aplicações de uma determinada Categoria
        /// </summary>
        /// <param name="publicoUID">UID da categoria das aplicações</param>
        /// <param name="perfilUID">UID do perfil a ser filtrado</param>
        /// <returns>Retorna todas as aplicações de uma determinada categoria</returns>
        public List<AplicacaoEntity> GetAplicacaoPorCategoria(long publicoUID, long perfilUID)
        {
            try
            {
                using (var db = new idbufmtEntities())
                {
                    var retorno = from apc in db.Aplicacao
                                  join pa in db.PublicoAplicacao on apc.aplicacaoUID equals pa.aplicacaoUID
                                  join p in db.Publico on pa.publicoUID equals p.publicoUID
                                  join ap in db.AplicacaoPerfil on apc.aplicacaoUID equals ap.aplicacaoUID
                                  join pf in db.Perfil on ap.perfilUID equals pf.perfilUID
                                  where p.publicoUID == publicoUID &&
                                        apc.situacao.Trim().Equals("PRODUÇÃO") &&
                                        !string.IsNullOrEmpty(apc.url) &&
                                        pf.perfilUID == perfilUID
                                  orderby apc.sigla ascending
                                  select new AplicacaoEntity
                                  {
                                      AplicacaoUID = apc.aplicacaoUID,
                                      Nome = apc.nome,
                                      Url = apc.url,
                                      Sigla = apc.sigla,
                                      Integrado = apc.integrado.Equals(null) ? false : (bool)apc.integrado
                                  };

                    if (retorno != null)
                    {
                        return retorno.ToList();
                    }
                    else
                    {
                        throw new FaultException("Não conseguimos encontrar nenhuma aplicação.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Retorna todas as Aplicações de um determinado Perfil
        /// </summary>
        /// <param name="perfilUID">IUD dos perfis das aplicações </param>
        /// <returns> Retorna todas as aplicações de um determinado perfil </returns>
        public List<AplicacaoEntity> GetAplicacaoPorPerfil(long perfilUID)
        {
            try
            {
                using (var db = new idbufmtEntities())
                {
                    var retorno = from apc in db.Aplicacao
                                  join p in db.AplicacaoPerfil on apc.aplicacaoUID equals p.aplicacaoUID
                                  join pe in db.Perfil on p.perfilUID equals pe.perfilUID
                                  where p.perfilUID == perfilUID &&
                                        apc.situacao.Trim().Equals("PRODUÇÃO") &&
                                        !string.IsNullOrEmpty(apc.url)
                                  orderby apc.sigla ascending
                                  select new AplicacaoEntity
                                  {
                                      AplicacaoUID = apc.aplicacaoUID,
                                      Nome = apc.nome,
                                      Url = apc.url,
                                      Sigla = apc.sigla,
                                      Integrado = apc.integrado.Equals(null) ? false : (bool)apc.integrado
                                  };

                    if (retorno != null)
                    {
                        return retorno.ToList();
                    }
                    else
                    {
                        throw new FaultException("Não conseguimos encontrar nenhuma aplicação.");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Retorna uma Estatistica das Aplicações mais Acessadas
        /// </summary>
        /// <returns>Rrtorna uma lista das aplicações mais acessadas </returns>
        public List<AplicacaoEntity> GetAplicacoesMaisAcessadas()
        {
            try
            {
                using (var db = new idbufmtEntities())
                {
                    var retorno = from at in db.Autorizacao
                                  join ap in db.Aplicacao on at.aplicacaoUID equals ap.aplicacaoUID
                                  where ap.situacao.Trim().Equals("PRODUÇÃO") &&
                                        !string.IsNullOrEmpty(ap.url)
                                  group ap by new { ap.nome, ap.sigla, ap.aplicacaoUID, ap.integrado, ap.url } into g
                                  let qtde = g.Count()
                                  orderby qtde descending
                                  select new AplicacaoEntity
                                  {
                                      AplicacaoUID = g.Key.aplicacaoUID,
                                      Sigla = g.Key.sigla,
                                      Nome = g.Key.nome,
                                      Qtde = qtde,
                                      Url = g.Key.url,
                                      Integrado = g.Key.integrado.Equals(null) ? false : (bool)g.Key.integrado
                                  };

                    if (retorno != null)
                    {
                        return retorno.ToList();
                    }
                    else
                    {
                        throw new FaultException("Não há registros para serem processados");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }


        /// <summary>
        /// Retorna as aplicações mais acessadas por categoria
        /// </summary>
        /// <param name="publicoUID">UID da categoria das aplicações </param>
        /// <param name="perfilUID"></param>
        /// <returns>Retorna uma lista das aplicações mais acessadas </returns>
        public List<AplicacaoEntity> GetAplicacoesMaisAcessadasPorCategoria(long publicoUID, long perfilUID)
        {
            try
            {
                using (var db = new idbufmtEntities())
                {
                    var retorno = from at in db.Autorizacao
                                  join ap in db.Aplicacao on at.aplicacaoUID equals ap.aplicacaoUID
                                  join pa in db.PublicoAplicacao on ap.aplicacaoUID equals pa.aplicacaoUID
                                  join app in db.AplicacaoPerfil on ap.aplicacaoUID equals app.aplicacaoUID
                                  where pa.publicoUID == publicoUID &&
                                        ap.situacao.Trim().Equals("PRODUÇÃO") &&
                                        !string.IsNullOrEmpty(ap.url) &&
                                        app.perfilUID == perfilUID
                                  group ap by new { ap.nome, ap.aplicacaoUID, ap.sigla, ap.url, ap.integrado } into g
                                  let qtde = g.Count()
                                  orderby qtde descending
                                  select new AplicacaoEntity
                                  {
                                      AplicacaoUID = g.Key.aplicacaoUID,
                                      Sigla = g.Key.sigla,
                                      Nome = g.Key.nome,
                                      Qtde = qtde,
                                      Url = g.Key.url,
                                      Integrado = g.Key.integrado.Equals(null) ? false : (bool)g.Key.integrado
                                  };

                    if (retorno != null)
                    {
                        return retorno.ToList();
                    }
                    else
                    {
                        throw new FaultException("Não há registros para serem processados");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Retorna as aplicações acessadas recentemente por um determinado usuario
        /// </summary>
        /// <param name="usuarioUID"></param>
        /// <param name="perfilUID"></param>
        /// <returns></returns>
        public List<AplicacaoEntity> GetAcessadosRecentemente(string usuarioUID, long perfilUID)
        {
            try
            {
                var user = long.Parse(usuarioUID);

                using (var db = new idbufmtEntities())
                {
                    var retorno = from at in db.Autorizacao
                                  join u in db.Usuario1Set on at.usuarioUID equals u.usuarioUID
                                  join ap in db.Aplicacao on at.aplicacaoUID equals ap.aplicacaoUID
                                  join p in db.AplicacaoPerfil on ap.aplicacaoUID equals p.aplicacaoUID
                                  where u.usuarioUID == user &&
                                        ap.situacao.Trim().Equals("PRODUÇÃO") &&
                                        !string.IsNullOrEmpty(ap.url) &&
                                        p.perfilUID == perfilUID
                                  group ap by new { ap.nome, ap.aplicacaoUID, ap.sigla, ap.url, ap.integrado, at.Data } into g
                                  orderby g.Key.Data descending
                                  select new AplicacaoEntity
                                  {
                                      Url = g.Key.url,
                                      Nome = g.Key.nome,
                                      AplicacaoUID = g.Key.aplicacaoUID,
                                      Sigla = g.Key.sigla,
                                      Integrado = g.Key.integrado.Equals(null) ? false : (bool)g.Key.integrado
                                  };
                    
                    if (retorno != null)
                    {
                        List<AplicacaoEntity> lista = retorno.GroupBy(ap => ap.AplicacaoUID).Select(g => g.FirstOrDefault()).ToList();
                        return lista;
                    }
                    else
                    {
                        throw new FaultException("Não há registros recentes!!");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }

        /// <summary>
        /// Retorna as aplicações acessadas recentemente por um determinado usuario e perfil
        /// </summary>
        /// <param name="usuarioUID"></param>
        /// <param name="publicoUID"></param>
        /// <param name="perfilUID"></param>
        /// <returns></returns>
        public List<AplicacaoEntity> GetAcessadosRecentementePorCategoria(string usuarioUID, long publicoUID, long perfilUID)
        {
            try
            {
                var user = long.Parse(usuarioUID);

                using (var db = new idbufmtEntities())
                {
                    var retorno = from at in db.Autorizacao
                                  join u in db.Usuario1Set on at.usuarioUID equals u.usuarioUID
                                  join ap in db.Aplicacao on at.aplicacaoUID equals ap.aplicacaoUID
                                  join app in db.AplicacaoPerfil on ap.aplicacaoUID equals app.aplicacaoUID
                                  join pu in db.PublicoAplicacao on ap.aplicacaoUID equals pu.aplicacaoUID
                                  where u.usuarioUID == user &&
                                        ap.situacao.Trim().Equals("PRODUÇÃO") &&
                                        !string.IsNullOrEmpty(ap.url) &&
                                        app.perfilUID == perfilUID &&
                                        pu.publicoUID == publicoUID
                                  group ap by new { ap.nome, ap.aplicacaoUID, ap.sigla, ap.url, ap.integrado, at.Data } into g
                                  orderby g.Key.Data ascending
                                  select new AplicacaoEntity
                                  {
                                      Url = g.Key.url,
                                      Nome = g.Key.nome,
                                      AplicacaoUID = g.Key.aplicacaoUID,
                                      Sigla = g.Key.sigla,
                                      Integrado = g.Key.integrado.Equals(null) ? false : (bool)g.Key.integrado
                                  };

                    if (retorno != null)
                    {
                        return retorno.Distinct().ToList();
                    }
                    else
                    {
                        throw new FaultException("Não há registros recentes!!");
                    }
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
    }
}
