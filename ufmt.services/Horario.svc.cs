﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Text;
using ufmt.services.entities;

namespace ufmt.services
{
    /// <summary>
    /// Métodos de consulta a Horário
    /// </summary>
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Horario" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Horario.svc or Horario.svc.cs at the Solution Explorer and start debugging.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class Horario : IHorario
    {
        /// <summary>
        /// Pesquisa de horário de um aluno por rga 
        /// </summary>
        /// <param name="rga">rga do aluno a ser pesquisado</param>
        /// <returns>Retorna a lista de horarios</returns>
        public List<HorarioEntity> GetHorario(string rga)
        {
            using (var db = new idbufmtEntities())
            {
                var retorno = db.ProcedureHistoricoAluno(decimal.Parse(rga));
                
                List<HorarioEntity> horarios = new List<HorarioEntity>();
                foreach (var item in retorno)
                {
                    HorarioEntity hor = new HorarioEntity(item);
                    horarios.Add(hor);
                }

                if (horarios.Count > 0)
                {
                    return horarios;
                }
                else
                {
                    throw new FaultException("Nenhum Horário encontrado para o RGA Solicitado");
                }
            }
        }
    }
}
