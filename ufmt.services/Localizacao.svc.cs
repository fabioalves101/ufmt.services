﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.ServiceModel.Activation;
using ufmt.sig.business;
using ufmt.services.entities;
using ufmt.sig.entity;

namespace ufmt.services
{
    /// <summary>
    /// Serviço que disponibiliza as informaçoes de unidade
    /// </summary>
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Localizacao" in code, svc and config file together.
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Required)]
    public class Localizacao : ILocalizacao
    {
        /// <summary>
        /// Lista de unidades 
        /// </summary>
        /// <param name="nomeParcial">Nome parcial da unidade a ser pesquisado</param>
        /// <param name="campusUID">UID do campus a ser verificado </param>
        /// <param name="apenasAtivos">Filtro de atividade das unidade a ser pesquisadas </param>
        /// <param name="registroInicial">Registro inicial da unidade a ser pesquisada </param>
        /// <param name="quantidadeRegistros">quantidades de registro por unidade a ser pesquisado</param>
        /// <returns>retorna uma lista de unidades com os parametros passados  </returns>
        public List<UnidadeEntity> GetUnidades(string nomeParcial, int? campusUID, bool? apenasAtivos, int? registroInicial,
            int? quantidadeRegistros)
        {
            try
            {
                List<entities.Unidade> unidades = new List<entities.Unidade>();
                using (var db = new idbufmtEntities())
                {
                    if (!string.IsNullOrEmpty(nomeParcial))
                    {
                        unidades = db.Unidade.Where(x => x.nome.Contains(nomeParcial)).ToList();
                    }

                    if (campusUID.HasValue)
                    {
                        if (unidades.Count > 0)
                        {
                            unidades = unidades.Where(x => x.campusUID == campusUID).ToList();
                        }
                        else
                        {
                            unidades = db.Unidade.Where(x => x.campusUID == campusUID).ToList();
                        }

                    }

                    if (apenasAtivos.HasValue)
                    {
                        if (unidades.Count > 0)
                        {
                            if (apenasAtivos.Value)
                            {
                                unidades = unidades.Where(u => u.ativa.Equals("1")).ToList();
                            }
                            else
                            {
                                unidades = unidades.Where(u => u.ativa.Equals("0")).ToList();
                            }
                        }
                        else
                        {
                            if (apenasAtivos.Value)
                            {
                                unidades = db.Unidade.Where(u => u.ativa.Equals("1")).ToList();
                            }
                            else
                            {
                                unidades = db.Unidade.Where(u => u.ativa.Equals("0")).ToList();
                            }
                        }
                    }

                    if (quantidadeRegistros.HasValue && quantidadeRegistros > 0)
                    {
                        unidades = unidades.Take((int)quantidadeRegistros).ToList();
                    }

                    List<UnidadeEntity> unidadesEntity = new List<UnidadeEntity>();
                    foreach (ufmt.services.entities.Unidade unidade in unidades)
                    {
                        UnidadeEntity unidadeEntity = new UnidadeEntity(unidade);

                        if (unidade.unidadeSuperiorUID != null && unidade.unidadeSuperiorUID > 0)
                            unidadeEntity.UnidadeSuperiorUID = (long)unidade.unidadeSuperiorUID;
                        else
                            unidadeEntity.UnidadeSuperiorUID = 0;

                        unidadesEntity.Add(unidadeEntity);
                    }

                    return unidadesEntity;
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        /// Pesquisa de unidade por UID 
        /// </summary>
        /// <param name="unidadeUID">UID da unidade a ser pesquisada </param>
        /// <returns> Retorna informações da unidade pesquisada </returns>
        public UnidadeEntity GetUnidade(long unidadeUID)
        {
            try
            {
                using (ILocalizacaoBO localizacaoBO = BusinessFactory.GetInstance<ILocalizacaoBO>())
                {
                    ufmt.sig.entity.Unidade unidade = localizacaoBO.GetUnidade(unidadeUID);
                    
                    UnidadeEntity unidadeEntity = new UnidadeEntity()
                    {
                        CampusUID = unidade.Campus.CampusUID,
                        Hierarquia = unidade.Hierarquia,
                        Nome = unidade.Nome,
                        Sigla = unidade.Sigla,
                        TipoUnidade = (int)unidade.TipoUnidade,
                        UnidadeUID = unidade.UnidadeUID
                    };

                    if (unidade.UnidadeSuperior != null)
                        unidadeEntity.UnidadeSuperiorUID = unidade.UnidadeSuperior.UnidadeUID;
                    else
                        unidadeEntity.UnidadeSuperiorUID = 0;

                    return unidadeEntity;
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        /// Pesquisa de Campi por nome parcial 
        /// </summary>
        /// <param name="nomeParcial"> nome parcial do campi a ser pesquisado </param>
        /// <returns>Retorna uma lista de campi com o nome parcial passado </returns>
        public List<CampusEntity> GetCampi(string nomeParcial)
        {
            try
            {
                using (ILocalizacaoBO localizacaoBO = BusinessFactory.GetInstance<ILocalizacaoBO>())
                {
                    List<ufmt.sig.entity.Campus> campi = new List<ufmt.sig.entity.Campus>();

                    if (String.IsNullOrEmpty(nomeParcial))
                    {
                        campi = localizacaoBO.GetCampi().ToList();
                    }
                    else
                    {
                        campi = localizacaoBO.GetCampi().Where(c => c.Nome.Contains(nomeParcial.ToUpper())).ToList();
                    }

                    List<CampusEntity> campiEntity = new List<CampusEntity>();
                    foreach (ufmt.sig.entity.Campus campus in campi)
                    {
                        CampusEntity campusEntity = new CampusEntity()
                        {
                            CampusUID = campus.CampusUID,
                            Nome = campus.Nome
                        };

                        campiEntity.Add(campusEntity);
                    }

                    return campiEntity;
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        /// Pesquisa de unidade por pessoa responsável 
        /// </summary>
        /// <param name="pessoaUID">UID da pessoa responsável a ser pesquisada </param>
        /// <returns>Retorna dados da unidade pesquisada </returns>
        public UnidadeEntity GetUnidadePorResponsavel(long pessoaUID)
        {
            try
            {
                using (var db = new idbufmtEntities())
                {
                    var unidade = db.ResponsavelUnidade
                        .Where(u => u.pessoaUID == pessoaUID &&

                                    u.dataFinal == null)
                        .OrderBy(x => x.Unidade.nome)
                        .First();

                    var unidaderaiz = db.Unidade.Where(x => x.unidadeUID == unidade.unidadeUID).First();
                    
                    UnidadeEntity unidadeEntity = new UnidadeEntity()
                    {
                        CampusUID = unidaderaiz.Campus.campusUID,
                        Hierarquia = unidaderaiz.hierarquia,
                        Nome = unidaderaiz.nome,
                        Sigla = unidaderaiz.sigla,
                        TipoUnidade = (int)unidaderaiz.tipoUnidade,
                        UnidadeSuperiorUID = (int)unidaderaiz.unidadeSuperiorUID,
                        UnidadeUID = unidaderaiz.unidadeUID
                    };

                    return unidadeEntity;
                }
            }
            catch (Exception ex)
            {
                throw new FaultException("Nenhuma Unidade encontrada para a Pessoa informada");
            }
        }
        
        /// <summary>
        /// 
        /// </summary>
        /// <param name="unidade"></param>
        /// <returns></returns>
        public UnidadeEntity CastUnidadeToUnidadeEntity(ufmt.sig.entity.Unidade unidade)
        {
            UnidadeEntity unidadeEntity = new UnidadeEntity(unidade);

            return unidadeEntity;
        }
        
        /// <summary>
        /// Pesquisa de unidade por unidade superior 
        /// </summary>
        /// <param name="unidadeUID">UID da unidade superior a ser pesquisada </param>
        /// <returns> Retorna uma lista de unidades encontradas na unidade superior informada </returns>
        public List<UnidadeEntity> GetUnidadesPorUnidadeSuperior(long unidadeUID)
        {
            try
            {
                using (ILocalizacaoBO localizacaoBO = BusinessFactory.GetInstance<ILocalizacaoBO>())
                {
                    List<ufmt.sig.entity.Unidade> unidades = localizacaoBO.GetUnidades(null, unidadeUID, null, null,
                        null, null, null).ToList();

                    List<UnidadeEntity> unidadesEntity = new List<UnidadeEntity>();
                    foreach (ufmt.sig.entity.Unidade unidade in unidades)
                    {
                        UnidadeEntity unidadeEntity = new UnidadeEntity()
                        {
                            CampusUID = unidade.Campus.CampusUID,
                            Hierarquia = unidade.Hierarquia,
                            Nome = unidade.Nome,
                            Sigla = unidade.Sigla,
                            TipoUnidade = (int)unidade.TipoUnidade,
                            UnidadeUID = unidade.UnidadeUID,
                            Ativa = unidade.Ativa
                        };

                        if (unidade.UnidadeSuperior != null)
                            unidadeEntity.UnidadeSuperiorUID = unidade.UnidadeSuperior.UnidadeUID;
                        else
                            unidadeEntity.UnidadeSuperiorUID = 0;

                        unidadesEntity.Add(unidadeEntity);
                    }

                    return unidadesEntity;
                }
            }
            catch (Exception ex)
            {
                throw new FaultException<Exception>(new Exception(ex.Message), new FaultReason(ex.Message));
            }
        }
        
        /// <summary>
        /// Pesquisa o nome do Campi de uma determinada Unidade
        /// </summary>
        /// <param name="unidadeUID">UID da unidade a ser pesquisada</param>
        /// <returns>Retorna o nome do Campi da Unidade</returns>
        public string GetNomeCampiPorUnidade(long unidadeUID)
        {
            using (var db = new idbufmtEntities())
            {
                var campi = db.Unidade.Where(x => x.unidadeUID == unidadeUID);
                return campi.First().Campus.nome;
            }
        }
    }
}
